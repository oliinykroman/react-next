export const BUILD_ENV = 'prod';

export const COLORS = {
  DARK: '#121212',
  DANGER: '#A60054',
  LIGHT_DANGER: '#E93C8D',
  RED: '#D24340',
  WHITE: '#FFFFFF',
  GRAY: '#555555',
  GRAY_2: '#999999',
  GRAY_81: '#CFCFCF',
  LIGHT_GRAY: '#EDEDF1',
  LIGHT_GRAY_2: '#F4F4F4',
  LIGHT_BEIGE_OPACITY_06: 'rgba(18, 18, 18, 0.06)',
  BEIGE: '#F4F4F4',
  YELLOW: '#F9AC19',
  TRANSPARENT: 'transparent',
  BURGUNDY: '#66002E',
  GREEN: '#4caf50'
};

export const SIZES = {
  s: `(max-width: 320px)`,
  sm: `(max-width: 575px)`,
  m: `(max-width: 767px)`,
  lg: `(max-width: 991px)`,
  xl: `(max-width: 1199px)`
};

export const SIZES_MIN = {
  s: `(min-width: 320px)`,
  sm: `(min-width: 576px)`,
  m: `(min-width: 768px)`,
  lg: `(min-width: 992px)`,
  xl: `(min-width: 1200px)`
};

export const ROUT_CONTENT_MODERATION = '/content-moderation';
export const GRID_COLUMN_PADDING = '15px';
export const FORM_SUBMIT_URL = '/integration/submit/6063253';

export const API_URL = 'https://api.hsforms.com/submissions/v3';
export const FORM_ID_DEV = '47c5b170-181c-4b97-9a15-968bf4355909';
export const FORM_ID_PROD = 'cfdea84a-aac9-4c17-9765-cc3fb9ec5cf7';
