import UkIcon from '../svgComponent/UkIcon';

export const langListConfig = [
  {
    title: 'lang_eng',
    Icon: UkIcon,
    key: 'en'
  },
  {
    title: 'lang_french',
    Icon: UkIcon,
    key: 'fr'
  }
];
