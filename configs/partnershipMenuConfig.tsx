export const partnershipList = [
  {
    image_2x: '/images/CCBill_logo.png, /images/2x/CCBill_logo@2x.png 2x',
    image: '/images/CCBill_logo.png',
    alt: 'CCBill'
  },
  {
    image_2x: '/images/SEGPAY_logo.png, /images/2x/SEGPAY_logo@2x.png 2x',
    image: '/images/SEGPAY_logo.png',
    alt: 'SEGPAY'
  },
  {
    image_2x: '/images/Vendo_logo.png, /images/2x/Vendo_logo@2x.png 2x',
    image: '/images/Vendo_logo.png',
    alt: 'Vendo'
  },
  {
    image_2x: '/images/Next_Step_logo.png, /images/2x/Next_Step_logo@2x.png 2x',
    image: '/images/Next_Step_logo.png',
    alt: 'Next Step'
  },
  {
    image_2x: '/images/Hive_logo.png, /images/2x/Hive_logo@2x.png 2x',
    image: '/images/Hive_logo.png',
    alt: 'Hive'
  }
];
