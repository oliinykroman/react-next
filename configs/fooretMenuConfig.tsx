export const aboutList = [
  {
    title: 'footer_menu_link_about_2',
    href: '/partnership'
  },
  {
    title: 'footer_menu_link_about_5',
    href: '/privacy-page'
  }
];

export const featureList = [
  {
    title: 'footer_menu_link_feature_1',
    href: '/content-provider'
  },
  {
    title: 'footer_menu_link_feature_2',
    href: '/content-moderation'
  },
  {
    title: 'footer_menu_link_feature_3',
    href: '/live-streaming'
  },
  {
    title: 'footer_menu_link_feature_4',
    href: '/complaint-resolution'
  },
  {
    title: 'footer_menu_link_feature_5',
    href: '/monthly-reporting'
  },
  {
    title: 'footer_menu_link_feature_6',
    href: '/quality-control'
  }
];

export const developersList = [
  {
    title: 'footer_menu_link_developer_1',
    href: 'https://docs.verifymyage.com/content/'
  }
];

export const certificateList = [
  {
    image_2x: '/images/ISO27001.png, /images/2x/ISO27001@2x.png 2x',
    image: '/images/ISO27001.png',
    alt: 'ISO27001',
    description: 'footer_list_certificate_1'
  },
  {
    image_2x: '/images/PAS.png, /images/2x/PAS@2x.png 2x',
    image: '/images/PAS.png',
    alt: 'PAS',
    description: 'footer_list_certificate_2'
  },
  {
    image_2x: '/images/UKIE.png, /images/2x/UKIE@2x.png 2x',
    image: '/images/UKIE.png',
    alt: 'UKIE',
    description: 'footer_list_certificate_3'
  },
  {
    image_2x: '/images/high-end-cryptography.png, /images/2x/high-end-cryptography@2x.png 2x',
    image: '/images/high-end-cryptography.png',
    alt: 'high-end-cryptography',
    description: 'footer_list_certificate_4'
  },
  {
    image_2x: '/images/KJM.png, /images/2x/KJM@2x.png 2x',
    image: '/images/KJM.png',
    alt: 'KJM',
    description: 'footer_list_certificate_1'
  }
];
