export const headerNavConfig = [
  // {
  //   title: 'header_menu_link_about',
  //   href: '/about',
  //   children: []
  // },
  {
    title: 'header_menu_link_features',
    href: '/features',
    children: [
      {
        title: 'header_menu_sub_link_1',
        href: '/content-provider'
      },
      {
        title: 'header_menu_sub_link_2',
        href: '/content-moderation'
      },
      {
        title: 'header_menu_sub_link_3',
        href: '/live-streaming'
      },
      {
        title: 'header_menu_sub_link_4',
        href: '/complaint-resolution'
      },
      {
        title: 'header_menu_sub_link_5',
        href: '/monthly-reporting'
      },
      {
        title: 'footer_menu_link_feature_6',
        href: 'quality-control'
      }
    ]
  },
  {
    title: 'header_menu_link_contact',
    href: '/#contactForm'
  }
];
