export const contentSolutionCarouselSection = {
  title: 'content_solution_carousel_title',
  text: 'content_solution_carousel_text'
};

export const contentSolutionCarousel = [
  {
    title: 'content_solution_carousel_item_1_title',
    text: 'content_solution_carousel_item_1_text',
    imageSrc_2x:
      '/images/solutionCarousel/Ellipse1.png, /images/solutionCarousel/2x/Ellipse1@2x.png 2x',
    imageSrc: '/images/solutionCarousel/Ellipse1.png'
  },
  {
    title: 'content_solution_carousel_item_2_title',
    text: 'content_solution_carousel_item_2_text',
    imageSrc_2x:
      '/images/solutionCarousel/Ellipse2.png, /images/solutionCarousel/2x/Ellipse2@2x.png 2x',
    imageSrc: '/images/solutionCarousel/Ellipse2.png'
  },
  {
    title: 'content_solution_carousel_item_3_title',
    text: 'content_solution_carousel_item_3_text',
    imageSrc_2x:
      '/images/solutionCarousel/Ellipse6.png, /images/solutionCarousel/2x/Ellipse6@2x.png 2x',
    imageSrc: '/images/solutionCarousel/Ellipse6.png'
  },
  {
    title: 'content_solution_carousel_item_4_title',
    text: 'content_solution_carousel_item_4_text',
    imageSrc_2x:
      '/images/solutionCarousel/Ellipse3.png, /images/solutionCarousel/2x/Ellipse3@2x.png 2x',
    imageSrc: '/images/solutionCarousel/Ellipse3.png'
  },
  {
    title: 'content_solution_carousel_item_5_title',
    text: 'content_solution_carousel_item_5_text',
    imageSrc_2x:
      '/images/solutionCarousel/Ellipse4.png, /images/solutionCarousel/2x/Ellipse4@2x.png 2x',
    imageSrc: '/images/solutionCarousel/Ellipse4.png'
  },
  {
    title: 'content_solution_carousel_item_6_title',
    text: 'content_solution_carousel_item_6_text',
    imageSrc_2x:
      '/images/solutionCarousel/Ellipse5.png, /images/solutionCarousel/2x/Ellipse5@2x.png 2x',
    imageSrc: '/images/solutionCarousel/Ellipse5.png'
  }
];
