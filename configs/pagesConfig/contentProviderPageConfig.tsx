import { COLORS } from '../../const';
import { buttonsType } from '../../components/button';
import WhiteChevron from '../../svgComponent/WhiteChewron';
import { qualityControlPageCarousel } from './qualityControlPageConfig';

export const contentProviderHero = {
  title: 'content_provider_hero_title',
  subtitle: 'content_provider_hero_subtitle',
  bgColor: COLORS.WHITE,
  sectionImg: '/images/heroImages/contentProvider.png',
  sectionImg_2x: '/images/heroImages/2x/contentProvider@2x.png',
  href: '/#contactForm',
  button: {
    type: buttonsType.largeDanger,
    label: 'Book a Demo',
    action: () => {
      console.log('action');
    }
  }
};

export const contentMastercard = {
  title: 'mastercard_title',
  text: 'mastercard_text',
  bgColor: COLORS.BEIGE
};

export const contentVerifyContent = {
  title: 'c_verification_content_section_title',
  text_1: 'c_verification_content_section_text_1',
  bgColor: COLORS.DARK
};

export const contentHowItWorks = {
  subtitle: 'conten_section_subtitle',
  title: 'conten_section_title',
  text: 'conten_section_text',
  bgColor: COLORS.BEIGE,
  image_2x: '/images/diagram.png, /images/2x/diagram@2x.png 2x',
  image: '/images/diagram.png',
  alt: 'diagram',
  href: '/#contactForm',
  button_1: {
    type: buttonsType.smallDanger,
    label: 'button_label_uploader_journey',
    action: () => {
      console.log('action');
    }
  },
  button_2: {
    type: buttonsType.smallGray,
    label: 'button_label_participant_journey',
    action: () => {
      console.log('action');
    }
  }
};

export const contentFeatureFocus = {
  subtitle: 'c_feature_focus_section_subtitle',
  title: 'c_feature_focus_section_title',
  text: 'c_feature_focus_section_text',
  bgColor: COLORS.DARK,
  image_2x: '/images/featureFocus/verification.png, /images/featureFocus/2x/verification@2x.png 2x',
  image: '/images/featureFocus/verification.png',
  alt: 'verification'
};

export const contentCard = {
  items: [
    {
      item_title: 'с_card_section_card_1_title',
      item_text: 'с_card_section_card_1_text',
      bgItemColor: COLORS.BEIGE
    },
    {
      item_title: 'с_card_section_card_2_title',
      item_text: 'с_card_section_card_2_text',
      bgItemColor: COLORS.BEIGE
    },
    {
      item_title: 'с_card_section_card_3_title',
      item_text: 'с_card_section_card_3_text',
      bgItemColor: COLORS.BEIGE
    }
  ]
};

export const contentDangerList = {
  title: 'c_danger_list_section_title',
  bgColor: COLORS.BEIGE,
  bgItemColor: COLORS.DANGER,
  items: [
    {
      item_image: '/images/heath-icon.png',
      item_alt: 'heath-icon',
      item_title: 'с_danger_list_1_title',
      item_text: 'с_danger_list_1_text'
    },
    {
      item_image: '/images/lock.png',
      item_alt: 'lock',
      item_title: 'с_danger_list_2_title',
      item_text: 'с_danger_list_2_text'
    },
    {
      item_image: '/images/check.png',
      item_alt: 'check',
      item_title: 'с_danger_list_3_title',
      item_text: 'с_danger_list_3_text'
    }
  ]
};

export const contentVerticalList = {
  text: 'c_vertical_list_section_text',
  items: [
    {
      item_image: '/images/id-card.png',
      item_alt: 'heath-icon',
      item_title: 'c_vertical_list_1_title',
      item_text: 'c_vertical_list_1_text'
    },
    {
      item_image: '/images/drivers-licence.png',
      item_alt: 'lock',
      item_title: 'c_vertical_list_2_title',
      item_text: 'c_vertical_list_2_text'
    },
    {
      item_image: '/images/passport.png',
      item_alt: 'check',
      item_title: 'c_vertical_list_3_title',
      item_text: 'c_vertical_list_3_text'
    }
  ]
};

export const doubleCardProvider = {
  title: 'p_provider_double_card_title',
  text: 'p_provider_double_card_text',
  bgColor: COLORS.WHITE,
  Icon: WhiteChevron,
  cards: [
    { title: 'p_provider_double_item_title_1', text: 'p_provider_double_item_text_1' },
    { title: 'p_provider_double_item_title_2', text: 'p_provider_double_item_text_2' }
  ]
};

export const contentProviderPageCarousel = {
  title: 'content_solution_carousel_title_provider_page',
  text: 'content_solution_carousel_text_provider_page',
  href: '/#contactForm',
  carousel: [
    {
      title: 'content_solution_carousel_item_1_title_provider',
      text: 'content_solution_carousel_item_1_text_provider',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse1.png, /images/solutionCarousel/2x/Ellipse1@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse1.png',
      href: '/content-provider'
    },
    {
      title: 'content_solution_carousel_item_2_title_provider',
      text: 'content_solution_carousel_item_2_text_provider',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse2.png, /images/solutionCarousel/2x/Ellipse2@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse2.png',
      href: '/content-moderation'
    },
    {
      title: 'content_solution_carousel_item_3_title_provider',
      text: 'content_solution_carousel_item_3_text_provider',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse6.png, /images/solutionCarousel/2x/Ellipse6@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse6.png',
      href: '/live-streaming'
    },
    {
      title: 'content_solution_carousel_item_4_title_provider',
      text: 'content_solution_carousel_item_4_text_provider',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse3.png, /images/solutionCarousel/2x/Ellipse3@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse3.png',
      href: '/complaint-resolution'
    },
    {
      title: 'content_solution_carousel_item_5_title_provider',
      text: 'content_solution_carousel_item_5_text_provider',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse4.png, /images/solutionCarousel/2x/Ellipse4@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse4.png',
      href: '/monthly-reporting'
    },
    {
      title: 'content_solution_carousel_item_6_title_provider',
      text: 'content_solution_carousel_item_6_text_provider',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse5.png, /images/solutionCarousel/2x/Ellipse5@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse5.png',
      href: '/quality-control'
    }
  ]
};
