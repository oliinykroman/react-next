import Video from '../../svgComponent/contentModeration/Video';
import WebCamera from '../../svgComponent/contentModeration/WebCamera';
import Chat from '../../svgComponent/contentModeration/Chat';
import Image from '../../svgComponent/contentModeration/Image';

export const contentModerationConfig = {
  title: 'content_moderation_title',
  text: 'content_moderation_text',
  labels: [
    {
      labelTitle: 'content_moderation_label_1',
      LabelIcon: Video
    },
    {
      labelTitle: 'content_moderation_label_2',
      LabelIcon: WebCamera
    },
    {
      labelTitle: 'content_moderation_label_3',
      LabelIcon: Image
    },
    {
      labelTitle: 'content_moderation_label_4',
      LabelIcon: Chat
    }
  ]
};
