import { COLORS } from '../../const';
import { buttonsType } from '../../components/button';
import OrDanger from '../../svgComponent/OrDanger';

export const liveStreamingHero = {
  title: 'live_striream_hero_title',
  subtitle: 'live_striream_hero_subtitle',
  bgColor: COLORS.WHITE,
  sectionImg: '/images/heroImages/liveStriream.png',
  sectionImg_2x: '/images/heroImages/2x/liveStriream@2x.png',
  href: '/#contactForm',
  button: {
    type: buttonsType.largeDanger,
    label: 'Book a Demo',
    action: () => {
      console.log('action');
    }
  }
};

export const contentMastercard = {
  title: 'mastercard_title',
  text: 'mastercard_test_2',
  bgColor: COLORS.BEIGE
};

export const contentVerifyContent = {
  title: 'c_verification_content_section_title',
  text_1: 'c_verification_content_section_text_3',
  bgColor: COLORS.DARK
};

export const doubleCardLiveStream = {
  title: 'live_moderation_double_card_title',
  text: 'live_moderation_double_card_text',
  bgColor: COLORS.WHITE,
  Icon: OrDanger,
  cards: [
    {
      item_icon_url: '/images/check-ico.png',
      item_icon_alt: 'check-ico',
      title: 'live_moderation_double_item_title_1',
      text: 'live_moderation_double_item_text_1'
    },
    {
      item_icon_url: '/images/cross-ico.png',
      item_icon_alt: 'cross-ico',
      title: 'live_moderation_double_item_title_2',
      text: 'live_moderation_double_item_text_2'
    }
  ]
};

export const contentHowItWorks = {
  subtitle: 'conten_section_subtitle',
  title: 'live_how_works_title',
  text: 'live_how_works_text',
  bgColor: COLORS.BEIGE,
  image: '/images/live-diagram.png',
  image_2x: '/images/live-diagram.png, /images/2x/live-diagram@2x.png 2x',
  alt: 'diagram',
  href: '/#contactForm'
};

export const contentFeatureFocus = {
  subtitle: 'home_feature_focus_section_subtitle',
  title: 'home_feature_focus_section_title',
  text: 'live_feature_focus_section_text',
  bgColor: COLORS.DARK,
  image_2x: '/images/featureFocus/clock.png, /images/featureFocus/2x/clock@2x.png 2x',
  image: '/images/featureFocus/clock.png',
  alt: 'clock'
};

export const contentCard = {
  items: [
    {
      item_title: 'с_card_section_card_1_title',
      item_text: 'с_card_section_card_1_text',
      bgItemColor: COLORS.BEIGE
    },
    {
      item_title: 'с_card_section_card_2_title',
      item_text: 'с_card_section_card_2_text',
      bgItemColor: COLORS.BEIGE
    },
    {
      item_title: 'с_card_section_card_3_title',
      item_text: 'с_card_section_card_3_text',
      bgItemColor: COLORS.BEIGE
    }
  ]
};

export const contentDangerList = {
  title: 'live_danger_list_section_title',
  bgColor: COLORS.BEIGE,
  bgItemColor: COLORS.DANGER,
  items: [
    {
      item_image: '/images/performer-confirmation.png',
      item_alt: 'heath-icon',
      item_title: 'live_danger_list_1_title',
      item_text: 'live_danger_list_1_text'
    },
    {
      item_image: '/images/multi-performer-streams.png',
      item_alt: 'lock',
      item_title: 'live_danger_list_2_title',
      item_text: 'live_danger_list_2_text'
    },
    {
      item_image: '/images/continuous-monitoring.png',
      item_alt: 'check',
      item_title: 'live_danger_list_3_title',
      item_text: 'live_danger_list_3_text'
    }
  ]
};

export const liveStreamPageCarousel = {
  title: 'content_solution_carousel_title_content_live_stream_page',
  text: 'content_solution_carousel_text_content_live_stream_page',
  href: '/#contactForm',
  carousel: [
    {
      title: 'content_solution_carousel_item_1_title',
      text: 'content_solution_carousel_item_1_text',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse1.png, /images/solutionCarousel/2x/Ellipse1@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse1.png',
      href: '/content-provider'
    },
    {
      title: 'content_solution_carousel_item_2_title',
      text: 'content_solution_carousel_item_2_text',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse2.png, /images/solutionCarousel/2x/Ellipse2@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse2.png',
      href: '/content-moderation'
    },
    {
      title: 'content_solution_carousel_item_3_title',
      text: 'content_solution_carousel_item_3_text',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse6.png, /images/solutionCarousel/2x/Ellipse6@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse6.png',
      href: '/live-streaming'
    },
    {
      title: 'content_solution_carousel_item_4_title',
      text: 'content_solution_carousel_item_4_text',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse3.png, /images/solutionCarousel/2x/Ellipse3@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse3.png',
      href: '/complaint-resolution'
    },
    {
      title: 'content_solution_carousel_item_5_title',
      text: 'content_solution_carousel_item_5_text',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse4.png, /images/solutionCarousel/2x/Ellipse4@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse4.png',
      href: '/monthly-reporting'
    },
    {
      title: 'content_solution_carousel_item_6_title',
      text: 'content_solution_carousel_item_6_text',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse5.png, /images/solutionCarousel/2x/Ellipse5@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse5.png',
      href: '/quality-control'
    }
  ]
};
