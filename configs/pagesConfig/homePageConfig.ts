import { COLORS } from '../../const';
import { buttonsType } from '../../components/button';

export const homeHero = {
  title: 'home_hero_title',
  subtitle: 'home_hero_subtitle',
  subtitleBold: 'home_hero_title_bold',
  bgColor: COLORS.BURGUNDY,
  href: '/#contactForm',
  button: {
    type: buttonsType.largeDanger,
    label: 'Book a Demo',
    action: () => {
      console.log('action');
    }
  }
};

export const homeVerifyContent = {
  title: 'home_verification_content_section_title',
  text_1: 'home_verification_content_section_text_1',
  text_2: 'home_verification_content_section_text_2',
  bgColor: COLORS.DARK
};

export const homeSafeHands = {
  image: '/images/device.png',
  image_2x: '/images/device.png, /images/2x/device@2x.png 2x',
  alt: 'device',
  title: 'home_safe_hands_title',
  text: 'home_safe_hands_text',
  image_2: '/images/compatible-application.png',
  image_2_2x: '/images/compatible-application.png, /images/2x/compatible-application@2x.png 2x',
  alt_2: 'compatible-application',
  bgColor: COLORS.BEIGE,
  button: {
    type: buttonsType.largeDanger,
    label: 'Learn More',
    action: () => {
      console.log('action');
    }
  }
};

export const homeFeatureFocusMonthlyReporting = {
  subtitle: 'home_reporting_section_subtitle',
  title: 'home_reporting_section_title',
  text: 'home_reporting_section_text',
  bgColor: COLORS.DARK,
  href: '/monthly-reporting',
  button: {
    type: buttonsType.largeDanger,
    label: 'Learn More',
    action: () => {
      console.log('action');
    }
  },
  image_2x:
    '/images/featureFocus/monthly-reporting.png, /images/featureFocus/2x/monthly-reporting@2x.png 2x',
  image: '/images/featureFocus/monthly-reporting.png',
  alt: 'monthly-reporting'
};

export const homeNumber = {
  number: 'home_number_section_number',
  title: 'home_number_section_title',
  text: 'home_number_section_text',
  bgColor: COLORS.DANGER
};

export const homeCard = {
  title: 'home_card_section_title',
  text: 'home_card_section_text',
  bgColor: COLORS.BEIGE,
  items: [
    {
      item_title: 'home_card_section_card_1_title',
      item_text: 'home_card_section_card_1_text'
    },
    {
      item_title: 'home_card_section_card_2_title',
      item_text: 'home_card_section_card_2_text'
    },
    {
      item_title: 'home_card_section_card_3_title',
      item_text: 'home_card_section_card_3_text'
    }
  ]
};

export const homeFeatureFocus = {
  subtitle: 'home_feature_focus_section_subtitle',
  title: 'home_feature_focus_section_title',
  text: 'home_feature_focus_section_text',
  bgColor: COLORS.DARK,
  image_2x: '/images/featureFocus/clock.png, /images/featureFocus/2x/clock@2x.png 2x',
  image: '/images/featureFocus/clock.png',
  alt: 'clock'
};

export const homeMasterCard = {
  title: 'home_master_card_section_title',
  text: 'home_master_card_section_text',
  quote: 'home_master_card_section_quote',
  smallTitle: 'home_master_card_section_link_title',
  link: {
    label: 'home_master_card_section_link_label',
    src: 'https://www.mastercard.com/news/perspectives/2021/protecting-our-network-protecting-you-preventing-illegal-adult-content-on-our-network/'
  },
  bgColor: COLORS.BEIGE,
  listData: [
    {
      listTitle: 'home_master_card_section_list_1_title',
      listContent: 'home_master_card_section_list_1_content'
    },
    {
      listTitle: 'home_master_card_section_list_2_title',
      listContent: 'home_master_card_section_list_2_content'
    },
    {
      listTitle: 'home_master_card_section_list_3_title',
      listContent: 'home_master_card_section_list_3_content'
    },
    {
      listTitle: 'home_master_card_section_list_4_title',
      listContent: 'home_master_card_section_list_4_content'
    }
  ],
  href: '/#contactForm',
  button: {
    type: buttonsType.largeDark,
    label: 'button_label_get_in_touch',
    action: () => {
      console.log('action');
    }
  }
};

export const homeCarousel = {
  title: 'content_solution_carousel_title',
  text: 'content_solution_carousel_text',
  href: '/#contactForm',
  carousel: [
    {
      title: 'content_solution_carousel_item_1_title',
      text: 'content_solution_carousel_item_1_text',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse1.png, /images/solutionCarousel/2x/Ellipse1@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse1.png',
      href: '/content-provider'
    },
    {
      title: 'content_solution_carousel_item_2_title',
      text: 'content_solution_carousel_item_2_text',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse2.png, /images/solutionCarousel/2x/Ellipse2@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse2.png',
      href: '/content-moderation'
    },
    {
      title: 'content_solution_carousel_item_3_title',
      text: 'content_solution_carousel_item_3_text',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse6.png, /images/solutionCarousel/2x/Ellipse6@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse6.png',
      href: '/live-streaming'
    },
    {
      title: 'content_solution_carousel_item_4_title',
      text: 'content_solution_carousel_item_4_text',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse3.png, /images/solutionCarousel/2x/Ellipse3@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse3.png',
      href: '/complaint-resolution'
    },
    {
      title: 'content_solution_carousel_item_5_title',
      text: 'content_solution_carousel_item_5_text',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse4.png, /images/solutionCarousel/2x/Ellipse4@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse4.png',
      href: '/monthly-reporting'
    },
    {
      title: 'content_solution_carousel_item_6_title',
      text: 'content_solution_carousel_item_6_text',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse5.png, /images/solutionCarousel/2x/Ellipse5@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse5.png',
      href: '/quality-control'
    }
  ]
};
