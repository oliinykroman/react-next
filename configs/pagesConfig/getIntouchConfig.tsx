import { COLORS } from '../../const';
import { buttonsType } from '../../components/button';

export const getInTouch = {
  title: 'home_get_in_touch_title',
  text: 'home_get_in_touch_text',
  href: '/#contactForm',
  bgColor: COLORS.DANGER,
  button: {
    type: buttonsType.largeDark,
    label: 'button_label_get_in_touch',
    action: () => {
      console.log('action');
    }
  }
};
