import { COLORS } from '../../const';
import { buttonsType } from '../../components/button';
import PlusDanger from '../../svgComponent/PlusDanger';

export const contentModerationHero = {
  title: 'content_moderation_hero_title',
  subtitle: 'content_moderation_hero_subtitle',
  bgColor: COLORS.WHITE,
  sectionImg: '/images/heroImages/contentModeration.png',
  sectionImg_2x: '/images/heroImages/2x/contentModeration@2x.png',
  href: '/#contactForm',
  button: {
    type: buttonsType.largeDanger,
    label: 'Book a Demo',
    action: () => {
      console.log('action');
    }
  }
};

export const vmcMastercard = {
  title: 'mastercard_title',
  text: 'mastercard_text_1',
  bgColor: COLORS.LIGHT_GRAY_2
};

export const contentVerifyContentModeration = {
  title: 'c_verification_content_section_title',
  text_1: 'c_verification_content_section_text_2',
  bgColor: COLORS.DARK
};

export const doubleCardModeration = {
  title: 'p_moderation_double_card_title',
  text: 'p_moderation_double_card_text',
  bgColor: COLORS.WHITE,
  Icon: PlusDanger,
  cards: [
    { title: 'p_moderation_double_item_title_1', text: 'p_moderation_double_item_text_1' },
    { title: 'p_moderation_double_item_title_2', text: 'p_moderation_double_item_text_2' }
  ]
};

export const contentHowItWorksModeration = {
  subtitle: 'conten_section_moderation_page_subtitle',
  title: 'conten_section_moderation_page_title',
  text: 'conten_section_moderation_page_text',
  bgColor: COLORS.BEIGE,
  image_2x: '/images/diagram_moderation.png, /images/2x/diagram_moderation@2x.png 2x',
  image: '/images/diagram_moderation.png',
  alt: 'diagram',
  href: '/#contactForm'
};

export const contentFeatureModeration = {
  subtitle: 'c_feature_focus_section_subtitle_moderation_page',
  title: 'c_feature_focus_section_title_moderation_page',
  text: 'c_feature_focus_section_text_moderation_page',
  bgColor: COLORS.DARK,
  image_2x:
    '/images/featureFocus/feature_moderation.png, /images/featureFocus/2x/feature_moderation@2x.png 2x',
  image: '/images/featureFocus/feature_moderation.png',
  alt: 'verification'
};

export const contentModerationPageCarousel = {
  title: 'content_solution_carousel_title_content_moderation_page',
  text: 'content_solution_carousel_text_content_moderation_page',
  href: '/#contactForm',
  carousel: [
    {
      title: 'content_solution_carousel_item_1_title_content_moderation_page',
      text: 'content_solution_carousel_item_1_text_content_moderation_page',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse1.png, /images/solutionCarousel/2x/Ellipse1@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse1.png',
      href: '/content-provider'
    },
    {
      title: 'content_solution_carousel_item_2_title_content_moderation_page',
      text: 'content_solution_carousel_item_2_text_content_moderation_page',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse2.png, /images/solutionCarousel/2x/Ellipse2@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse2.png',
      href: '/content-moderation'
    },
    {
      title: 'content_solution_carousel_item_3_title_content_moderation_page',
      text: 'content_solution_carousel_item_3_text_content_moderation_page',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse6.png, /images/solutionCarousel/2x/Ellipse6@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse6.png',
      href: '/live-streaming'
    },
    {
      title: 'content_solution_carousel_item_4_title_content_moderation_page',
      text: 'content_solution_carousel_item_4_text_content_moderation_page',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse3.png, /images/solutionCarousel/2x/Ellipse3@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse3.png',
      href: '/complaint-resolution'
    },
    {
      title: 'content_solution_carousel_item_5_title_content_moderation_page',
      text: 'content_solution_carousel_item_5_text_content_moderation_page',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse4.png, /images/solutionCarousel/2x/Ellipse4@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse4.png',
      imageSrc: '/images/solutionCarousel/Ellipse4.png',
      href: '/monthly-reporting'
    },
    {
      title: 'content_solution_carousel_item_6_title_content_moderation_page',
      text: 'content_solution_carousel_item_6_text_content_moderation_page',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse5.png, /images/solutionCarousel/2x/Ellipse5@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse5.png',
      href: '/quality-control'
    }
  ]
};
