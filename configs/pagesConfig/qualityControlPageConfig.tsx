import { COLORS } from '../../const';
import { buttonsType } from '../../components/button';

export const qualityControlHero = {
  title: 'quality_control_hero_title',
  subtitle: 'quality_control_hero_subtitle',
  bgColor: COLORS.WHITE,
  sectionImg: '/images/heroImages/qualityControl.png',
  sectionImg_2x: '/images/heroImages/2x/qualityControl@2x.png',
  href: '/#contactForm',
  button: {
    type: buttonsType.largeDanger,
    label: 'Book a Demo',
    action: () => {
      console.log('action');
    }
  }
};

export const contentVerifyContent = {
  title: 'quality_control_verification_section_title',
  text_1: 'quality_control_verification_section_text_1',
  bgColor: COLORS.DARK
};

export const contentHowItWorks = {
  subtitle: 'quality_control_section_subtitle',
  title: 'quality_control_section_title',
  text: 'quality_control_section_text',
  text_2: 'quality_control_section_text_2',
  bgColor: COLORS.BEIGE,
  image_2x: '/images/diagram_quality_control.png, /images/2x/diagram_quality_control@2x.png 2x',
  image: '/images/diagram_quality_control.png',
  alt: 'diagram',
  href: '/#contactForm'
};

export const contentFeatureFocus = {
  subtitle: 'quality_control_feature_focus_subtitle',
  title: 'quality_control_feature_focus_title',
  text: 'quality_control_feature_focus_text',
  bgColor: COLORS.DARK,
  image_2x:
    '/images/featureFocus/measure-twice.png, /images/featureFocus/2x/measure-twice@2x.png 2x',
  image: '/images/featureFocus/measure-twice.png',
  alt: 'verification'
};

export const qualityControlPageCarousel = {
  title: 'content_solution_carousel_title_quality_control_page',
  text: 'content_solution_carousel_text_quality_control_page',
  href: '/#contactForm',
  carousel: [
    {
      title: 'content_solution_carousel_item_1_title',
      text: 'content_solution_carousel_item_1_text',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse1.png, /images/solutionCarousel/2x/Ellipse1@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse1.png',
      href: '/content-provider'
    },
    {
      title: 'content_solution_carousel_item_2_title',
      text: 'content_solution_carousel_item_2_text',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse2.png, /images/solutionCarousel/2x/Ellipse2@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse2.png',
      href: '/content-moderation'
    },
    {
      title: 'content_solution_carousel_item_3_title',
      text: 'content_solution_carousel_item_3_text',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse6.png, /images/solutionCarousel/2x/Ellipse6@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse6.png',
      href: '/live-streaming'
    },
    {
      title: 'content_solution_carousel_item_4_title',
      text: 'content_solution_carousel_item_4_text',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse3.png, /images/solutionCarousel/2x/Ellipse3@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse3.png',
      href: '/complaint-resolution'
    },
    {
      title: 'content_solution_carousel_item_5_title',
      text: 'content_solution_carousel_item_5_text',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse4.png, /images/solutionCarousel/2x/Ellipse4@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse4.png',
      href: '/monthly-reporting'
    },
    {
      title: 'content_solution_carousel_item_6_title',
      text: 'content_solution_carousel_item_6_text',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse5.png, /images/solutionCarousel/2x/Ellipse5@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse5.png',
      href: '/quality-control'
    }
  ]
};
