export enum privacyContentType {
  list,
  table,
  tableVertical,
  tableReverse,
  linked
}

export const privacyPageAsideList = {
  title: 'privacy_page_aside_title',
  subtitle: 'privacy_page_aside_subtitle',
  list: [
    { title: 'privacy_page_aside_list_1', href: '' },
    { title: 'privacy_page_aside_list_2', href: '' },
    { title: 'privacy_page_aside_list_3', href: '' },
    { title: 'privacy_page_aside_list_4', href: '' },
    { title: 'privacy_page_aside_list_5', href: '' },
    { title: 'privacy_page_aside_list_6', href: '' },
    { title: 'privacy_page_aside_list_7', href: '' },
    { title: 'privacy_page_aside_list_8', href: '' },
    { title: 'privacy_page_aside_list_9', href: '' },
    { title: 'privacy_page_aside_list_10', href: '' },
    { title: 'privacy_page_aside_list_11', href: '' },
    { title: 'privacy_page_aside_list_12', href: '' },
    { title: 'privacy_page_aside_list_13', href: '' },
    { title: 'privacy_page_aside_list_14', href: '' },
    { title: 'privacy_page_aside_list_15', href: '' }
  ]
};

export const privacyPageContent = [
  {
    title: 'privacy_page_content_title_0',
    content: 'privacy_page_content_inner_0'
  },
  {
    title: 'privacy_page_content_title_1',
    content: {
      type: privacyContentType.table,
      title: 'privacy_page_content_inner_1',
      table: [
        {
          title: 'privacy_page_content_table_title_1',
          text: 'privacy_page_content_table_text_1'
        },
        {
          title: 'privacy_page_content_table_title_2',
          text: 'privacy_page_content_table_text_2'
        },
        {
          title: 'privacy_page_content_table_title_3',
          text: 'privacy_page_content_table_text_3'
        }
      ]
    }
  },
  {
    title: 'privacy_page_content_title_2',
    content: 'privacy_page_content_inner_2'
  },
  {
    title: 'privacy_page_content_title_3',
    content: 'privacy_page_content_inner_3'
  },
  {
    title: 'privacy_page_content_title_4',
    content: 'privacy_page_content_inner_4'
  },
  {
    title: 'privacy_page_content_title_5',
    content: 'privacy_page_content_inner_5'
  },
  {
    title: 'privacy_page_content_title_6',
    content: {
      type: privacyContentType.table,
      title: 'privacy_page_content_inner_6',
      table: [
        {
          title: 'privacy_page_content_table_2_title_1',
          text: 'privacy_page_content_table_2_text_1'
        },
        {
          title: 'privacy_page_content_table_2_title_2',
          text: 'privacy_page_content_table_2_text_2'
        },
        {
          title: 'privacy_page_content_table_2_title_3',
          text: 'privacy_page_content_table_2_text_3'
        },
        {
          title: 'privacy_page_content_table_2_title_4',
          text: 'privacy_page_content_table_2_text_4'
        },
        {
          title: 'privacy_page_content_table_2_title_5',
          text: 'privacy_page_content_table_2_text_5'
        },
        {
          title: 'privacy_page_content_table_2_title_6',
          text: 'privacy_page_content_table_2_text_6'
        },
        {
          title: 'privacy_page_content_table_2_title_7',
          text: 'privacy_page_content_table_2_text_7'
        },
        {
          title: 'privacy_page_content_table_2_title_8',
          text: 'privacy_page_content_table_2_text_8'
        },
        {
          title: 'privacy_page_content_table_2_title_9',
          text: 'privacy_page_content_table_2_text_9'
        },
        {
          title: 'privacy_page_content_table_2_title_10',
          text: 'privacy_page_content_table_2_text_10'
        },
        {
          title: 'privacy_page_content_table_2_title_11',
          text: 'privacy_page_content_table_2_text_11'
        },
        {
          title: 'privacy_page_content_table_2_title_12',
          text: 'privacy_page_content_table_2_text_12'
        },
        {
          title: 'privacy_page_content_table_2_title_13',
          text: 'privacy_page_content_table_2_text_13'
        },
        {
          title: 'privacy_page_content_table_2_title_14',
          text: 'privacy_page_content_table_2_text_14'
        },
        {
          title: 'privacy_page_content_table_2_title_15',
          text: 'privacy_page_content_table_2_text_15'
        },
        {
          title: 'privacy_page_content_table_2_title_16',
          text: 'privacy_page_content_table_2_text_16'
        },
        {
          title: 'privacy_page_content_table_2_title_17',
          text: 'privacy_page_content_table_2_text_17'
        },
        {
          title: 'privacy_page_content_table_2_title_18',
          text: 'privacy_page_content_table_2_text_18'
        }
      ],
      table2: [
        {
          title: 'privacy_page_content_table_3_title_1',
          text: 'privacy_page_content_table_3_text_1'
        },
        {
          title: 'privacy_page_content_table_3_title_2',
          text: 'privacy_page_content_table_3_text_2'
        },
        {
          title: 'privacy_page_content_table_3_title_3',
          text: 'privacy_page_content_table_3_text_3'
        },
        {
          title: 'privacy_page_content_table_3_title_4',
          text: 'privacy_page_content_table_3_text_4'
        },
        {
          title: 'privacy_page_content_table_3_title_5',
          text: 'privacy_page_content_table_3_text_5'
        },
        {
          title: 'privacy_page_content_table_3_title_6',
          text: 'privacy_page_content_table_3_text_6'
        },
        {
          title: 'privacy_page_content_table_3_title_7',
          text: 'privacy_page_content_table_3_text_7'
        },
        {
          title: 'privacy_page_content_table_3_title_8',
          text: 'privacy_page_content_table_3_text_8'
        },
        {
          title: 'privacy_page_content_table_3_title_9',
          text: 'privacy_page_content_table_3_text_9'
        },
        {
          title: 'privacy_page_content_table_3_title_10',
          text: 'privacy_page_content_table_3_text_10'
        },
        {
          title: 'privacy_page_content_table_3_title_11',
          text: 'privacy_page_content_table_3_text_11'
        }
      ]
    }
  },
  {
    title: 'privacy_page_content_title_7',
    content: {
      type: privacyContentType.tableVertical,
      text: 'privacy_page_content_inner_7',
      table: [
        {
          text1: 'privacy_page_content_table_4_title_1',
          text2: 'privacy_page_content_table_4_title_2',
          text3: 'privacy_page_content_table_4_title_3'
        },
        {
          text1: 'privacy_page_content_table_4_col_1',
          text2: 'privacy_page_content_table_4_col_2',
          text3: 'privacy_page_content_table_4_col_3'
        },
        {
          text1: 'privacy_page_content_table_4_col_1_2',
          text2: 'privacy_page_content_table_4_col_2_2',
          text3: 'privacy_page_content_table_4_col_3_2'
        },
        {
          text1: 'privacy_page_content_table_4_col_1_3',
          text2: 'privacy_page_content_table_4_col_1_3',
          text3: 'privacy_page_content_table_4_col_1_3'
        },
        {
          text1: 'privacy_page_content_table_4_col_1_4',
          text2: 'privacy_page_content_table_4_col_1_4',
          text3: 'privacy_page_content_table_4_col_1_4'
        }
      ],
      contentText2: 'privacy_page_content_inner_7_2'
    }
  },
  {
    title: 'privacy_page_content_title_8',
    content: {
      type: privacyContentType.tableReverse,
      text: 'privacy_page_content_inner_8',
      contentText: 'privacy_page_content_inner_8_2',
      table: [
        {
          text1: 'privacy_page_content_table_5_title_1',
          text2: 'privacy_page_content_table_5_title_2'
        },
        {
          text1: 'privacy_page_content_table_5_text_1',
          text2: 'privacy_page_content_table_5_text_1_2',
          text3: 'privacy_page_content_table_5_text_1_1'
        },
        {
          text1: 'privacy_page_content_table_5_text_2',
          text2: 'privacy_page_content_table_5_text_2_2',
          text3: 'privacy_page_content_table_5_text_2_1'
        },
        {
          text1: 'privacy_page_content_table_5_text_3',
          text2: 'privacy_page_content_table_5_text_3_2',
          text3: 'privacy_page_content_table_5_text_3_1'
        },
        {
          text1: 'privacy_page_content_table_5_text_4',
          text2: 'privacy_page_content_table_5_text_4_2',
          text3: 'privacy_page_content_table_5_text_4_1'
        },
        {
          text1: 'privacy_page_content_table_5_text_5',
          text2: 'privacy_page_content_table_5_text_5_2',
          text3: 'privacy_page_content_table_5_text_5_1'
        },
        {
          text1: 'privacy_page_content_table_5_text_6',
          text2: 'privacy_page_content_table_5_text_6_2',
          text3: 'privacy_page_content_table_5_text_6_1'
        }
      ]
    }
  },
  {
    title: 'privacy_page_content_title_9',
    content: {
      type: privacyContentType.linked,
      text: 'privacy_page_content_inner_9',
      href: 'https://ec.europa.eu/info/law/law-topic/data-protection/data-transfers-outside-eu/model-contracts-transfer-personal-data-third-countries_en'
    }
  },
  {
    title: 'privacy_page_content_title_10',
    content: 'privacy_page_content_inner_10'
  },
  {
    title: 'privacy_page_content_title_11',
    content: 'privacy_page_content_inner_11'
  },
  {
    title: 'privacy_page_content_title_12',
    content: {
      type: privacyContentType.linked,
      text: 'privacy_page_content_inner_12',
      href: 'https://www.aboutcookies.org.uk/'
    }
  },
  {
    title: 'privacy_page_content_title_13',
    content: 'privacy_page_content_inner_13'
  },
  {
    title: 'privacy_page_content_title_14',
    content: {
      type: privacyContentType.linked,
      text: 'privacy_page_content_inner_14',
      href: 'mailto:info@verifymycontent.com'
    }
  },
  {
    title: 'privacy_page_content_title_15',
    content: {
      type: privacyContentType.linked,
      text: 'privacy_page_content_inner_15'
    }
  }
];
