import { COLORS } from '../../const';
import { buttonsType } from '../../components/button';

export const monthlyReportingHero = {
  title: 'monthly_reporting_hero_title',
  subtitle: 'monthly_reporting_hero_subtitle',
  bgColor: COLORS.WHITE,
  sectionImg: '/images/heroImages/mounthlyReporting.png',
  sectionImg_2x: '/images/heroImages/2x/mounthlyReporting@2x.png',
  button: {
    type: buttonsType.largeDanger,
    label: 'Book a Demo',
    action: () => {
      console.log('action');
    }
  }
};

export const contentMastercard = {
  title: 'mastercard_title',
  text: 'mastercard_text_4',
  bgColor: COLORS.BEIGE
};

export const contentVerifyContent = {
  title: 'c_verification_content_section_title',
  text_1: 'c_verification_content_section_text_5',
  bgColor: COLORS.DARK
};

export const contentMonthlyReportsDetail = {
  bgColor: COLORS.BEIGE,
  text: 'monthly_reports_section_text',
  list_title: 'monthly_reports_section_list_title',
  list: [
    'monthly_reports_list_item_1',
    'monthly_reports_list_item_2',
    'monthly_reports_list_item_3',
    'monthly_reports_list_item_4',
    'monthly_reports_list_item_5'
  ],
  image: '/images/monthly-reports-detail.png',
  image_2x: '/images/monthly-reports-detail.png, /images/2x/monthly-reports-detail@2x.png 2x',
  alt: 'evidence-admin'
};

export const contentFeatureFocus = {
  subtitle: 'monthly_reporting_focus_section_subtitle',
  title: 'monthly_reporting_focus_section_title',
  text: 'monthly_reporting_focus_section_text',
  bgColor: COLORS.DARK,
  image_2x:
    '/images/featureFocus/evidence-admin.png, /images/featureFocus/2x/evidence-admin@2x.png 2x',
  image: '/images/featureFocus/evidence-admin.png',
  alt: 'evidence-admin'
};

export const reportDetailPageCarousel = {
  title: 'content_solution_carousel_title_monthly_reporting_page',
  text: 'content_solution_carousel_text_content_monthly_reporting_page',
  href: '/#contactForm',
  carousel: [
    {
      title: 'content_solution_carousel_item_1_title',
      text: 'content_solution_carousel_item_1_text',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse1.png, /images/solutionCarousel/2x/Ellipse1@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse1.png',
      href: '/content-provider'
    },
    {
      title: 'content_solution_carousel_item_2_title',
      text: 'content_solution_carousel_item_2_text',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse2.png, /images/solutionCarousel/2x/Ellipse2@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse2.png',
      href: '/content-moderation'
    },
    {
      title: 'content_solution_carousel_item_3_title',
      text: 'content_solution_carousel_item_3_text',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse6.png, /images/solutionCarousel/2x/Ellipse6@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse6.png',
      href: '/live-streaming'
    },
    {
      title: 'content_solution_carousel_item_4_title',
      text: 'content_solution_carousel_item_4_text',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse3.png, /images/solutionCarousel/2x/Ellipse3@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse3.png',
      href: '/complaint-resolution'
    },
    {
      title: 'content_solution_carousel_item_5_title',
      text: 'content_solution_carousel_item_5_text',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse4.png, /images/solutionCarousel/2x/Ellipse4@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse4.png',
      href: '/monthly-reporting'
    },
    {
      title: 'content_solution_carousel_item_6_title',
      text: 'content_solution_carousel_item_6_text',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse5.png, /images/solutionCarousel/2x/Ellipse5@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse5.png',
      href: '/quality-control'
    }
  ]
};
