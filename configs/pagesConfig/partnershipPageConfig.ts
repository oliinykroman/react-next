import { COLORS } from '../../const';
import { buttonsType } from '../../components/button';

const carouselIndustry = [
  {
    title: 'p_partnership_carousel_industry_title_item_1',
    subtitle: 'p_partnership_carousel_industry_sub_title_item_1',
    text: 'p_partnership_carousel_industry_text_item_1',
    imageSrc: '/images/partnershipLogo/CCBill_logo.svg'
  },
  {
    title: 'p_partnership_carousel_industry_title_item_2',
    subtitle: 'p_partnership_carousel_industry_sub_title_item_2',
    text: 'p_partnership_carousel_industry_text_item_2',
    imageSrc: '/images/partnershipLogo/SEGPAY_colour.svg'
  },
  {
    title: 'p_partnership_carousel_industry_title_item_3',
    subtitle: 'p_partnership_carousel_industry_sub_title_item_3',
    text: 'p_partnership_carousel_industry_text_item_3',
    imageSrc: '/images/partnershipLogo/Vendo.svg'
  },
  {
    title: 'p_partnership_carousel_industry_title_item_4',
    subtitle: 'p_partnership_carousel_industry_sub_title_item_4',
    text: 'p_partnership_carousel_industry_text_item_4',
    imageSrc: '/images/partnershipLogo/NextStep.svg'
  },
  {
    title: 'p_partnership_carousel_industry_title_item_5',
    subtitle: 'p_partnership_carousel_industry_sub_title_item_5',
    text: 'p_partnership_carousel_industry_text_item_5',
    imageSrc: '/images/partnershipLogo/HiveLogo.svg'
  },
  {
    title: 'p_partnership_carousel_industry_title_item_6',
    subtitle: 'p_partnership_carousel_industry_sub_title_item_6',
    text: 'p_partnership_carousel_industry_text_item_6',
    imageSrc: '/images/partnershipLogo/paysafe_colour.svg'
  }
];

const carouselOrganisation = [
  {
    title: 'p_partnership_carousel_organisation_title_item_1',
    subtitle: 'p_partnership_carousel_organisation_sub_title_item_1',
    text: 'p_partnership_carousel_organisation_text_item_1',
    imageSrc: '/images/partnershipLogo/pineapple-logo-horizontal.png'
  },
  {
    title: 'p_partnership_carousel_organisation_title_item_2',
    subtitle: 'p_partnership_carousel_organisation_sub_title_item_2',
    text: 'p_partnership_carousel_organisation_text_item_2',
    imageSrc: '/images/partnershipLogo/CC_Logo.png'
  },
  {
    title: 'p_partnership_carousel_organisation_title_item_3',
    subtitle: 'p_partnership_carousel_organisation_sub_title_item_3',
    text: 'p_partnership_carousel_organisation_text_item_3',
    imageSrc: '/images/partnershipLogo/asacp.png'
  },
  {
    title: 'p_partnership_carousel_organisation_title_item_4',
    subtitle: 'p_partnership_carousel_organisation_sub_title_item_4',
    text: 'p_partnership_carousel_organisation_text_item_4',
    imageSrc: '/images/partnershipLogo/5rights.png'
  },
  {
    title: 'p_partnership_carousel_organisation_title_item_5',
    subtitle: 'p_partnership_carousel_organisation_sub_title_item_5',
    text: 'p_partnership_carousel_organisation_text_item_5',
    imageSrc: '/images/partnershipLogo/nspcc-online-press-logo.png'
  }
];

export const partnershipHero = {
  title: 'p_partnership_hero_title',
  subtitle: 'p_partnership_hero_text',
  bgColor: COLORS.WHITE,
  sectionImg_2x: '/images/heroImages/partnership.png, images/heroImages/2x/partnership@2x.png 2x',
  sectionImg: '/images/heroImages/partnership.png',
  href: '/#contactForm',
  button: {
    type: buttonsType.largeDanger,
    label: 'Book a Demo',
    action: () => {
      console.log('action');
    }
  }
};

export const verifyContentPartnership = {
  title: 'home_verification_content_section_title',
  text_1: 'c_verification_content_section_text_7',
  bgColor: COLORS.DARK
};

export const partnershipCarouselIndustry = {
  title: 'p_partnership_carousel_industry_title',
  bgColor: COLORS.WHITE,
  carousel: carouselIndustry
};

export const partnershipCarouselOrganisation = {
  title: 'p_partnership_carousel_organisation_title',
  bgColor: COLORS.LIGHT_GRAY_2,
  carousel: carouselOrganisation
};
