import { COLORS } from '../../const';
import { buttonsType } from '../../components/button';

export const complaintResolutionHero = {
  title: 'content_resolution_hero_title',
  subtitle: 'content_resolution_hero_subtitle',
  bgColor: COLORS.WHITE,
  sectionImg: '/images/heroImages/complainResolution.png',
  sectionImg_2x: '/images/heroImages/2x/complainResolution@2x.png',
  href: '/#contactForm',
  button: {
    type: buttonsType.largeDanger,
    label: 'Book a Demo',
    action: () => {
      console.log('action');
    }
  }
};

export const vmcMastercardResolutionPage = {
  title: 'mastercard_title',
  text: 'mastercard_text_3',
  bgColor: COLORS.LIGHT_GRAY_2
};

export const contentVerifyContentResolutionPage = {
  title: 'c_verification_content_section_title',
  text_1: 'c_verification_content_section_text_4',
  bgColor: COLORS.DARK
};

export const contentHowItWorksResolutionPage = {
  subtitle: 'conten_section_resolution_page_subtitle',
  title: 'conten_section_resolution_page_title',
  text: 'conten_section_resolution_page_text',
  bgColor: COLORS.BEIGE,
  image_2x: '/images/diagram_resolution.png, /images/2x/diagram_resolution@2x.png 2x',
  image: '/images/diagram_resolution.png',
  alt: 'diagram',
  contentText: 'content_section_resolution_page_text_content',
  href: '/#contactForm'
};

export const contentFeatureResolutionPage = {
  subtitle: 'c_feature_focus_section_subtitle_resolution_page',
  title: 'c_feature_focus_section_title_resolution_page',
  text: 'c_feature_focus_section_text_resolution_page',
  bgColor: COLORS.DARK,
  image_2x:
    '/images/featureFocus/circle_resolution.png, /images/featureFocus/2x/circle_resolution@2x.png 2x',
  image: '/images/featureFocus/circle_resolution.png',
  alt: 'verification'
};

export const resolutionPageCarousel = {
  title: 'content_solution_carousel_title_resolution_page',
  text: 'content_solution_carousel_text_resolution_page',
  href: '/#contactForm',
  carousel: [
    {
      title: 'content_solution_carousel_item_1_title',
      text: 'content_solution_carousel_item_1_text',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse1.png, /images/solutionCarousel/2x/Ellipse1@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse1.png',
      href: '/content-provider'
    },
    {
      title: 'content_solution_carousel_item_2_title',
      text: 'content_solution_carousel_item_2_text',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse2.png, /images/solutionCarousel/2x/Ellipse2@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse2.png',
      href: '/content-moderation'
    },
    {
      title: 'content_solution_carousel_item_3_title',
      text: 'content_solution_carousel_item_3_text',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse6.png, /images/solutionCarousel/2x/Ellipse6@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse6.png',
      href: '/live-streaming'
    },
    {
      title: 'content_solution_carousel_item_4_title',
      text: 'content_solution_carousel_item_4_text',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse3.png, /images/solutionCarousel/2x/Ellipse3@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse3.png',
      href: '/complaint-resolution'
    },
    {
      title: 'content_solution_carousel_item_5_title',
      text: 'content_solution_carousel_item_5_text',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse4.png, /images/solutionCarousel/2x/Ellipse4@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse4.png',
      href: '/monthly-reporting'
    },
    {
      title: 'content_solution_carousel_item_6_title',
      text: 'content_solution_carousel_item_6_text',
      imageSrc_2x:
        '/images/solutionCarousel/Ellipse5.png, /images/solutionCarousel/2x/Ellipse5@2x.png 2x',
      imageSrc: '/images/solutionCarousel/Ellipse5.png',
      href: '/quality-control'
    }
  ]
};
