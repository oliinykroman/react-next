FROM node:buster-slim

RUN apt-get update && apt-get install nginx -y

WORKDIR /app

COPY ./package.json ./

COPY . .

RUN npm install

EXPOSE 3000

CMD [ "yarn", "start" ]