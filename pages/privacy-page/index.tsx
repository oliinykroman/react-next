import React from 'react';
import styled from 'styled-components';
import Container from '../../components/grid/container';
import Row from '../../components/grid/row';
import { GRID_COLUMN_PADDING, SIZES_MIN } from '../../const';
import PrivacyAside from '../../components/privacyAside';
import {
  privacyPageAsideList,
  privacyPageContent
} from '../../configs/pagesConfig/privacyPageConfig';
import PrivacyContent from '../../components/privacyContent';
import StickyBox from 'react-sticky-box';

const StyledColumnLg = styled.div`
  margin-top: 20px;
  padding: 0 ${GRID_COLUMN_PADDING};
  flex: 0 0 100%;

  @media ${SIZES_MIN.lg} {
    flex: 0 0 66.5555%;
  }
`;

const nativeStyle = `align-items: flex-start;`;

const PrivacyPage = () => {
  return (
    <Container>
      <Row nativeStyles={nativeStyle}>
        <StickyBox className={'stickyBox'}>
          <PrivacyAside data={privacyPageAsideList} />
        </StickyBox>
        <StyledColumnLg>
          <PrivacyContent data={privacyPageContent} />
        </StyledColumnLg>
      </Row>
    </Container>
  );
};

export default PrivacyPage;
