import React from 'react';
import styled from 'styled-components';
import { COLORS } from '../../../../const';
import { useIntl } from 'react-intl';

const StyledList = styled.ul`
  display: flex;
  flex-direction: column;
  li {
    margin-bottom: 10px;
    position: relative;
    padding-left: 10px;

    &:after {
      content: '';
      width: 2px;
      height: 2px;
      background: ${COLORS.DARK};
      position: absolute;
      left: 0;
      top: 5px;
    }
  }
`;
const StyledP = styled.p`
  font-style: normal;
  font-weight: 400;
  font-size: 18px;
  line-height: 1.35;
  padding: 0 0 35px;
`;

const PrivacyContentList = (props: { list: any }) => {
  const { list } = props;
  const intl = useIntl();

  if (!list) return null;

  return (
    <>
      <StyledP>{intl.formatMessage({ id: list.title })}</StyledP>
      <StyledList>
        {list?.content?.map((item, index) => {
          return <li key={`li_${index}`}>{intl.formatMessage({ id: item.content })}</li>;
        })}
      </StyledList>
    </>
  );
};

export default PrivacyContentList;
