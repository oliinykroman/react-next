import React from 'react';
import styled from 'styled-components';
import { COLORS } from '../../../../const';
import { useIntl } from 'react-intl';
import ParagraphNormal from '../../../../components/typografy/paragraphNormal';
import { privacyContentType } from '../../../../configs/pagesConfig/privacyPageConfig';

const StyledList = styled.ul`
  display: flex;
  flex-direction: column;
  li {
    margin-bottom: 10px;
    position: relative;
    padding-left: 10px;

    &:after {
      content: '';
      width: 2px;
      height: 2px;
      background: ${COLORS.DARK};
      position: absolute;
      left: 0;
      top: 5px;
    }
  }
`;
const StyledP = styled.p`
  font-style: normal;
  font-weight: 400;
  font-size: 18px;
  line-height: 1.35;
  padding: 0 0 35px;
`;

const StyledWrapper = styled.div`
  display: flex;
  flex-direction: column;
  background: ${COLORS.WHITE};
  box-shadow: 0 2px 6px rgb(0 0 0 / 15%);
  border-radius: 12px;
  border: 1px solid ${COLORS.DARK};
  margin-bottom: 30px;
  min-width: 500px;
  overflow-x: auto;
  & > div {
    border-bottom: 1px solid ${COLORS.DARK};

    &:last-child {
      border-bottom: none;
    }
  }
`;

const StyledRow = styled.div`
  max-width: 910px;
  display: flex;
  &.strong {
    font-weight: 600;
    font-family: 'codec_proextrabold';
  }
`;

const StyledCol = styled.div`
  display: flex;
  align-items: flex-start;
  flex: 0 0 33.3333%;
  padding: 10px;
  font-style: normal;
  font-weight: 400;
  font-size: 18px;
  line-height: 1.35;
  white-space: pre-line;
  border-right: 1px solid ${COLORS.DARK};
  &:last-child {
    border-right: none;
  }
`;

const StyledTable = styled.div`
  width: 100%;
  max-width: calc(100vw - 60px);
  overflow-x: auto;
`;

type contentType = {
  text: string;
  type: privacyContentType;
  table: { text1: string; text2: string; text3: string }[];
  contentText?: string;
  contentText2?: string;
};

const PrivacyContentTableVertical = (props: { list: contentType }) => {
  const { list } = props;
  const intl = useIntl();

  if (!list) return null;

  return (
    <>
      <StyledRow>
        <ParagraphNormal>{intl.formatMessage({ id: list.text })}</ParagraphNormal>
      </StyledRow>
      <StyledTable>
        <StyledWrapper>
          {list.table.map((item, index) => {
            return (
              <StyledRow key={`item_${index}`} className={`${index === 0 ? 'strong' : ''}`}>
                <StyledCol>{intl.formatMessage({ id: item.text1 })}</StyledCol>
                <StyledCol>{intl.formatMessage({ id: item.text2 })}</StyledCol>
                <StyledCol>{intl.formatMessage({ id: item.text3 })}</StyledCol>
              </StyledRow>
            );
          })}
        </StyledWrapper>
      </StyledTable>

      {list.contentText && (
        <StyledRow>
          <ParagraphNormal>{intl.formatMessage({ id: list.contentText })}</ParagraphNormal>
        </StyledRow>
      )}
      {list.contentText2 && (
        <StyledRow>
          <ParagraphNormal>{intl.formatMessage({ id: list.contentText2 })}</ParagraphNormal>
        </StyledRow>
      )}
    </>
  );
};

export default PrivacyContentTableVertical;
