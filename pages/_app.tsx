import React from 'react';
import '@nextcss/reset';
import GlobalStyle from '../styles/globalStyles';
import { IntlProvider } from 'react-intl';
import en from '../public/locales/en/comon_en.json';
import fr from '../public/locales/fr/comon_fr.json';
import { useRouter } from 'next/router';
import Header from '../components/header';
import Footer from '../components/footer';
import SectionGetInTouch from '../components/sectionGetInTouch';
import { getInTouch } from '../configs/pagesConfig/getIntouchConfig';

const messages = { en, fr };

export default function App({ Component, pageProps }) {
  const { locale } = useRouter();
  return (
    <IntlProvider locale={locale} messages={messages[locale]}>
      <Header {...pageProps} />
      <Component {...pageProps} />
      <SectionGetInTouch data={getInTouch} {...pageProps} />
      <Footer {...pageProps} />
      <GlobalStyle key={'globalStyles'} />
    </IntlProvider>
  );
}
