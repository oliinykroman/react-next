import React from 'react';
import Image from 'next/image';
import SectionHero from '../../components/sectionHero';
import {
  qualityControlHero,
  contentVerifyContent,
  contentHowItWorks,
  contentFeatureFocus,
  qualityControlPageCarousel
} from '../../configs/pagesConfig/qualityControlPageConfig';
import SectionVerifyContent from '../../components/sectionVerifyContent';
import SectionHowItWorks from '../../components/sectionHowItWorks';
import SectionFeatureFocus from '../../components/sectionFeatureFocus';
import SectionCarousel from '../../components/sectionCarousel';
import styled from 'styled-components';
import { SIZES } from '../../const';
const Wrapper = styled.div`
  @media ${SIZES.lg} {
    margin-bottom: 50px;
  }
`;

const QualityControl = () => {
  return (
    <>
      <SectionHero data={qualityControlHero} imageBackground={false}>
        <Image
          src={'/images/home-hero-bg.svg'}
          objectFit="cover"
          objectPosition="center"
          layout="fill"
        />
      </SectionHero>
      <SectionVerifyContent data={contentVerifyContent} imageBackground={true}>
        <Image
          src={'/images/dark-bg.svg'}
          objectFit="cover"
          objectPosition="center"
          layout="fill"
        />
      </SectionVerifyContent>
      <SectionHowItWorks data={contentHowItWorks} />
      <SectionFeatureFocus data={contentFeatureFocus} imageBackground={true}>
        <Image
          src={'/images/dark-bg.svg'}
          objectFit="cover"
          objectPosition="center"
          layout="fill"
        />
      </SectionFeatureFocus>
      <Wrapper>
        <SectionCarousel data={qualityControlPageCarousel} />
      </Wrapper>
    </>
  );
};

export default QualityControl;
