import React from 'react';
import Image from 'next/image';
import SectionHero from '../../components/sectionHero';
import SectionMastercard from '../../components/sectionMastercard';
import {
  complaintResolutionHero,
  contentFeatureResolutionPage,
  contentHowItWorksResolutionPage,
  contentVerifyContentResolutionPage,
  resolutionPageCarousel,
  vmcMastercardResolutionPage
} from '../../configs/pagesConfig/complaintResolutionPageConfig';
import SectionVerifyContent from '../../components/sectionVerifyContent';
import SectionHowItWorks from '../../components/sectionHowItWorks';
import SectionFeatureFocus from '../../components/sectionFeatureFocus';
import { contentDangerList } from '../../configs/pagesConfig/liveStreamingPageConfig';
import DangerListSection from '../../components/sectionDangerList';
import SectionCarousel from '../../components/sectionCarousel';

const ComplaintResolution = () => {
  return (
    <>
      <SectionHero data={complaintResolutionHero} imageBackground={false}>
        <Image
          src={'/images/home-hero-bg.svg'}
          objectFit="cover"
          objectPosition="center"
          layout="fill"
        />
      </SectionHero>
      <SectionMastercard data={vmcMastercardResolutionPage} />
      <SectionVerifyContent
        // @ts-ignore
        data={contentVerifyContentResolutionPage}
        imageBackground={true}>
        <Image
          src={'/images/dark-bg.svg'}
          objectFit="cover"
          objectPosition="center"
          layout="fill"
        />
      </SectionVerifyContent>
      <SectionHowItWorks data={contentHowItWorksResolutionPage} contentSection={true} />
      <SectionFeatureFocus data={contentFeatureResolutionPage} imageBackground={true}>
        <Image
          src={'/images/dark-bg.svg'}
          objectFit="cover"
          objectPosition="center"
          layout="fill"
        />
      </SectionFeatureFocus>
      <DangerListSection data={contentDangerList} />
      <SectionCarousel data={resolutionPageCarousel} />
    </>
  );
};

export default ComplaintResolution;
