import React from 'react';
import Image from 'next/image';
import {
  liveStreamingHero,
  contentMastercard,
  contentVerifyContent,
  contentHowItWorks,
  contentFeatureFocus,
  contentCard,
  contentDangerList,
  doubleCardLiveStream,
  liveStreamPageCarousel
} from '../../configs/pagesConfig/liveStreamingPageConfig';
import SectionHero from '../../components/sectionHero';
import SectionMastercard from '../../components/sectionMastercard';
import SectionVerifyContent from '../../components/sectionVerifyContent';
import { contentModerationConfig } from '../../configs/pagesConfig/contentModerationConfig';
import SectionContentModeration from '../../components/sectionContentModeration';
import SectionFeatureFocus from '../../components/sectionFeatureFocus';
import SectionHowItWorks from '../../components/sectionHowItWorks';
import CardSection from '../../components/sectionCard';
import DangerListSection from '../../components/sectionDangerList';
import { contentReviewControlConfig } from '../../configs/contentReviewControlConfig';
import SectionContentReviewControl from '../../components/sectionContentReviewControl';
import SectionDoubleCart from '../../components/sectionDoubleCart';
import SectionCarousel from '../../components/sectionCarousel';

const LiveStreaming = () => {
  return (
    <>
      <SectionHero data={liveStreamingHero} imageBackground={false}>
        <Image
          src={'/images/home-hero-bg.svg'}
          objectFit="cover"
          objectPosition="center"
          layout="fill"
        />
      </SectionHero>
      <SectionMastercard data={contentMastercard} />
      <SectionVerifyContent data={contentVerifyContent} imageBackground={true}>
        <Image
          src={'/images/dark-bg.svg'}
          objectFit="cover"
          objectPosition="center"
          layout="fill"
        />
      </SectionVerifyContent>

      <SectionDoubleCart data={doubleCardLiveStream} />

      <SectionContentReviewControl data={contentReviewControlConfig} />
      <SectionContentModeration data={contentModerationConfig} />
      <SectionHowItWorks data={contentHowItWorks} />
      <SectionFeatureFocus data={contentFeatureFocus} imageBackground={true}>
        <Image
          src={'/images/dark-bg.svg'}
          objectFit="cover"
          objectPosition="center"
          layout="fill"
        />
      </SectionFeatureFocus>
      <CardSection
        // @ts-ignore
        data={contentCard}
      />
      <DangerListSection data={contentDangerList} />

      <SectionCarousel data={liveStreamPageCarousel} />
    </>
  );
};

export default LiveStreaming;
