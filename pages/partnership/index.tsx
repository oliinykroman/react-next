import React from 'react';
import Image from 'next/image';
import SectionHero from '../../components/sectionHero';
import {
  verifyContentPartnership,
  partnershipHero,
  partnershipCarouselIndustry,
  partnershipCarouselOrganisation
} from '../../configs/pagesConfig/partnershipPageConfig';
import SectionVerifyContent from '../../components/sectionVerifyContent';
import PartnershipCarousel from '../../components/pagesComponent/partnershipPage';
import { COLORS } from '../../const';

const PartnershipPage = () => {
  return (
    <>
      <SectionHero data={partnershipHero} imageBackground={false}>
        <Image
          src={'/images/home-hero-bg.svg'}
          objectFit="cover"
          objectPosition="center"
          layout="fill"
        />
      </SectionHero>
      <SectionVerifyContent data={verifyContentPartnership} imageBackground={true}>
        <Image
          src={'/images/Overview.svg'}
          objectFit="cover"
          objectPosition="center"
          layout="fill"
        />
      </SectionVerifyContent>
      <PartnershipCarousel
        // @ts-ignore
        data={partnershipCarouselIndustry}
        cardCustomBg={COLORS.LIGHT_GRAY_2}
      />
      <PartnershipCarousel
        // @ts-ignore
        data={partnershipCarouselOrganisation}
        titleMinHeight
      />
    </>
  );
};

export default PartnershipPage;
