import Document, { Head, Html, Main, NextScript } from 'next/document';
import React from 'react';
class AppDocument extends Document {
  render() {
    return (
      <Html>
        <Head>
          <meta
            name="description"
            content="VerifyMyContent is the complete identity and content verification solution you need to safeguard your online business. Discover our compliant identity verification solution here."
          />
          <meta property="og:title" content="VerifyMyContent Identity Verification" />
          <meta
            property="og:description"
            content="VerifyMyContent is the complete identity and content verification solution you need to safeguard your online business. Discover our compliant identity verification solution here."
          />
          <meta property="og:image" content="/og-verifymycontent.jpg" />
          <meta property="og:url" content="https://verifymycontent.com/" />
          <meta property="og:locale" content="en_GB" />
          <meta property="og:type" content="website" />
          <link rel="icon" href="/favicon.ico" />

          <link rel="preconnect" href="https://fonts.googleapis.com" />
          <link
            rel="preconnect"
            href="https://fonts.gstatic.com"
            // @ts-ignore
            crossOrigin
          />
          <link
            href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@700&display=swap"
            rel="stylesheet"
          />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default AppDocument;
