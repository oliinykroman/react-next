import { useEffect } from 'react';
import SectionHero from '../components/sectionHero';
import Image from 'next/image';
import {
  homeCard,
  homeFeatureFocus,
  homeFeatureFocusMonthlyReporting,
  homeHero,
  homeNumber,
  homeMasterCard,
  homeSafeHands,
  homeVerifyContent,
  homeCarousel
} from '../configs/pagesConfig/homePageConfig';
import ContactFormSection from '../components/pagesComponent/homePage/contactFormSection';
import PartnerShipSection from '../components/pagesComponent/homePage/partnerShipSection';
import SectionVerifyContent from '../components/sectionVerifyContent';
import SectionFeatureFocus from '../components/sectionFeatureFocus';
import NumberSection from '../components/pagesComponent/homePage/numberSection';
import SectionCarousel from '../components/sectionCarousel';
import CardSection from '../components/sectionCard';
import MasterCardOverview from '../components/pagesComponent/homePage/masterCardOverview';
import SafeHandsSection from '../components/pagesComponent/homePage/safeHandsSection';
import SectionContentModeration from '../components/sectionContentModeration';
import { contentModerationConfig } from '../configs/pagesConfig/contentModerationConfig';
import styled from 'styled-components';
import { SIZES, SIZES_MIN } from '../const';

const StyledImage = styled.div`
  @media ${SIZES.lg} {
    display: none;
  }
`;
const StyledImageSm = styled.div`
  display: block;
  position: absolute;
  width: auto;
  top: 61px;
  left: 44px;

  @media ${SIZES_MIN.m} {
    display: none;
  }
`;

const smPadding = `
  @media ${SIZES.m} {
    padding: 108px 0 60px;
  }
`;

const Home = () => {
  useEffect(() => {
    document.body.classList.add('home-page');
    return () => {
      document.body.classList.remove('home-page');
    };
  }, []);
  return (
    <>
      <SectionHero data={homeHero} imageBackground={true} customStyle={smPadding}>
        <>
          <StyledImageSm>
            <img src="/images/LogoPowered.svg" alt="verify" />
          </StyledImageSm>
          <StyledImage>
            <Image
              src={'/images/home-hero-bg.svg'}
              objectFit="cover"
              objectPosition="center"
              layout="fill"
            />
          </StyledImage>
        </>
      </SectionHero>
      <ContactFormSection />
      <PartnerShipSection />
      <MasterCardOverview data={homeMasterCard} />

      <SectionVerifyContent data={homeVerifyContent} imageBackground={true}>
        <Image
          src={'/images/Overview.svg'}
          objectFit="cover"
          objectPosition="center"
          layout="fill"
        />
      </SectionVerifyContent>
      <SectionCarousel data={homeCarousel} />
      <SectionContentModeration data={contentModerationConfig} />
      <SafeHandsSection data={homeSafeHands} />

      <SectionFeatureFocus data={homeFeatureFocusMonthlyReporting} imageBackground={true}>
        <Image
          src={'/images/dark-bg.svg'}
          objectFit="cover"
          objectPosition="center"
          layout="fill"
        />
      </SectionFeatureFocus>

      <NumberSection data={homeNumber} />

      <CardSection data={homeCard} />

      <SectionFeatureFocus data={homeFeatureFocus} imageBackground={true}>
        <Image
          src={'/images/dark-bg.svg'}
          objectFit="cover"
          objectPosition="center"
          layout="fill"
        />
      </SectionFeatureFocus>
    </>
  );
};

export default Home;
