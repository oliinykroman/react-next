import React from 'react';
import Image from 'next/image';
import {
  contentProviderHero,
  contentCard,
  contentDangerList,
  contentFeatureFocus,
  contentHowItWorks,
  contentMastercard,
  contentVerifyContent,
  contentVerticalList,
  doubleCardProvider,
  contentProviderPageCarousel
} from '../../configs/pagesConfig/contentProviderPageConfig';
import SectionHero from '../../components/sectionHero';
import SectionMastercard from '../../components/sectionMastercard';
import SectionVerifyContent from '../../components/sectionVerifyContent';
import SectionHowItWorks from '../../components/sectionHowItWorks';
import SectionFeatureFocus from '../../components/sectionFeatureFocus';
import CardSection from '../../components/sectionCard';
import DangerListSection from '../../components/sectionDangerList';
import VerticalListSection from '../../components/sectionVerticalList';
import SectionDoubleCart from '../../components/sectionDoubleCart';
import SectionCarousel from '../../components/sectionCarousel';

const ContentProvider = () => {
  return (
    <>
      <SectionHero data={contentProviderHero} imageBackground={false}>
        <Image
          src={'/images/home-hero-bg.svg'}
          objectFit="cover"
          objectPosition="center"
          layout="fill"
        />
      </SectionHero>
      <SectionMastercard data={contentMastercard} />
      <SectionVerifyContent data={contentVerifyContent} imageBackground={true}>
        <Image
          src={'/images/dark-bg.svg'}
          objectFit="cover"
          objectPosition="center"
          layout="fill"
        />
      </SectionVerifyContent>
      <SectionDoubleCart data={doubleCardProvider} />
      <SectionHowItWorks data={contentHowItWorks} />
      <SectionFeatureFocus data={contentFeatureFocus} imageBackground={true}>
        <Image
          src={'/images/dark-bg.svg'}
          objectFit="cover"
          objectPosition="center"
          layout="fill"
        />
      </SectionFeatureFocus>
      <CardSection
        // @ts-ignore
        data={contentCard}
      />
      <DangerListSection
        // @ts-ignore
        data={contentDangerList}
      />
      <VerticalListSection
        // @ts-ignore
        data={contentVerticalList}
      />
      <SectionCarousel data={contentProviderPageCarousel} />
    </>
  );
};

export default ContentProvider;
