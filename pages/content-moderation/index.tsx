import React from 'react';
import Image from 'next/image';
import SectionHero from '../../components/sectionHero';
import SectionMastercard from '../../components/sectionMastercard';
import {
  contentFeatureModeration,
  contentHowItWorksModeration,
  contentModerationHero,
  contentModerationPageCarousel,
  contentVerifyContentModeration,
  doubleCardModeration,
  vmcMastercard
} from '../../configs/pagesConfig/contentModerationPageConfig';
import SectionVerifyContent from '../../components/sectionVerifyContent';
import SectionDoubleCart from '../../components/sectionDoubleCart';
import SectionContentReviewControl from '../../components/sectionContentReviewControl';
import { contentReviewControlConfig } from '../../configs/contentReviewControlConfig';
import SectionContentModeration from '../../components/sectionContentModeration';
import { contentModerationConfig } from '../../configs/pagesConfig/contentModerationConfig';
import SectionHowItWorks from '../../components/sectionHowItWorks';
import SectionFeatureFocus from '../../components/sectionFeatureFocus';
import { contentDangerList } from '../../configs/pagesConfig/liveStreamingPageConfig';
import DangerListSection from '../../components/sectionDangerList';
import SectionCarousel from '../../components/sectionCarousel';

const ContentModeration = () => {
  return (
    <>
      <SectionHero data={contentModerationHero} imageBackground={false}>
        <Image
          src={'/images/home-hero-bg.svg'}
          objectFit="cover"
          objectPosition="center"
          layout="fill"
        />
      </SectionHero>
      <SectionMastercard data={vmcMastercard} />
      <SectionVerifyContent data={contentVerifyContentModeration} imageBackground={true}>
        <Image
          src={'/images/dark-bg.svg'}
          objectFit="cover"
          objectPosition="center"
          layout="fill"
        />
      </SectionVerifyContent>
      <SectionDoubleCart data={doubleCardModeration} />
      <SectionContentReviewControl data={contentReviewControlConfig} />
      <SectionContentModeration data={contentModerationConfig} />
      <SectionHowItWorks data={contentHowItWorksModeration} />
      <SectionFeatureFocus data={contentFeatureModeration} imageBackground={true}>
        <Image
          src={'/images/dark-bg.svg'}
          objectFit="cover"
          objectPosition="center"
          layout="fill"
        />
      </SectionFeatureFocus>
      <DangerListSection data={contentDangerList} />
      <SectionCarousel data={contentModerationPageCarousel} />
    </>
  );
};

export default ContentModeration;
