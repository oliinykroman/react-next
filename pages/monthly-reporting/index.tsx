import React from 'react';
import Image from 'next/image';
import SectionHero from '../../components/sectionHero';
import SectionMastercard from '../../components/sectionMastercard';
import {
  monthlyReportingHero,
  contentMastercard,
  contentVerifyContent,
  contentFeatureFocus,
  contentMonthlyReportsDetail,
  reportDetailPageCarousel
} from '../../configs/pagesConfig/monthlyReportingPageConfig';
import SectionVerifyContent from '../../components/sectionVerifyContent';
import SectionCarousel from '../../components/sectionCarousel';
import SectionFeatureFocus from '../../components/sectionFeatureFocus';
import SectionMonthlyReportsDetail from '../../components/sectionMonthlyReportsDetail';

const MonthlyReporting = () => {
  return (
    <>
      <SectionHero data={monthlyReportingHero} imageBackground={false}>
        <Image
          src={'/images/home-hero-bg.svg'}
          objectFit="cover"
          objectPosition="center"
          layout="fill"
        />
      </SectionHero>
      <SectionMastercard data={contentMastercard} />
      <SectionVerifyContent data={contentVerifyContent} imageBackground={true}>
        <Image
          src={'/images/dark-bg.svg'}
          objectFit="cover"
          objectPosition="center"
          layout="fill"
        />
      </SectionVerifyContent>

      <SectionMonthlyReportsDetail
        // @ts-ignore
        data={contentMonthlyReportsDetail}
      />

      <SectionFeatureFocus data={contentFeatureFocus} imageBackground={true}>
        <Image
          src={'/images/dark-bg.svg'}
          objectFit="cover"
          objectPosition="center"
          layout="fill"
        />
      </SectionFeatureFocus>

      <SectionCarousel data={reportDetailPageCarousel} />
    </>
  );
};

export default MonthlyReporting;
