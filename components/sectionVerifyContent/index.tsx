import React from 'react';
import styled from 'styled-components';
import Section from '../grid/section';
import Row from '../grid/row';
import { GRID_COLUMN_PADDING, COLORS, SIZES, SIZES_MIN } from '../../const';
import Container from '../grid/container';
import { useIntl } from 'react-intl';

type verifyContentDataType = {
  title: string;
  text_1?: string;
  text_2?: string;
  bgColor?: string;
};

type SectionVerifyContentProps = {
  data: verifyContentDataType;
  children: React.ReactNode;
  imageBackground: boolean;
};

const SectionTitle = styled.h2`
  font-family: 'codec_proextrabold';
  font-size: 22px;
  line-height: 40px;
  color: ${COLORS.LIGHT_DANGER};
  margin-bottom: 17px;
  text-transform: uppercase;
  max-width: 920px;
  width: 100%;
  @media (min-width: 768px) {
    font-size: 24px;
    margin-bottom: 30px;
  }
`;

const SectionText = styled.p`
  font-size: 18px;
  color: ${COLORS.WHITE};
  margin-bottom: 30px;
  line-height: 142%;
  @media (min-width: 768px) {
    font-size: 24px;
    margin-bottom: 40px;
  }
`;

const Column = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  padding: 0 42px;
  @media ${SIZES_MIN.lg} {
    padding: 0 ${GRID_COLUMN_PADDING};
  }
  .text-holder {
    max-width: 796px;
    margin: 0 auto;
    @media (min-width: 1200px) {
      margin: 0 auto 0 23%;
    }
  }
`;

const StyledBackground = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  z-index: 0;
  @media ${SIZES.m} {
    display: none;
  }
`;

const smPadding = `
  @media ${SIZES.m} {
    padding: 55px 0 27px;
    overflow: hidden;
  }
`;

const nativeStyles = `position: relative; z-index: 1`;
const SectionVerifyContent = (props: SectionVerifyContentProps) => {
  const { data, children, imageBackground } = props;
  const { title, text_1, text_2, bgColor } = data;
  const intl = useIntl();
  return (
    <Section customPadding={'80px 0 38px'} bgColor={bgColor} customStyle={smPadding}>
      {imageBackground && <StyledBackground>{children}</StyledBackground>}
      <Container nativeStyles={nativeStyles}>
        <Row>
          <Column>
            <SectionTitle>{intl.formatMessage({ id: title })}</SectionTitle>
            <div className="text-holder">
              <SectionText>{intl.formatMessage({ id: text_1 })}</SectionText>
              {text_2 && <SectionText>{intl.formatMessage({ id: text_2 })}</SectionText>}
            </div>
          </Column>
        </Row>
      </Container>
    </Section>
  );
};

export default SectionVerifyContent;
