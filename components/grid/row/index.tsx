import React from 'react';
import styled from 'styled-components';

const StyledRow = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-left: -15px;
  margin-right: -15px;
  ${(props) => {
    // @ts-ignore
    return props.nativeStyles;
  }}
`;

const Row = (props: { children: React.ReactNode; nativeStyles?: string }) => {
  const { children, nativeStyles } = props;
  return (
    <StyledRow
      // @ts-ignore
      nativeStyles={nativeStyles}>
      {children}
    </StyledRow>
  );
};

export default Row;
