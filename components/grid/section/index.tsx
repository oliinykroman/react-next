import React from 'react';
import styled from 'styled-components';
import { COLORS, SIZES } from '../../../const';
const sectionPadding = '87px 0 80px';

const StyledSection = styled.section`
  position: relative;
  overflow: hidden;
  padding: ${(props) =>
    // @ts-ignore
    props.customPadding ? props.customPadding : sectionPadding};
  background-color: ${(props) =>
    // @ts-ignore
    props.bgColor ? props.bgColor : COLORS.WHITE};
  @media ${SIZES.lg} {
    padding: 60px 0 53px;
  }
  ${(props) =>
    // @ts-ignore
    props.customStyle}
`;

const Section = (props: {
  children: React.ReactNode;
  customPadding?: string;
  bgColor?: string;
  customStyle?: string;
  id?: string;
}) => {
  const { children, customPadding = '', bgColor = '', customStyle = '', id = '' } = props;
  return (
    <StyledSection
      // @ts-ignore
      customPadding={customPadding}
      bgColor={bgColor}
      customStyle={customStyle}
      id={id}>
      {children}
    </StyledSection>
  );
};

export default Section;
