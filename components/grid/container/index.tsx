import React from 'react';
import styled from 'styled-components';

const StyledWrapper = styled.div`
  width: 100%;
  margin: 0 auto;
  padding-right: 15px;
  padding-left: 15px;
  @media (min-width: 576px) {
    max-width: 540px;
  }
  @media (min-width: 768px) {
    max-width: 720px;
    padding-right: 20px;
    padding-left: 20px;
  }
  @media (min-width: 992px) {
    max-width: 960px;
  }
  @media (min-width: 1200px) {
    max-width: 1280px;
  }

  ${(props) => {
    // @ts-ignore
    return props.nativeStyles;
  }}
`;

const Container = (props: { children: React.ReactNode; nativeStyles?: string }) => {
  const { children, nativeStyles } = props;
  return (
    <StyledWrapper
      // @ts-ignore
      nativeStyles={nativeStyles}>
      {children}
    </StyledWrapper>
  );
};

export default Container;
