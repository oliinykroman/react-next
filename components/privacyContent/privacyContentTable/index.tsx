import React from 'react';
import styled from 'styled-components';
import { COLORS, GRID_COLUMN_PADDING, SIZES } from '../../../const';
import { useIntl } from 'react-intl';

const StyledList = styled.ul`
  display: flex;
  flex-direction: column;
  li {
    margin-bottom: 10px;
    position: relative;
    padding-left: 10px;

    &:after {
      content: '';
      width: 2px;
      height: 2px;
      background: ${COLORS.DARK};
      position: absolute;
      left: 0;
      top: 5px;
    }
  }
`;
const StyledP = styled.p`
  font-style: normal;
  font-weight: 400;
  font-size: 18px;
  line-height: 1.35;
  padding: 0 0 35px;
`;

const StyledWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const StyledRow = styled.div`
  max-width: 910px;
  display: flex;
  background: ${COLORS.WHITE};
  box-shadow: 0 2px 6px rgb(0 0 0 / 15%);
  border-radius: 12px;
  padding: 5px;
  margin-bottom: 10px;

  @media ${SIZES.m} {
    flex-wrap: wrap;
  }
`;

const StyledColSm = styled.div`
  display: flex;
  align-items: flex-start;
  flex: 0 0 25%;
  padding: 0 ${GRID_COLUMN_PADDING};
  font-style: normal;
  font-weight: 600;
  font-family: 'codec_proextrabold';
  font-size: 18px;
  line-height: 1.35;
  margin-bottom: 16px;
  white-space: pre-line;
  border-right: 1px solid ${COLORS.DARK};
  @media ${SIZES.m} {
    flex: 0 0 100%;
    border-right: none;
    justify-content: center;
  }
`;

const StyledColLg = styled.div`
  display: flex;
  align-items: flex-start;
  flex: 0 0 75%;
  padding: 0 ${GRID_COLUMN_PADDING};
  font-style: normal;
  font-weight: 400;
  font-size: 18px;
  line-height: 1.35;
  white-space: pre-line;
  @media ${SIZES.m} {
    flex: 0 0 100%;
    border-right: none;
    justify-content: center;
  }
`;

const GetContent = (props: { array: any; localeFunc: any }) => {
  const { array, localeFunc } = props;
  if (!array) {
    return null;
  }
  return (
    <>
      {array?.map((item, index) => {
        return (
          <StyledRow key={`li_item_${index}`}>
            <StyledColSm>{localeFunc.formatMessage({ id: item.title })}</StyledColSm>
            <StyledColLg>{localeFunc.formatMessage({ id: item.text })}</StyledColLg>
          </StyledRow>
        );
      })}
    </>
  );
};

const StyledDelimiter = styled.div`
  display: flex;
  height: 40px;
`;

const PrivacyContentTable = (props: { list: any }) => {
  const { list } = props;
  const intl = useIntl();
  console.log('PrivacyContentTable');
  return (
    <StyledWrapper>
      <GetContent array={list.table} localeFunc={intl} />
      {list?.table2 && <StyledDelimiter />}
      <GetContent array={list?.table2} localeFunc={intl} />
    </StyledWrapper>
  );
};

export default PrivacyContentTable;
