import React from 'react';
import styled from 'styled-components';
import { useIntl } from 'react-intl';
import { COLORS } from '../../const';
import { privacyContentType } from '../../configs/pagesConfig/privacyPageConfig';
import PrivacyContentList from './privacyContentList';
import PrivacyContentTable from './privacyContentTable';
import ParagraphNormal from '../typografy/paragraphNormal';
import PrivacyContentTableVertical from './privacyContentTableVertical';
import PrivacyContentTableReverse from './privacyContentTablereverse';
import PrivacyContentLinked from './privacyContentLinked';

const StyledTitle = styled.h2`
  display: flex;
  align-items: flex-end;
  font-family: 'codec_proextrabold';
  font-weight: 600;
  font-size: 24px;
  line-height: 1.2;
  white-space: pre-line;
  margin-bottom: 25px;
`;

const StyledIndex = styled.div`
  display: flex;
  align-items: flex-end;
  font-family: 'codec_proextrabold';
  font-weight: 600;
  font-size: 24px;
  line-height: 1.2;
  margin-right: 10px;
  span {
    color: ${COLORS.DANGER};
  }
`;

const StyledBox = styled.div`
  margin-bottom: 20px;
  p {
    font-style: normal;
    font-weight: 400;
    font-size: 18px;
    line-height: 1.35;
    margin-bottom: 16px;
    white-space: pre-line;
    max-width: 910px;
  }
`;

const getContent = (obj) => {
  if (obj.type === privacyContentType.list) {
    return <PrivacyContentList list={obj} />;
  }
  if (obj.type === privacyContentType.table) {
    return <PrivacyContentTable list={obj} />;
  }
  if (obj.type === privacyContentType.tableVertical) {
    return <PrivacyContentTableVertical list={obj} />;
  }
  if (obj.type === privacyContentType.tableReverse) {
    return <PrivacyContentTableReverse list={obj} />;
  }
  if (obj.type === privacyContentType.linked) {
    return <PrivacyContentLinked list={obj} />;
  }
};

const PrivacyContent = (props: { data: { title: string; content: any }[] }) => {
  const { data } = props;
  const intl = useIntl();

  return (
    <>
      {data.map((item, index) => {
        const { content } = item;
        return (
          <StyledBox key={`item_${index}`}>
            <StyledTitle id={`anchor_${index}`}>
              {index > 0 && (
                <StyledIndex>
                  {index}
                  <span>.</span>
                </StyledIndex>
              )}
              {intl.formatMessage({ id: item.title })}
            </StyledTitle>
            {typeof content === 'string' && (
              <ParagraphNormal>{intl.formatMessage({ id: content })}</ParagraphNormal>
            )}
            {typeof content === 'object' && getContent(content)}
          </StyledBox>
        );
      })}
    </>
  );
};

export default PrivacyContent;
