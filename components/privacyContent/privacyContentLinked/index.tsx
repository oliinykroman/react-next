import React from 'react';
import styled from 'styled-components';
import { FormattedMessage, useIntl } from 'react-intl';
import { privacyContentType } from '../../../configs/pagesConfig/privacyPageConfig';

const StyledRow = styled.div`
  max-width: 910px;
  font-style: normal;
  font-weight: 400;
  font-size: 18px;
  line-height: 1.35;
  white-space: pre-line;
  .strong {
    font-weight: 600;
    font-family: 'codec_proextrabold';
    text-decoration: underline;
    &:hover {
      opacity: 0.8;
    }
  }
`;

type contentType = {
  text: string;
  type: privacyContentType;
  href: string;
};

const PrivacyContentLinked = (props: { list: contentType }) => {
  const { list } = props;
  return (
    <StyledRow>
      <FormattedMessage
        id={list.text}
        values={{
          a: (msg: string) => {
            return (
              <a className={'strong'} target="_blank" href={list.href} rel="noreferrer">
                {msg}
              </a>
            );
          },
          strong: (msg: string) => {
            return <strong>{msg}</strong>;
          }
        }}
      />
    </StyledRow>
  );
};

export default PrivacyContentLinked;
