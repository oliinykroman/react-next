import React from 'react';
import styled from 'styled-components';
import { COLORS } from '../../../const';
import { useIntl } from 'react-intl';
import ParagraphNormal from '../../typografy/paragraphNormal';
import { privacyContentType } from '../../../configs/pagesConfig/privacyPageConfig';

const StyledWrapper = styled.div`
  display: flex;
  flex-direction: column;
  background: ${COLORS.WHITE};
  box-shadow: 0 2px 6px rgb(0 0 0 / 15%);
  border-radius: 12px;
  border: 1px solid ${COLORS.DARK};
  margin-bottom: 30px;
  & > div {
    border-bottom: 1px solid ${COLORS.DARK};

    &:last-child {
      border-bottom: none;
    }
  }
`;

const StyledRow = styled.div`
  max-width: 910px;
  display: flex;
  p {
    margin-bottom: 0;
  }
  &.strong,
  .strong {
    font-weight: 600;
    font-family: 'codec_proextrabold';
  }
`;

const StyledColLg = styled.div`
  display: flex;
  align-items: flex-start;
  flex-direction: column;
  flex: 0 0 75%;
  padding: 10px;
  font-style: normal;
  font-weight: 400;
  font-size: 18px;
  line-height: 1.35;
  white-space: pre-line;
  border-right: 1px solid ${COLORS.DARK};
  &:last-child {
    border-right: none;
  }
`;

const StyledColSm = styled.div`
  display: flex;
  align-items: flex-start;
  flex: 0 0 25%;
  padding: 10px;
  font-style: normal;
  font-weight: 400;
  font-size: 18px;
  line-height: 1.35;
  white-space: pre-line;
  border-right: 1px solid ${COLORS.DARK};
  &:last-child {
    border-right: none;
  }
`;

type contentType = {
  text: string;
  type: privacyContentType;
  table: { text1: string; text2: string; text3?: string }[];
  contentText?: string;
};

const PrivacyContentTableReverse = (props: { list: contentType }) => {
  const { list } = props;
  const intl = useIntl();
  return (
    <>
      <StyledRow>
        <ParagraphNormal>{intl.formatMessage({ id: list.text })}</ParagraphNormal>
      </StyledRow>
      <StyledWrapper>
        {list.table.map((item, index) => {
          return (
            <StyledRow key={`item_${index}`} className={`${index === 0 ? 'strong' : ''}`}>
              <StyledColLg>
                <p className={`${item.text3 ? 'strong' : ''}`}>
                  {intl.formatMessage({ id: item.text1 })}
                </p>
                {item.text3 && <p>{intl.formatMessage({ id: item.text3 })}</p>}
              </StyledColLg>
              <StyledColSm>{intl.formatMessage({ id: item.text2 })}</StyledColSm>
            </StyledRow>
          );
        })}
      </StyledWrapper>
      {list.contentText && (
        <StyledRow>
          <ParagraphNormal>{intl.formatMessage({ id: list.contentText })}</ParagraphNormal>
        </StyledRow>
      )}
    </>
  );
};

export default PrivacyContentTableReverse;
