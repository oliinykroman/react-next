import React from 'react';
import styled from 'styled-components';
import Section from '../grid/section';
import Row from '../grid/row';
import { GRID_COLUMN_PADDING, COLORS, SIZES } from '../../const';
import Container from '../grid/container';
import { useIntl } from 'react-intl';

type VerticalListDataType = {
  text: string;
};

type SectionVerticalListProps = {
  data?: VerticalListDataType;
  children?: React.ReactNode;
  imageBackground?: boolean;
};

const Column = styled.div`
  margin: 0 ${GRID_COLUMN_PADDING};
  padding: 80px 60px 20px;
  display: flex;
  @media (max-width: 1199px) {
    flex-wrap: wrap;
  }
  @media (max-width: 991px) {
    padding: 0 40px 20px;
  }
  @media ${SIZES.m} {
    padding: 0 27px;
  }
`;

const SectionText = styled.p`
  color: ${COLORS.DARK};
  font-size: 18px;
  line-height: 1.43;
  margin: 0 0 40px 0;
  flex: 1 0 100%;
  @media (min-width: 768px) {
    font-size: 24px;
  }
  @media (min-width: 1200px) {
    max-width: 400px;
    margin: 0 30px 0 0;
  }
`;

const CardItemWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

const CardItem = styled.div`
  display: flex;
  width: 100%;
  flex-direction: column;
  &:not(:last-child) {
    padding: 0 0 40px;
  }
  @media (min-width: 768px) {
    flex-direction: row;
    &:not(:last-child) {
      padding: 0 0 70px;
    }
  }
  .card-title {
    color: ${COLORS.DARK};
    font-family: 'codec_proextrabold';
    font-size: 28px;
    line-height: 1.1;
    margin: 0 0 8px;
  }
`;

const StyledImage = styled.div`
  display: flex;
  align-items: flex-start;
  flex: 1 0 auto;
  margin: 0 0 20px 0;
  max-width: 64px;
  @media (min-width: 768px) {
    margin: 0 25px 0 0;
    max-width: 100px;
  }
`;

const StyledContent = styled.p`
  color: ${COLORS.DARK};
  font-size: 16px;
  line-height: 1.35;
  @media (min-width: 768px) {
    font-size: 18px;
  }
`;

const smPadding = `
  @media (max-width: 991px) {
    padding-bottom: 0;
  }
`;

const VerticalListSection = (props: SectionVerticalListProps) => {
  const { data } = props;
  const { text } = data;
  const intl = useIntl();
  return (
    <Section customPadding={'0'} customStyle={smPadding}>
      <Container>
        <Row>
          <Column>
            {text && <SectionText>{intl.formatMessage({ id: text })}</SectionText>}

            <CardItemWrapper>
              {
                // @ts-ignore
                data.items.map((item, index) => {
                  const { item_image, item_alt, item_title, item_text } = item;
                  return (
                    <CardItem key={`item-${index}`}>
                      <StyledImage>
                        <img src={item_image} alt={item_alt} />
                      </StyledImage>

                      <div>
                        <h4 className="card-title">
                          {intl.formatMessage({ id: item_title })}
                          <span className="text-danger">.</span>
                        </h4>
                        <StyledContent>{intl.formatMessage({ id: item_text })}</StyledContent>
                      </div>
                    </CardItem>
                  );
                })
              }
            </CardItemWrapper>
          </Column>
        </Row>
      </Container>
    </Section>
  );
};

export default VerticalListSection;
