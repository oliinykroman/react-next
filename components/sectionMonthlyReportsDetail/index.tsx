import React from 'react';
import styled from 'styled-components';
import Section from '../grid/section';
import Row from '../grid/row';
import { GRID_COLUMN_PADDING, COLORS, SIZES } from '../../const';
import Container from '../grid/container';
import { useIntl } from 'react-intl';
import DownloadIcon from '../../svgComponent/DownloadIcon';

type MonthlyReportsDetailDataType = {
  bgColor: string;
  text: string;
  list_title: string;
  image: string;
  image_2x: string;
  alt: string;
  list?: any;
};

type SectionMonthlyReportsDetailProps = {
  data?: MonthlyReportsDetailDataType;
  children?: React.ReactNode;
  imageBackground?: boolean;
  list?: string[];
};

const Column = styled.div`
  padding: 0 ${GRID_COLUMN_PADDING};
  display: flex;
  position: relative;
  width: 100%;

  @media ${SIZES.m} {
    flex-direction: column;
    padding: 0 42px;
  }
`;

const StyledIcon = styled.div`
  flex-shrink: 0;
  height: 88px;
  width: 88px;
`;

const SectionText = styled.div`
  @media (min-width: 768px) {
    margin: 0 0 0 25px;
  }
  p {
    color: ${COLORS.DARK};
    font-size: 24px;
    line-height: 1.3;
    margin: 0 0 45px 0;
    max-width: 500px;
    flex: 1 0 100%;
    @media ${SIZES.m} {
      font-size: 18px;
      margin: 25px 0;
    }
  }
`;

const ListWrapper = styled.div`
  .list-title {
    font-size: 18px;
    margin: 0 0 20px;
  }
  @media (min-width: 575px) {
    padding: 0 0 75px;
  }
  @media (min-width: 768px) {
    padding: 0 0 150px;
  }
`;
const List = styled.ul`
  li {
    display: flex;
    font-size: 16px;
    line-height: 1;
    margin: 0 0 20px;
    @media (min-width: 768px) {
      font-size: 18px;
    }
    .list-ico {
      background-color: ${COLORS.LIGHT_DANGER};
      border-radius: 50%;
      display: flex;
      margin-right: 15px;
      width: 16px;
      height: 16px;
      position: relative;
      &:before,
      &:after {
        background-color: ${COLORS.WHITE};
        content: '';
        position: absolute;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
        margin: 0 auto;
        transform: translate(0px, 7px);
        height: 0.1rem;
        width: 0.5rem;
      }
      &:after {
        transform: translate(0px, 7px) rotate(90deg);
      }
    }
  }
`;

const ListHolder = styled.div`
  background-color: ${COLORS.WHITE};
  border-radius: 12px;
  box-shadow: 0px 14px 40px rgba(0, 0, 0, 0.25);
  display: flex;
  max-width: 929px;
  padding: 38px 32px;
  position: absolute;
  left: 130px;
  bottom: 170px;
  width: 100%;
  span {
    &:nth-child(1),
    &:nth-child(2),
    &:nth-child(3),
    &:nth-child(4) {
      background-color: ${COLORS.BEIGE};
      height: 12px;
    }
    &:nth-child(1) {
      width: 73px;
    }
    &:nth-child(2) {
      margin-left: 44px;
      width: 201px;
    }
    &:nth-child(3) {
      margin-left: 44px;
      width: 201px;
    }
    &:nth-child(4) {
      margin-left: 44px;
      width: 138px;
    }
    &:nth-child(5) {
      background-color: ${COLORS.RED};
      opacity: 0.5;
      border-radius: 24px;
      margin-left: 44px;
      height: 12px;
      width: 73px;
    }
  }

  @media ${SIZES.xl} {
    width: 80%;
    bottom: 0;
  }

  @media ${SIZES.m} {
    width: 70%;
    padding: 15px 20px;
  }
  @media ${SIZES.sm} {
    display: none;
  }
`;

const StyledImage = styled.div`
  align-items: flex-end;
  display: flex;
  @media ${SIZES.m} {
    display: none;
  }
  img {
    max-width: 100%;
  }
`;

const SectionMonthlyReportsDetail = (props: SectionMonthlyReportsDetailProps) => {
  const { data } = props;
  const { bgColor, text, list_title, image, image_2x, alt, list } = data;
  const intl = useIntl();
  return (
    <Section customPadding={'80px 0 90px'} bgColor={bgColor}>
      <Container>
        <Row>
          <Column>
            <StyledIcon>
              <DownloadIcon />
            </StyledIcon>

            <SectionText>
              <p>{intl.formatMessage({ id: text })}</p>

              <ListWrapper>
                <h4 className="list-title">{intl.formatMessage({ id: list_title })}</h4>
                <List>
                  {list?.map((index) => {
                    return (
                      <li key={`item-${index}`}>
                        <span className="list-ico"></span>
                        {intl.formatMessage({ id: index })}
                      </li>
                    );
                  })}
                </List>
              </ListWrapper>
            </SectionText>

            <StyledImage>
              <picture>
                <source srcSet={image_2x} type="image/png" />
                <img src={image} alt={alt} />
              </picture>
            </StyledImage>

            <ListHolder>
              <span />
              <span />
              <span />
              <span />
              <span />
            </ListHolder>
          </Column>
        </Row>
      </Container>
    </Section>
  );
};

export default SectionMonthlyReportsDetail;
