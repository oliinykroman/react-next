import React from 'react';
import styled from 'styled-components';
import Section from '../grid/section';
import Container from '../grid/container';
import Row from '../grid/row';
import { COLORS, GRID_COLUMN_PADDING, SIZES } from '../../const';
import Button, { buttonsType } from '../button';
import SectionTitleSmall from '../typografy/sectionTitleSmall';
import { useIntl } from 'react-intl';
import CustomerIcon from '../../svgComponent/CustomerIcon';
import ParagraphNormal from '../typografy/paragraphNormal';
import Link from 'next/link';

const StyledInner = styled.div`
  display: flex;
  align-items: flex-start;
  background: ${COLORS.LIGHT_GRAY_2};
  box-shadow: 0 2px 6px rgba(0, 0, 0, 0.15);
  border-radius: 12px;
  padding: 30px 27px;
  margin: -18px 0 0;
  @media ${SIZES.m} {
    flex-direction: column;
    align-items: flex-start;
    text-align: center;
    padding: 17px 22px;
  } ;
`;

const StyledColumn = styled.div`
  margin-left: 30px;
  display: flex;
  flex-direction: column;
  margin-right: 15px;
  @media ${SIZES.m} {
    margin: 20px auto 0;
  }
`;

const StyledIcon = styled.div`
  display: flex;
  align-items: center;
  align-self: center;
  justify-content: center;
  max-width: 70px;
  height: 70px;
  min-width: 70px;
`;

const StyledWrapper = styled.div`
  min-width: 190px;
  align-self: center;
  flex-shrink: 0;
  margin-left: auto;
  margin-right: 22px;

  * {
    width: 100%;
  }

  @media ${SIZES.m} {
    margin-left: auto;
    margin-right: auto;
    margin-top: 22px;
  } ;
`;

const StyledContainer = styled.div`
  padding: 0 ${GRID_COLUMN_PADDING};
  width: 100%;
`;

const customStyles = `margin-bottom: 10px; line-height: 1.2;`;
const customTextStyle = `max-width: 800px`;

const SectionContentReviewControl = (props: {
  data: { title: string; text: string; href: string };
}) => {
  const { data } = props;
  const { title, text, href } = data;
  const intl = useIntl();
  return (
    <Section customPadding={'18px 0;'}>
      <Container>
        <Row>
          <StyledContainer>
            <StyledInner>
              <StyledIcon>
                <CustomerIcon width={70} height={70} />
              </StyledIcon>
              <StyledColumn>
                <SectionTitleSmall customStyles={customStyles}>
                  {intl.formatMessage({ id: title })}
                  <span className="text-light-danger">.</span>
                </SectionTitleSmall>
                <ParagraphNormal customStyles={customTextStyle}>
                  {intl.formatMessage({ id: text })}
                </ParagraphNormal>
              </StyledColumn>
              <StyledWrapper>
                <Link href={href}>
                  <Button type={buttonsType.smallDanger}>
                    {intl.formatMessage({ id: 'button_label_learn_more' })}
                  </Button>
                </Link>
              </StyledWrapper>
            </StyledInner>
          </StyledContainer>
        </Row>
      </Container>
    </Section>
  );
};

export default SectionContentReviewControl;
