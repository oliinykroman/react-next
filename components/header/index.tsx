import * as React from 'react';
import LogoMain from '../../svgComponent/Logo';
import { headerNavConfig } from '../../configs/headerConfig';
import NavListItem from './navListItem';
import styled from 'styled-components';
import Button, { buttonsType } from '../button';
import HeaderDropdown from './headerDropdown';
import { COLORS, GRID_COLUMN_PADDING, SIZES } from '../../const';
import { useIntl } from 'react-intl';
import Link from 'next/link';
import { useState } from 'react';

const StyledHeader = styled.header`
  position: relative;
  z-index: 20;
  position: relative;
  background: ${COLORS.WHITE};

  position: fixed;
  top: 0;
  width: 100%;
`;

const HeaderBorder = styled.div`
  background-color: ${COLORS.DANGER};
  display: flex;
  height: 4px;
  width: 100%;
  position: relative;
  &:after {
    background-color: ${COLORS.DARK};
    content: '';
    height: 4px;
    width: 20%;
    position: absolute;
    top: 0;
    left: 0;
  }
`;

const StyledRow = styled.div`
  display: flex;
  align-items: center;

  button {
    min-width: auto;
  }

  @media ${SIZES.lg} {
    padding-top: 15px;
    padding-bottom: 15px;
  }

  .navigation {
    display: flex;
    list-style-type: none;
    margin: 0;
    padding: 0;
    & > li {
      @media (min-width: 992px) {
        &:hover,
        &.active {
          & > a,
          .parent-link {
            background: ${COLORS.DARK};
            color: ${COLORS.WHITE};

            &:after {
              background: ${COLORS.DARK};
            }
          }
          .arrow-down {
            border-width: 0 3.5px 4px 3.5px;
            border-color: transparent transparent ${COLORS.WHITE} transparent;
            z-index: 2;
          }
        }
      }
    }
    li {
      position: relative;
      display: flex;
      align-items: center;
      padding: 19px 17px;
      .parent-link {
        border-top-left-radius: 4px;
        border-bottom-left-radius: 4px;
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
        &:after {
          content: '';
          width: 17px;
          height: 100%;
          position: absolute;
          top: 0;
          right: -17px;
          border-top-right-radius: 4px;
          border-bottom-right-radius: 4px;
          transition: 0.2s;
        }
        & + .arrow-down {
          @media ${SIZES.lg} {
            width: 100%;
            height: 64px;
            top: 0;
            right: 0;
            &:before {
              @media ${SIZES.lg} {
                top: calc(50% - 5px);
                right: 17px;
                transform: translate(0, -50%);
                transform: rotate(-45deg);
                z-index: 5;
              }
            }
          }
        }
      }
      a,
      .parent-link {
        color: ${COLORS.DARK};
        cursor: pointer;
        font-family: 'codec_proextrabold';
        text-decoration: none;
        padding: 8px 10px;
        transition: 0.2s;
        position: relative;
      }
      a {
        border-radius: 4px;
      }
      @media (min-width: 992px) {
        &:hover,
        &.active {
          ul {
            display: block;
          }
        }
      }
    }
    ul {
      display: none;
      position: absolute;
      top: 100%;
      left: 0;
      padding: 0;
      list-style-type: none;
      min-width: 347px;
      background: ${COLORS.WHITE};
      box-shadow: 0px 6px 8px rgba(0, 0, 0, 0.25), inset 0px 12px 6px -12px #ccc;
      border-radius: 0px 0px 4px 4px;
      li {
        border-bottom: 1px solid ${COLORS.LIGHT_GRAY};
        padding: 23px 18px;
        a {
          display: block;
          font-family: 'Codec Pro';
        }
      }
    }
    @media ${SIZES.lg} {
      display: none;
      flex-direction: column;
      width: 100%;
      align-items: flex-start;
      position: absolute;
      top: 100%;
      left: 0;
      background: ${COLORS.WHITE};
      box-shadow: 0px 2px 4px rgb(0 0 0 / 15%);
      padding: 2.5rem 0;
      font-size: 22px;

      button {
        min-width: auto;
      }

      ul {
        box-shadow: none;
      }

      &.active {
        display: flex;
      }

      & > li.active {
        ul {
          display: flex;
          flex-direction: column;
        }
      }
      & > li {
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: flex-start;
        flex-wrap: wrap;
        // padding: 0 ${GRID_COLUMN_PADDING};
        padding: 0;

        & > a,
        .parent-link {
          border-radius: 0;
          width: 100%;
          padding: 1rem 1.5rem;
          border-bottom: 1px solid #ededf1;

          &:after {
            border-radius: 0;
          }
        }

        //&:hover,
        &.active {
          .arrow-down {
            display: block;
            cursor: pointer;
            // &:after {
            //   content: '';
            //   position: absolute;
            //   top: 50%;
            //   right: -13px;
            //   width: 35px;
            //   height: 39.5px;
            //   background: ${COLORS.DARK};
            //   transform: translateY(-47%);
            // }

            &:before {
              content: '';
              position: absolute;
              top: 50%;
              right: 17px;
              width: 10px;
              height: 10px;
              transform: translate(0, -50%);
              border-left: 1px solid ${COLORS.DARK};
              border-bottom: 1px solid ${COLORS.DARK};
              transform: rotate(135deg);
              z-index: 5;
            }
          }
        }

        ul {
          position: static;
          width: 100%;
          & > li {
            padding: 0;

            & a {
              width: 100%;
              border-radius: 0;
              @media ${SIZES.lg} {
                font-size: 18px;
                padding: 1rem 1.5rem;
              }
            }
          }
        }
      }
      .arrow-down {
        position: absolute;
        right: 17px;
        top: 16px;
        border-color: transparent;
        border-width: 1px !important;
        &:before {
          content: '';
          position: absolute;
          top: 0;
          right: 50%;
          width: 10px;
          height: 10px;
          background: ${COLORS.WHITE};
          transform: translate(0, -50%);
          border-left: 1px solid ${COLORS.DARK};
          border-bottom: 1px solid ${COLORS.DARK};
          transform: rotate(135deg);
          z-index: 5;
        }
      }
    }
  }
`;

const HeaderRow = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 23px;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.15);

  @media ${SIZES.m} {
    padding: 0 17px 0 23px;
  }
`;

const StyledLinkMobile = styled.img`
  cursor: pointer;
  display: none;
  @media ${SIZES.lg} {
    display: flex;
  }
`;

const StyledLink = styled.div`
  cursor: pointer;
  @media ${SIZES.lg} {
    display: none;
    color: red;
  }
`;

const StyledBurger = styled.button`
  width: 23px;
  height: 14px;
  position: relative;
  cursor: pointer;
  display: none;
  margin-left: 16px;
  div {
    width: 100%;
    height: 2px;
    background: ${COLORS.DARK};
    transition: 0.2s;
    position: absolute;

    &:first-child {
      top: 0;
      left: 0;
      ${(props) =>
        // @ts-ignore
        props.isActive ? 'top: 50%; transform: translateY(-50%) rotate(-45deg);' : ''}
    }
    &:nth-child(2) {
      top: 50%;
      left: 0;
      ${(props) =>
        // @ts-ignore
        props.isActive ? 'display: none;' : ''}
    }
    &:last-child {
      top: 100%;
      left: 0;
      ${(props) =>
        // @ts-ignore
        props.isActive ? 'top: 50%; transform: translateY(-50%) rotate(45deg);' : ''}
    }
  }
  @media ${SIZES.lg} {
    display: block;
  }
`;

const fontSize = `font-size: 12px;`;

const StyledBtnLg = styled.div`
  @media ${SIZES.lg} {
    display: none;
  }
`;

const StyledBtnSm = styled.div`
  display: none;
  & > div {
    margin: auto;
  }
  @media ${SIZES.lg} {
    display: block;
    margin: 35px auto 15px;
  }
`;

const Header = (props) => {
  const intl = useIntl();
  const [isActiveMenu, setIsActiveMenu] = useState(false);
  return (
    <StyledHeader>
      <HeaderBorder />
      <HeaderRow>
        <Link href="/">
          <StyledLinkMobile src={'/images/LogoMobile.svg'} />
        </Link>
        <Link href="/">
          <StyledLink>
            <LogoMain />
          </StyledLink>
        </Link>
        <StyledRow>
          <ul className={`navigation ${isActiveMenu ? 'active' : ''}`}>
            {headerNavConfig.map((item, index) => {
              return <NavListItem data={item} key={`list_${index}`}></NavListItem>;
            })}
            {/* <li> */}
            {/*  <StyledBtnSm> */}
            {/*    <HeaderDropdown /> */}
            {/*  </StyledBtnSm> */}
            {/* </li> */}
          </ul>

          <Link href="/#contactForm">
            <Button type={buttonsType.smallDanger} nativeStyles={fontSize}>
              {intl.formatMessage({ id: 'button_label_book_demo' })}
            </Button>
          </Link>
          {/* <StyledBtnLg> */}
          {/*  <HeaderDropdown /> */}
          {/* </StyledBtnLg> */}
          <StyledBurger
            // @ts-ignore
            isActive={isActiveMenu}
            onClick={() => {
              setIsActiveMenu((state) => !state);
            }}>
            <div></div>
            <div></div>
            <div></div>
          </StyledBurger>
        </StyledRow>
      </HeaderRow>
    </StyledHeader>
  );
};

export default Header;
