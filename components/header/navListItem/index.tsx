import * as React from 'react';
import Link from 'next/link';
import { useIntl } from 'react-intl';
import styled from 'styled-components';
import { COLORS, SIZES } from '../../../const';
import { useState } from 'react';

type navListItemProps = {
  data: {
    title: string;
    href: string;
    children?: { title: string; href: string }[];
  };
};

const StyledList = styled.ul`
  z-index: 3;
  a {
    position: relative;

    &:before {
      content: '';
      position: absolute;
      left: 10px;
      bottom: 8px;
      width: 0;
      height: 2px;
      background: ${COLORS.LIGHT_DANGER};
      transition: 0.3s;
    }

    &:hover {
      &:before {
        width: 50%;
      }
    }
  }
`;

const NavListItem = (props: navListItemProps) => {
  const { data } = props;
  const { title, href, children } = data;
  const intl = useIntl();
  const [activeState, setActiveState] = useState(false);
  return (
    <li className={activeState ? 'active' : ''}>
      {!children && <Link href={href}>{intl.formatMessage({ id: title })}</Link>}
      {children && (
        <>
          <p className="parent-link">{intl.formatMessage({ id: title })}</p>
          <span
            className="arrow-down"
            onClick={() => {
              setActiveState((state) => !state);
            }}></span>
        </>
      )}
      {children && (
        <StyledList>
          {children.map((item) => {
            const { title, href } = item;
            return (
              <li key={title}>
                <Link href={href} key={`link-${title}`}>
                  {intl.formatMessage({ id: title })}
                </Link>
              </li>
            );
          })}
        </StyledList>
      )}
    </li>
  );
};

export default NavListItem;
