import React from 'react';
import styled from 'styled-components';
import { langListConfig } from '../../../configs/languagesList';
import { COLORS } from '../../../const';
import { useIntl } from 'react-intl';
import Link from 'next/link';
import { useRouter } from 'next/router';

const Wrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: 0 0 0 38px;
  position: relative;
  background-color: ${COLORS.WHITE};
  border: 1px solid ${COLORS.DARK};
  border-radius: 4px;

  &:hover {
    & > * {
      display: flex;
    }
  }
`;

const Box = styled.div`
  cursor: pointer;
  padding: 6px 10px;
  min-width: 113px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  & + div {
    position: absolute;
    top: 100%;
    left: 0;
    display: none;
  }
  .drop-info {
    display: flex;
    align-items: center;
  }
  .drop-title {
    font-size: 11px;
  }
  .icon {
    margin: 0 8px 0 0;
  }
  a {
    display: flex;
    align-items: center;
  }
`;

const IconWrapper = styled.div`
  display: flex;
  border-radius: 50%;
  width: 21px;
  height: 21px;
  overflow: hidden;
`;

const BoxBlock = styled.div`
  position: absolute;
  top: 100%;
  left: 0;
  flex-direction: column;
  background-color: ${COLORS.WHITE};
  border: 1px solid ${COLORS.DARK};
  border-radius: 4px;
  div {
    position: static;
    display: flex;
  }
`;

const HeaderDropdown = () => {
  const intl = useIntl();
  const router = useRouter();
  const { locale } = router;
  const getActiveLang = (langObj, key) => {
    const newObj = langObj.find((item) => {
      return item.key === key;
    });
    const { Icon, title } = newObj;
    return (
      <div className="drop-info">
        <IconWrapper className="icon">
          <Icon />
        </IconWrapper>
        <div className="drop-title">{intl.formatMessage({ id: title })}</div>
      </div>
    );
  };

  return (
    <Wrapper>
      <Box>
        {getActiveLang(langListConfig, locale)}
        <span className="arrow-down"></span>
      </Box>
      <BoxBlock>
        {langListConfig.map((item, index) => {
          const { title, Icon, key } = item;
          return (
            <Box key={index}>
              <Link
                href={router.asPath}
                locale={key}
                // @ts-ignore
                className="drop-info">
                <a>
                  <IconWrapper className="icon">
                    <Icon />
                  </IconWrapper>
                  <div className="drop-title">{intl.formatMessage({ id: title })}</div>
                </a>
              </Link>
            </Box>
          );
        })}
      </BoxBlock>
    </Wrapper>
  );
};

export default HeaderDropdown;
