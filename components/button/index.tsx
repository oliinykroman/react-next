import React from 'react';
import styled from 'styled-components';
import { COLORS, SIZES } from '../../const';

export enum buttonsType {
  largeDanger,
  smallDanger,
  largeDark,
  smallWhite,
  largeYellow,
  smallGray
}

const buttonsTypeStyles = {
  // largeDanger: `background:${COLOR.DANGER};color:${COLOR.WHITE};`
};

type ButtonProps = {
  children: React.ReactNode;
  type: buttonsType;
  nativeStyles?: string;
  onClick?: () => void;
  disabled?: boolean;
};

const StyledButton = styled('button')`
  border: none;
  outline: none;
  font-weight: 600;
  font-family: 'codec_proextrabold';
  border: 2px solid;
  background: ${(props) => {
    // @ts-ignore
    switch (props.buttonsType) {
      case buttonsType.largeDanger:
      case buttonsType.smallDanger:
        return COLORS.DANGER;
      case buttonsType.largeDark:
        return COLORS.DARK;
      case buttonsType.smallWhite:
        return COLORS.WHITE;
      case buttonsType.largeYellow:
        return COLORS.YELLOW;
      case buttonsType.smallGray:
        return COLORS.TRANSPARENT;
      default:
        return COLORS.WHITE;
    }
  }};
  color: ${(props) => {
    // @ts-ignore
    switch (props.buttonsType) {
      case buttonsType.largeDanger:
      case buttonsType.smallDanger:
        return COLORS.WHITE;
      case buttonsType.largeDark:
        return COLORS.WHITE;
      case buttonsType.smallWhite:
        return COLORS.DANGER;
      case buttonsType.largeYellow:
        return COLORS.DARK;
      case buttonsType.smallGray:
        return COLORS.GRAY_2;
      default:
        return COLORS.WHITE;
    }
  }};
  padding: ${(props) => {
    // @ts-ignore
    switch (props.buttonsType) {
      case buttonsType.largeDanger:
        return '21px 48px';
      case buttonsType.largeDark:
        return '21px 70px';
      case buttonsType.smallDanger:
        return '10px 20px';
      case buttonsType.smallWhite:
        return '5px 29px';
      case buttonsType.largeYellow:
        return '21px 70px';
      case buttonsType.smallGray:
        return '10px 30px';
      default:
        return '12px 30px';
    }
  }};
  border-color: ${(props) => {
    // @ts-ignore
    switch (props.buttonsType) {
      case buttonsType.largeDanger:
      case buttonsType.smallDanger:
        return COLORS.DANGER;
      case buttonsType.largeDark:
        return COLORS.DARK;
      case buttonsType.smallWhite:
        return COLORS.WHITE;
      case buttonsType.largeYellow:
        return COLORS.YELLOW;
      case buttonsType.smallGray:
        return COLORS.GRAY_2;
      default:
        return COLORS.WHITE;
    }
  }};
  border-radius: 44px;
  cursor: pointer;
  font-weight: 600;
  font-size: 15px;
  transition: 0.3s;
  ${(props) => {
    return props.disabled ? 'cursor: not-allowed;\n  opacity: 0.5;' : '';
  }}
  ${(props) => {
    // @ts-ignore
    return props.nativeStyles;
  }}

  &:hover {
    // opacity: 0.8;
    background-color: #E93C8D;
    border-color: #E93C8D;
  }

  @media ${SIZES.lg} {
    ${(props) => {
      switch (props.type) {
        // @ts-ignore
        case buttonsType.smallWhite:
          return 'min-width: 140px;\n    padding: 4.5px 10px;';
        default:
          return 'min-width: 190px;\n    padding: 9px 18px;';
      }
    }}
`;

const Button = (props: ButtonProps) => {
  const { children, type, nativeStyles, onClick, disabled = false } = props;
  return (
    <StyledButton
      // @ts-ignore
      buttonsType={type}
      disabled={disabled}
      nativeStyles={nativeStyles}
      onClick={onClick}>
      {children}
    </StyledButton>
  );
};

export default Button;
