import React, { useCallback } from 'react';
import styled from 'styled-components';
import Link from 'next/link';
import { useRouter } from 'next/router';
import SectionTitleMedium from '../typografy/sectionTitleMedium';
import SectionTitleSmall from '../typografy/sectionTitleSmall';
import { useIntl } from 'react-intl';
import { COLORS, SIZES } from '../../const';
import StickyBox from 'react-sticky-box';

const StyledList = styled.ul`
  display: flex;
  flex-direction: column;
`;

const StyledLink = styled.li`
  display: flex;
  margin-bottom: 15px;
  a {
    display: flex;
    position: relative;
    font-style: normal;
    font-weight: 400;
    font-size: 18px;
    line-height: 1.35;
    cursor: pointer;

    &:after {
      content: '';
      width: 0;
      height: 1px;
      background: ${COLORS.DARK};
      transition: 0.2s;
      position: absolute;
      left: 0;
      bottom: 0;
    }

    &:hover {
      &:after {
        width: 50%;
      }
    }
  }

  @media ${SIZES.lg} {
    margin-bottom: 5px;
  }
`;

const StyledIndex = styled.div`
  display: flex;
  align-items: flex-start;
  font-weight: 600;
  font-family: 'codec_proextrabold';
  margin-right: 10px;
  span {
    color: ${COLORS.DANGER};
  }
`;

const PrivacyAside = (props: {
  data: { title: string; subtitle: string; list: { title: string; href: string }[] };
}) => {
  const { data } = props;
  const { title, subtitle, list } = data;
  const route = useRouter();
  const intl = useIntl();
  const handleClick = useCallback((e, targetHref) => {
    e.preventDefault();
    const destination = document.querySelector(targetHref);
    if (destination) {
      destination.scrollIntoView({
        behavior: 'smooth'
      });
    }
  }, []);
  return (
    <div>
      <SectionTitleMedium>{intl.formatMessage({ id: title })}</SectionTitleMedium>
      <SectionTitleSmall>{intl.formatMessage({ id: subtitle })}</SectionTitleSmall>
      <StyledList>
        {list.map((item, index) => {
          const { title, href } = item;
          return (
            <StyledLink key={`link_${index}`}>
              <Link href={`#anchor_${index + 1}`} locale={route.locale} passHref={true}>
                <a
                  onClick={(e) => {
                    handleClick(e, `#anchor_${index + 1}`);
                  }}>
                  <StyledIndex>
                    {index + 1}
                    <span>.</span>
                  </StyledIndex>
                  {intl.formatMessage({ id: title })}
                </a>
              </Link>
            </StyledLink>
          );
        })}
      </StyledList>
    </div>
  );
};

export default PrivacyAside;
