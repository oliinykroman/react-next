import React from 'react';
import styled from 'styled-components';
import Section from '../grid/section';
import Container from '../grid/container';
import Row from '../grid/row';
import { useIntl } from 'react-intl';
import SectionTitleMedium from '../typografy/sectionTitleMedium';
import ParagraphNormal from '../typografy/paragraphNormal';
import { COLORS, SIZES } from '../../const';

type SectionContentModerationProps = {
  title: string;
  text: string;
  labels: { labelTitle: string; LabelIcon: any }[];
};

const StyledWrapper = styled.div`
  width: 100%;
  display: grid;
  grid-template-columns: repeat(1, 1fr);
  grid-gap: 20px;
  padding: 0 20px;
  @media (min-width: 768px) {
    grid-template-columns: repeat(2, 1fr);
    padding: 0 15px;
    grid-gap: 10px;
  }
  @media (min-width: 1280px) {
    grid-template-columns: repeat(4, 1fr);
    padding: 0;
  }
`;

const StyledColumn = styled.div`
  display: flex;
  width: 100%;
  max-width: 500px;
  padding: 10px;
  align-items: center;
  justify-content: flex-start;
  background: ${COLORS.DARK};
  font-family: 'codec_proextrabold';
  font-weight: 600;
  font-size: 22px;
  line-height: 1;
  color: ${COLORS.WHITE};
  border-radius: 82px;
  margin: 0 auto;
  @media (min-width: 1280px) {
    font-size: 24px;
    max-width: 300px;
  }

  @media ${SIZES.lg} {
    padding: 5px;
  }
  svg {
    margin-right: 13px;
    width: 100%;
    max-width: 41px;
    @media (min-width: 768px) {
      max-width: 71px;
    }
  }
`;

const customTitleStyle = `text-align: center; margin-left: auto;  margin-right: auto;  margin-bottom: 20px;`;

const customPStyle = `max-width: 750px; text-align: center; margin-left: auto;  margin-right: auto;  margin-bottom: 45px;`;

const StyledTop = styled.div`
  @media ${SIZES.lg} {
    padding: 0 27px;

    * {
      text-align: left;
    }
    p {
      margin-bottom: 30px;
    }
  }
`;

const SectionContentModeration = (props: { data: SectionContentModerationProps }) => {
  const { data } = props;
  const { title, text, labels } = data;
  const intl = useIntl();
  return (
    <Section>
      <Container>
        <StyledTop>
          <SectionTitleMedium customStyles={customTitleStyle}>
            {intl.formatMessage({ id: title })}
            <span className="text-light-danger">.</span>
          </SectionTitleMedium>

          <ParagraphNormal customStyles={customPStyle}>
            {intl.formatMessage({ id: text })}
          </ParagraphNormal>
        </StyledTop>

        <StyledWrapper>
          {labels?.map((item, index) => {
            const { LabelIcon, labelTitle } = item;
            return (
              <StyledColumn key={`item_${index}`}>
                <LabelIcon />
                {intl.formatMessage({ id: labelTitle })}
                <span className="text-light-danger">.</span>
              </StyledColumn>
            );
          })}
        </StyledWrapper>
      </Container>
    </Section>
  );
};

export default SectionContentModeration;
