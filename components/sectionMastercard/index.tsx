import React from 'react';
import styled from 'styled-components';
import Section from '../grid/section';
import Row from '../grid/row';
import { GRID_COLUMN_PADDING, COLORS } from '../../const';
import Container from '../grid/container';
import { useIntl } from 'react-intl';
import MasterCardLogo from '../../svgComponent/MasterCardLogo';

type MastercardDataType = {
  title?: string;
  text?: string;
  bgColor?: string;
};

type SectionMastercarProps = {
  data?: MastercardDataType;
  children?: React.ReactNode;
  imageBackground?: boolean;
};

const Column = styled.div`
  padding: 0 42px;
  display: flex;
  width: 100%;
  flex-direction: column;
  @media (min-width: 768px) {
    flex-direction: row;
    padding: 0 ${GRID_COLUMN_PADDING};
  }
`;

const TextWrapper = styled.div`
  @media (min-width: 768px) {
    margin: 0 0 0 80px;
    max-width: 790px;
  }
`;

const StyledIconWrapper = styled.div`
  display: block;
  svg {
    max-width: 90px;
    margin: 0 0 30px;
    @media (min-width: 768px) {
      margin: 0;
    }
    @media (min-width: 1200px) {
      max-width: 100%;
    }
  }
`;

const SectionTitle = styled.h2`
  color: ${COLORS.DARK};
  font-family: 'codec_proextrabold';
  font-size: 24px;
  line-height: 1;
  margin: 0 0 25px;
  @media (min-width: 768px) {
    font-size: 36px;
    margin: 0 0 20px;
  }
`;

const SectionText = styled.p`
  color: ${COLORS.DARK};
  font-size: 18px;
  font-style: italic;
  line-height: 1.34;
  font-weight: bold;
`;

const SectionMastercard = (props: SectionMastercarProps) => {
  const { data } = props;
  const { title, text, bgColor } = data;
  const intl = useIntl();
  return (
    <Section customPadding={'80px 0'} bgColor={bgColor}>
      <Container>
        <Row>
          <Column>
            <StyledIconWrapper>
              <MasterCardLogo />
            </StyledIconWrapper>
            <TextWrapper>
              <SectionTitle>
                {intl.formatMessage({ id: title })}
                <span className="text-light-danger">.</span>
              </SectionTitle>
              <SectionText>{intl.formatMessage({ id: text })}</SectionText>
            </TextWrapper>
          </Column>
        </Row>
      </Container>
    </Section>
  );
};

export default SectionMastercard;
