import React from 'react';
import styled from 'styled-components';

const StyledParagraph = styled.p`
  font-style: normal;
  font-weight: 400;
  font-size: 16px;
  line-height: 1.35;
  @media (min-width: 768px) {
    font-size: 18px;
  }
  ${(props) => {
    // @ts-ignore
    return props.customStyles;
  }}
`;

type ParagraphNormalType = {
  children: string;
  customStyles?: string;
};

const ParagraphNormal = (props: ParagraphNormalType) => {
  const { children, customStyles } = props;
  // @ts-ignore
  return <StyledParagraph customStyles={customStyles}>{children}</StyledParagraph>;
};

export default ParagraphNormal;
