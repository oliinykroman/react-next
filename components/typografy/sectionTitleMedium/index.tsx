import React from 'react';
import styled from 'styled-components';

const StyledTitle = styled.h2`
  font-family: 'codec_proextrabold';
  font-weight: 600;
  font-size: 24px;
  line-height: 1.2;
  white-space: pre-line;
  @media (min-width: 768px) {
    font-size: 36px;
  }
  ${(props) => {
    // @ts-ignore
    return props.customStyles;
  }}
`;

const SectionTitleMedium = (props: { children: any; customStyles?: string }) => {
  const { children, customStyles } = props;
  return (
    <StyledTitle
      // @ts-ignore
      customStyles={customStyles}>
      {children}
    </StyledTitle>
  );
};

export default SectionTitleMedium;
