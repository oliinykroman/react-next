import React from 'react';
import styled from 'styled-components';

const StyledTitle = styled.h2`
  font-family: 'codec_proextrabold';
  font-weight: 600;
  font-size: 28px;
  line-height: 40px;
  ${(props) => {
    // @ts-ignore
    return props.customStyles;
  }}
`;

const SectionTitleSmall = (props: { children: any; customStyles?: string }) => {
  const { children, customStyles } = props;
  return (
    <StyledTitle
      // @ts-ignore
      customStyles={customStyles}>
      {children}
    </StyledTitle>
  );
};

export default SectionTitleSmall;
