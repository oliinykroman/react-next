import React from 'react';
import styled from 'styled-components';

const StyledTitle = styled.h2`
  font-family: 'codec_proextrabold';
  font-weight: 600;
  font-size: 38px;
  line-height: 1;
  white-space: pre-line;
  @media (min-width: 768px) {
    font-size: 60px;
  }
  ${(props) => {
    // @ts-ignore
    return props.customStyles;
  }}
`;

type SectionTitleLargeProps = {
  children: any;
  customStyles?: string;
};

const SectionTitleLarge = (props: SectionTitleLargeProps) => {
  const { children, customStyles } = props;
  return (
    <StyledTitle
      // @ts-ignore
      customStyles={customStyles}>
      {children}
    </StyledTitle>
  );
};

export default SectionTitleLarge;
