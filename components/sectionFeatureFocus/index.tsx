import React from 'react';
import styled from 'styled-components';
import Section from '../grid/section';
import Row from '../grid/row';
import Button, { buttonsType } from '../button';
import { COLORS, SIZES, SIZES_MIN } from '../../const';
import Container from '../grid/container';
import { useIntl } from 'react-intl';
import Link from 'next/link';

type featureFocusDataType = {
  subtitle?: string;
  title?: string;
  text?: string;
  image?: string;
  alt?: string;
  bgColor?: string;
  href?: string;
  image_2x?: string;
  button?: {
    type: buttonsType;
    label: string;
    action: any;
  };
};

type SectionFeatureFocusProps = {
  data: featureFocusDataType;
  children: React.ReactNode;
  imageBackground: boolean;
};

const SectionSubTitle = styled.p`
  font-family: 'codec_proextrabold';
  font-size: 20px;
  line-height: 40px;
  color: ${COLORS.LIGHT_DANGER};
  text-transform: uppercase;
  width: 100%;
  margin-top: 109px;
  @media (min-width: 768px) {
    font-size: 24px;
  }
  @media (min-width: 992px) {
    margin-top: 0;
  }
`;

const SectionTitle = styled.h2`
  color: ${COLORS.WHITE};
  font-family: 'codec_proextrabold';
  font-size: 38px;
  line-height: 1;
  width: 100%;
  margin-bottom: 20px;
  white-space: pre-line;
  @media (min-width: 768px) {
    font-size: 60px;
    margin-bottom: 40px;
  }
`;

const SectionText = styled.p`
  color: ${COLORS.WHITE};
  font-size: 18px;
  width: 100%;
  @media (min-width: 768px) {
    font-size: 24px;
  }
`;

const Column = styled.div`
  padding: 0 42px;
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  position: relative;
  @media (min-width: 992px) {
    width: 50%;
  }

  &:last-child {
    @media (min-width: 768px) {
      position: relative;
      order: unset;
    }
    @media (min-width: 992px) {
      height: 440px;
    }

    @media ${SIZES.lg} {
      order: -1;
      margin-top: -134px;
      margin-left: -73px;
      &:before {
        width: 410px;
        height: 410px;
        position: absolute;
        top: -34px;
        left: -90px;
      }
    }
  }
  &.justify-end {
    justify-content: flex-end;
  }
  &.align-start {
    align-items: flex-start;
  }
`;

const StyledBackground = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  z-index: 0;

  @media ${SIZES.lg} {
    display: none;
  }
`;

const StyledImage = styled.div`
  width: 325px;
  height: 325px;
  flex-shrink: 0;
  position: relative;

  @media ${SIZES_MIN.m} and ${SIZES.lg} {
    width: 290px;
    height: 290px;
  }
  &:before {
    content: '';
    width: 440px;
    height: 440px;
    position: absolute;
    top: -25px;
    left: -90px;
    z-index: 0;
    background: ${COLORS.DANGER};
    border-radius: 50%;

    @media ${SIZES_MIN.m} and ${SIZES.lg} {
      width: 390px;
      height: 390px;
      left: -34px;
    }

    @media ${SIZES.m} {
      width: 400px;
      height: 400px;
      left: -81px;
    }
  }
  img {
    border-radius: 50%;
    width: 100%;
    height: 100%;
    position: relative;
    z-index: 1;
    transform: translate(-9px, 10px);
    filter: drop-shadow(0px 9px 50px rgba(0, 0, 0, 0.35));
  }

  @media ${SIZES.m} {
    order: -1;
    width: 295px;
    height: 295px;
  }
`;

const ButtonWrapper = styled.div`
  margin: 38px 0 0;
  @media ${SIZES.m} {
    display: none;
  }
`;

const nativeStyles = `z-index: 1`;
const rowStyles = `align-items: flex-start;`;
const SectionFeatureFocus = (props: SectionFeatureFocusProps) => {
  const { data, children, imageBackground } = props;

  const { subtitle, title, text, bgColor, button, image, image_2x, alt, href } = data;
  const intl = useIntl();
  return (
    <Section customPadding={'110px 0 50px; overflow: hidden;'} bgColor={bgColor}>
      {imageBackground && <StyledBackground>{children}</StyledBackground>}
      <Container nativeStyles={nativeStyles}>
        <Row nativeStyles={rowStyles}>
          <Column>
            <SectionSubTitle>{intl.formatMessage({ id: subtitle })}</SectionSubTitle>
            <SectionTitle>
              {intl.formatMessage({ id: title })}
              <span className="text-light-danger">.</span>
            </SectionTitle>
            <SectionText>{intl.formatMessage({ id: text })}</SectionText>
            {button && (
              <ButtonWrapper>
                <Link href={href}>
                  <Button type={button.type} onClick={button.action}>
                    {button.label}
                  </Button>
                </Link>
              </ButtonWrapper>
            )}
          </Column>
          <Column className="justify-end align-start">
            <StyledImage>
              <picture>
                <source srcSet={image_2x} type="image/png" />
                <img src={image} alt={alt} />
              </picture>
            </StyledImage>
          </Column>
        </Row>
      </Container>
    </Section>
  );
};

export default SectionFeatureFocus;
