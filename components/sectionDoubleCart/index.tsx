import React from 'react';
import Section from '../grid/section';
import Container from '../grid/container';
import Row from '../grid/row';
import SectionTitleMedium from '../typografy/sectionTitleMedium';
import { useIntl } from 'react-intl';
import ParagraphNormal from '../typografy/paragraphNormal';
import styled from 'styled-components';
import { COLORS, GRID_COLUMN_PADDING } from '../../const';

type SectionDoubleCartProps = {
  data: {
    title: string;
    text: string;
    Icon: any;
    bgColor: string;
    cards: { item_icon_url?: string; item_icon_alt?: string; title: string; text: string }[];
  };
};

const customTitleStyle = `
margin-bottom: 12px;
padding: 0 25px;
@media (min-width: 768px) {
  padding: 0;
  margin-bottom: 20px;
}
`;

const StyledColumn = styled.div`
  padding: 0 ${GRID_COLUMN_PADDING};
  max-width: 555px;
  z-index: 1;
  @media (max-width: 1199px) {
    position: relative;
  }
  &:first-child {
    margin: 0 0 53px 0;
    @media (min-width: 1200px) {
      margin: 0 40px 0 0;
    }
    [class*='StyledIcon'] {
      display: none;
    }
  }
  &:nth-child(2) {
    margin: 53px 0 0 0;
    @media (min-width: 1200px) {
      margin: 0 0 0 40px;
    }
  }
`;
const StyledInner = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
  background: ${COLORS.LIGHT_GRAY_2};
  padding: 17px 22px;
  box-shadow: 0px 2px 6px rgba(0, 0, 0, 0.15);
  border-radius: 12px;
  position: relative;
  @media (min-width: 768px) {
    flex-direction: row;
    align-items: flex-start;
    text-align: left;
    padding: 30px 22px;
  }
  &:after {
    content: '';
    width: 18px;
    height: 18px;
    border-radius: 50%;
    background: ${COLORS.LIGHT_DANGER};
    position: absolute;
    z-index: 1;
    top: 50%;
    ${(props) => {
      // @ts-ignore
      return (props.index + 1) % 2 ? 'right: -9px;' : 'left: -9px';
    }};
    transform: translateY(-50%);
    @media (max-width: 1199px) {
      width: 14px;
      height: 14px;
      left: calc(50% - 7px);
      ${(props) => {
        // @ts-ignore
        return (props.index + 1) % 2 ? 'top: 100%;' : 'top: 0';
      }};
    }
  }
`;

const StyledIndex = styled.div`
  display: flex;
  font-family: 'codec_proextrabold';
  font-weight: 600;
  font-size: 36px;
  line-height: 1;
  margin-bottom: 13px;
  @media (min-width: 768px) {
    font-size: 75px;
    padding-left: 12px;
    min-width: 75px;
    margin-bottom: 0;
  }
  span {
    display: inline-block;
    color: ${COLORS.LIGHT_DANGER};
  }
`;

const StyledInnerColumn = styled.div`
  display: flex;
  flex-direction: column;
  @media (min-width: 768px) {
    padding-left: 24px;
    padding-top: 6px;
  }
`;

const StyledTitle = styled.h3`
  font-weight: 600;
  font-size: 20px;
  line-height: 1;
  margin-bottom: 12px;
  font-family: 'codec_proextrabold';
  @media (min-width: 768px) {
    font-size: 28px;
  }
`;

const StyledContainer = styled.div`
  display: flex;
  width: 100%;
  position: relative;
  flex-wrap: wrap;
  justify-content: center;
  @media (min-width: 1200px) {
    justify-content: space-between;
    flex-wrap: nowrap;
  }
  &:before {
    content: '';
    width: 1px;
    height: 100%;
    background: ${COLORS.LIGHT_DANGER};
    position: absolute;
    left: 50%;
    top: 50%;
    z-index: 0;
    transform: translate(-50%, -50%);
    @media (min-width: 1200px) {
      width: 50%;
      height: 1px;
    }
  }
`;

const StyledIcon = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  width: 48px;
  height: 48px;
  left: 50%;
  top: -50px;
  transform: translate(-50%, -50%);
  border-radius: 50%;
  background: ${COLORS.LIGHT_DANGER};
  @media (min-width: 1200px) {
    top: 50%;
  }
  svg {
    margin: 12px auto;
    &.WhiteChevron {
      @media (max-width: 1199px) {
        transform: rotate(90deg);
      }
    }
  }
`;

const StyledContent = styled.p`
  font-weight: 400;
  font-size: 16px;
  line-height: 1.35;
  max-width: 850px;
  margin-bottom: 35px;
  padding: 0 25px;
  @media (min-width: 768px) {
    font-size: 24px;
    margin-bottom: 75px;
    padding: 0;
  }
`;

const StyledItemIcon = styled.div`
  flex: 1 0 auto;
  width: 64px;
  height: 64px;
`;
const SectionDoubleCart = (props: SectionDoubleCartProps) => {
  const { data } = props;
  const { title, text, Icon, cards, bgColor } = data;
  const intl = useIntl();
  return (
    <Section bgColor={bgColor}>
      <Container>
        <Row>
          <div className={'column'}>
            <SectionTitleMedium customStyles={customTitleStyle}>
              {intl.formatMessage({ id: title })}
            </SectionTitleMedium>
            <StyledContent>{intl.formatMessage({ id: text })}</StyledContent>
          </div>
        </Row>
        <Row>
          <StyledContainer>
            {cards.map((item, index) => {
              return (
                <StyledColumn key={`item_${index}`}>
                  <StyledInner
                    // @ts-ignore
                    index={index}>
                    {!item.item_icon_url && (
                      <StyledIndex>
                        {index + 1}
                        <span>.</span>
                      </StyledIndex>
                    )}

                    {item.item_icon_url && (
                      <StyledItemIcon>
                        <img src={item.item_icon_url} alt={item.item_icon_alt} />
                      </StyledItemIcon>
                    )}

                    <StyledInnerColumn>
                      <StyledTitle>
                        {intl.formatMessage({ id: item.title })}
                        <span className="text-light-danger">.</span>
                      </StyledTitle>
                      <ParagraphNormal>{intl.formatMessage({ id: item.text })}</ParagraphNormal>
                    </StyledInnerColumn>
                  </StyledInner>
                  <StyledIcon>
                    <Icon />
                  </StyledIcon>
                </StyledColumn>
              );
            })}
            {/* <StyledIcon> */}
            {/*  <Icon /> */}
            {/* </StyledIcon> */}
          </StyledContainer>
        </Row>
      </Container>
    </Section>
  );
};

export default SectionDoubleCart;
