import React from 'react';
import styled from 'styled-components';
import Section from '../grid/section';
import Row from '../grid/row';
import { GRID_COLUMN_PADDING, COLORS, SIZES } from '../../const';
import Container from '../grid/container';
import { useIntl } from 'react-intl';

type DangerListDataType = {
  title: string;
  bgColor: string;
  bgItemColor: string;
};

type SectionDangerListProps = {
  data?: DangerListDataType;
  children?: React.ReactNode;
  imageBackground?: boolean;
};

const SectionBg = styled.div`
  background-color: ${(item) =>
    // @ts-ignore
    item.bgColor ? item.bgColor : COLORS.WHITE};
  height: calc(100% - 150px);
  width: 100%;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 0;
`;

const Column = styled.div`
  background-color: ${(item) =>
    // @ts-ignore
    item.bgItemColor ? item.bgItemColor : COLORS.WHITE};
  margin: 0 ${GRID_COLUMN_PADDING};
  padding: 60px 40px 35px;
  display: flex;
  flex-wrap: wrap;
  position: relative;
  z-index: 1;
  width: 100%;
  @media (min-width: 992px) {
    padding: 95px 60px 128px;
  }
  @media ${SIZES.m} {
    padding: 60px 30px 35px;
  }
`;

const SectionTitle = styled.h2`
  color: ${COLORS.WHITE};
  font-family: 'codec_proextrabold';
  font-size: 38px;
  line-height: 1;
  margin: 0 0 37px;
  @media (min-width: 768px) {
    font-size: 60px;
    margin: 0 0 60px;
    max-width: 750px;
  }
`;

const CardItemWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  @media (min-width: 992px) {
    margin-left: -30px;
    margin-right: -30px;
  }
`;

const CardItem = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex-direction: column;
  padding: 0 0 30px;
  @media (min-width: 992px) {
    width: calc(100% / 3);
    padding: 0 30px;
  }
  .card-title {
    color: ${COLORS.WHITE};
    font-family: 'codec_proextrabold';
    font-size: 28px;
    line-height: 0.9;
    margin: 0 0 20px;
    text-transform: uppercase;
    white-space: pre-line;
  }
`;

const StyledImage = styled.div`
  display: flex;
  margin: 0 0 20px;
`;

const StyledContent = styled.p`
  color: ${COLORS.WHITE};
  font-size: 18px;
  line-height: 1.3;
`;

const smPadding = `
  @media (max-width: 991px) {
    padding-bottom: 0;
  }
`;

const DangerListSection = (props: SectionDangerListProps) => {
  const { data } = props;
  const { title, bgColor, bgItemColor } = data;
  const intl = useIntl();
  return (
    <Section customPadding={'0'}>
      <SectionBg
        // @ts-ignore
        bgColor={bgColor}
      />
      <Container>
        <Row>
          <Column
            // @ts-ignore
            bgItemColor={bgItemColor}>
            {title && (
              <SectionTitle>
                {intl.formatMessage({ id: title })}
                <span className="text-light-danger">.</span>
              </SectionTitle>
            )}

            <CardItemWrapper>
              {
                // @ts-ignore
                data.items.map((item, index) => {
                  const { item_image, item_alt, item_title, item_text } = item;
                  return (
                    <CardItem key={`item-${index}`}>
                      <StyledImage>
                        <img src={item_image} alt={item_alt} />
                      </StyledImage>

                      <h4 className="card-title">{intl.formatMessage({ id: item_title })}</h4>
                      <StyledContent>{intl.formatMessage({ id: item_text })}</StyledContent>
                    </CardItem>
                  );
                })
              }
            </CardItemWrapper>
          </Column>
        </Row>
      </Container>
    </Section>
  );
};

export default DangerListSection;
