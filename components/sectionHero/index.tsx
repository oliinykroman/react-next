import React from 'react';
import styled from 'styled-components';
import Section from '../grid/section';
import Row from '../grid/row';
import Button, { buttonsType } from '../button';
import { COLORS, GRID_COLUMN_PADDING, SIZES, SIZES_MIN } from '../../const';
import Container from '../grid/container';
import { useIntl } from 'react-intl';
import { useTranslation } from 'react-i18next';
import Link from 'next/link';

type heroDataType = {
  title: string;
  subtitle: string;
  bgColor: string;
  subtitleBold?: string;
  href?: string;
  button: {
    type: buttonsType;
    label: string;
    action: any;
  };
  sectionImg?: string;
  sectionImg_2x?: string;
};

type SectionHeroProps = {
  data: heroDataType;
  children: React.ReactNode;
  imageBackground: boolean;
  customStyle?: string;
};

const HeroSubtitle = styled.h2`
  font-size: 18px;
  margin-bottom: 42px;
  white-space: pre-line;
  line-height: 140%;
  color: ${(props) => {
    // @ts-ignore
    return props.bgColor === COLORS.WHITE ? COLORS.DANGER : COLORS.WHITE;
  }};
  @media (min-width: 768px) {
    font-size: 24px;
    margin-bottom: 55px;
  }
  ${(props) => {
    // @ts-ignore
    return props.isMain ? '' : 'max-width: 670px;';
  }}
  ${(props) => {
    // @ts-ignore
    if (!props.isMain) {
      // @ts-ignore
      return `@media ${SIZES.sm} { margin-bottom: 32px; }`;
    }
  }}
`;

const HeroTitle = styled.h1`
  font-family: 'codec_proextrabold';
  font-weight: 600;
  font-size: 38px;
  line-height: 1;
  max-width: 700px;
  white-space: pre-line;
  color: ${(props) => {
    // @ts-ignore
    return props.bgColor === COLORS.WHITE ? COLORS.DARK : COLORS.WHITE;
  }};
  margin-bottom: 21px;
  @media (max-width: 450px) {
    max-width: 300px;
  }
  @media (min-width: 768px) {
    font-size: 60px;
    margin-bottom: 35px;
  }
`;

const HeroSubtitleExtraBold = styled.span`
  font-family: 'codec_proextrabold';
`;

const Column = styled.div`
  padding: 0 ${GRID_COLUMN_PADDING};
  display: flex;
  flex-direction: column;
  max-width: 910px;

  ${(props) => {
    // @ts-ignore
    if (!props.isMain) {
      return ` @media (min-width: 1200px) and (max-width: 1230px) {
                max-width: 810px;
              }`;
    }
  }}

  ${(props) => {
    // @ts-ignore
    if (!props.isMain) {
      return `@media ${SIZES.xl} {\n    max-width: 770px;\n    margin-left: auto;\n  }`;
    }
  }}

  ${(props) => {
    // @ts-ignore
    if (!props.isMain) {
      return `@media ${SIZES.lg} { max-width: 480px; }`;
    }
  }}

  ${(props) => {
    // @ts-ignore
    if (!props.isMain) {
      return `@media ${SIZES.m} { max-width: 320px; }`;
    }
  }}
 

  ${(props) => {
    // @ts-ignore
    if (!props.isMain) {
      return `@media ${SIZES.sm} { max-width: 390px;  &>*:first-child { margin-top: 115px; } }`;
    }
  }}

  @media ${SIZES.sm} {
    padding: 0 42px;
    max-width: 100%;
  }
`;
const ButtonWrapper = styled.div`
  display: flex;
  button {
    padding: 21px 48px;
    font-size: 15px;
  }

  @media ${SIZES.lg} {
    button {
      padding: 10px;
    }
  }
`;

const StyledBackground = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  z-index: 0;
`;

const StyledSectionImage = styled.div`
  height: 385px;
  flex-shrink: 0;
  position: relative;

  &:before {
    content: '';
    width: 440px;
    height: 440px;
    position: absolute;
    top: -25px;
    left: -90px;
    z-index: 0;
    background: ${COLORS.DANGER};
    border-radius: 50%;
  }

  img {
    border-radius: 50%;
    width: 100%;
    height: 100%;
    position: relative;
    z-index: 1;
    transform: translate(-9px, 10px);
    filter: drop-shadow(0px 8.42553px 46.8085px rgba(0, 0, 0, 0.35));

    @media ${SIZES_MIN.xl} {
      width: 325px;
      height: 325px;
    }
  }

  @media (max-width: 1330px) {
    width: 240px;
    height: 240px;
    margin-right: 20px;

    &:before {
      width: 340px;
      height: 340px;
    }
    img {
      width: 100%;
      height: 100%;
    }
  }

  @media ${SIZES.xl} {
    margin-top: -209px;
    margin-left: -136px;
    order: -1;
    width: 290px;
    height: 290px;
    &:before {
      width: 406px;
      height: 397px;
    }
  }
  @media ${SIZES.lg} {
    margin-top: -156px;
    margin-left: -147px;
  }

  @media ${SIZES.sm} {
    margin-left: -15px;
  }
  ${(props) => {
    // @ts-ignore
    if (!props.isMain) {
      return `@media ${SIZES_MIN.xl} {
                margin-left: auto;
                margin-right: 35px;
              }`;
    }
  }}
`;

const nativeStyles = `position: relative; z-index: 1`;
const SectionHero = (props: SectionHeroProps) => {
  const { data, children, imageBackground, customStyle } = props;
  const {
    href,
    title,
    subtitle,
    button,
    bgColor,
    subtitleBold,
    sectionImg = '',
    sectionImg_2x = ''
  } = data;
  const intl = useIntl();
  const { t } = useTranslation();
  return (
    <Section customPadding={'195px 0 119px'} bgColor={bgColor} customStyle={customStyle}>
      {imageBackground && <StyledBackground>{children}</StyledBackground>}
      <Container nativeStyles={nativeStyles}>
        <Row>
          <Column
            // @ts-ignore
            isMain={imageBackground}>
            <HeroTitle
              // @ts-ignore
              bgColor={bgColor}>
              {intl.formatMessage({ id: title })}
              <span className="text-light-danger">.</span>
            </HeroTitle>
            <HeroSubtitle
              // @ts-ignore
              bgColor={bgColor}
              isMain={imageBackground}>
              {intl.formatMessage({ id: subtitle })}
              {subtitleBold && (
                <HeroSubtitleExtraBold>
                  {' '}
                  {intl.formatMessage({ id: subtitleBold })}
                </HeroSubtitleExtraBold>
              )}
            </HeroSubtitle>
            <ButtonWrapper>
              {href ? (
                <Link href={href}>
                  <Button type={button.type} onClick={button.action}>
                    {button.label}
                  </Button>
                </Link>
              ) : (
                <Button type={button.type} onClick={button.action}>
                  {button.label}
                </Button>
              )}
            </ButtonWrapper>
          </Column>
          {sectionImg && (
            <StyledSectionImage>
              <picture>
                <source srcSet={sectionImg_2x} type="image/png" />
                <img src={sectionImg} alt={title} />
              </picture>
            </StyledSectionImage>
          )}
        </Row>
      </Container>
    </Section>
  );
};

export default SectionHero;
