import React from 'react';
import Section from '../grid/section';
import Row from '../grid/row';
import styled from 'styled-components';
import { COLORS, GRID_COLUMN_PADDING, SIZES } from '../../const';
import SectionTitleLarge from '../typografy/sectionTitleLarge';
import { useIntl } from 'react-intl';
import ParagraphNormal from '../typografy/paragraphNormal';
import Button, { buttonsType } from '../button';
import CarouselBlock, { carouselTypeEnum } from '../carousel';
import Container from '../grid/container';
import Link from 'next/link';

const StyledFirstCol = styled.div`
  padding: 0 ${GRID_COLUMN_PADDING};
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  width: 327px;
  flex-shrink: 0;
  position: relative;
  background: ${COLORS.WHITE};
  &:after {
    content: '';
    border-radius: 50%;
    width: 25px;
    height: 100%;
    position: absolute;
    right: 0;
    top: -68px;
    z-index: 0;
    box-shadow: 4px 0 6px rgb(0 0 0 / 28%);
  }
  &:before {
    content: '';
    width: 40px;
    height: 100%;
    position: absolute;
    right: 0;
    top: -68px;
    z-index: 1;
    background: ${COLORS.WHITE};
  }

  @media ${SIZES.lg} {
    width: 100%;
    padding: 0 42px;
    margin-bottom: 35px;
    & > * {
      max-width: 100%;
    }
    &:before,
    &:after {
      display: none;
    }
  }
`;

const StyledSecondColumn = styled.div`
  // padding: 0 ${GRID_COLUMN_PADDING};
  padding: 0;
  width: 81vw;
  margin-bottom: 80px;
  .home-page & {
    @media ${SIZES.lg} {
      margin-bottom: 0;
    }
  }
  @media ${SIZES.lg} {
    width: 120vw;
    & > div {
      margin-left: -30px;
    }
    .react-multi-carousel-track {
      padding-left: 20px;
    }
  }
  @media ${SIZES.m} {
    & > div {
      margin-left: -15px;
    }
  }
  @media ${SIZES.sm} {
    width: 165vw;
  } ;
`;

const StyledContainer = styled.div`
  overflow-x: hidden;
`;

const StyledRow = styled.div`
  display: flex;
  margin-left: -15px;
  margin-right: -15px;
  flex-wrap: nowrap;

  @media ${SIZES.lg} {
    flex-wrap: wrap;
  } ;
`;
const StyledTitle = styled.div`
  padding: 0 ${GRID_COLUMN_PADDING};
  margin-bottom: 55px;
  @media ${SIZES.lg} {
    padding: 0 42px;
    margin-bottom: 15px;
  }
`;
const customParagraphStyle = ` 
  margin-bottom: 55px;
  position: relative;
  z-index: 2;
  // max-width: 230px;
  @media ${SIZES.lg} {
    margin-bottom: 27px;
  }
 `;

const StyledSpan = styled.span`
  color: ${COLORS.LIGHT_DANGER};
`;

type SectionCarouselProps = {
  data: {
    title: string;
    text: string;
    href: string;
    carousel: {
      title: string;
      text: string;
      imageSrc: string;
      imageSrc_2x: string;
      href: string;
    }[];
  };
};

const smPadding = `
  @media ${SIZES.m} {
    padding: 55px 0 0;
    overflow: hidden;
  }
`;

const SectionCarousel = (props: SectionCarouselProps) => {
  const { data } = props;
  const { title, text, carousel, href } = data;
  const intl = useIntl();
  return (
    <StyledContainer>
      <Section customPadding={'70px 0 8px'} customStyle={smPadding}>
        <Container>
          <Row>
            <StyledTitle>
              <SectionTitleLarge>
                <>
                  {intl.formatMessage({ id: title })}
                  <StyledSpan>.</StyledSpan>
                </>
              </SectionTitleLarge>
            </StyledTitle>
          </Row>
          <StyledRow>
            <StyledFirstCol>
              <ParagraphNormal customStyles={customParagraphStyle}>
                {intl.formatMessage({ id: text })}
              </ParagraphNormal>
              <Link href={href}>
                <Button type={buttonsType.largeDanger}>
                  {intl.formatMessage({ id: 'button_label_book_demo' })}
                </Button>
              </Link>
            </StyledFirstCol>
            <StyledSecondColumn>
              <CarouselBlock data={carousel} type={carouselTypeEnum.solution} />
            </StyledSecondColumn>
          </StyledRow>
        </Container>
      </Section>
    </StyledContainer>
  );
};

export default SectionCarousel;
