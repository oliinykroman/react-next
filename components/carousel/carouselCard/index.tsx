import React from 'react';
import styled from 'styled-components';
import { carouselDataType, carouselTypeEnum } from '../index';
import { COLORS, SIZES } from '../../../const';
import Button, { buttonsType } from '../../button';
import { useIntl } from 'react-intl';
import Link from 'next/link';

const StyledCard = styled.div`
  border-radius: 12px;
  background: transparent;
  box-shadow: 0 14px 25px rgba(0, 0, 0, 0.35);
  margin: 0 25px 40px;
  max-width: 360px;
  @media ${SIZES.sm} {
    margin: 0 10px 40px;
  }
`;

const StyledTop = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: flex-start;
  width: 100%;
  // @ts-ignore
  height: ${(props) =>
    // @ts-ignore
    props.type === carouselTypeEnum.partnership ? '145px' : '153px'};
  position: relative;
  background-position: bottom;
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;
  z-index: 2;
  background-image: ${(props) =>
    // @ts-ignore
    props.type === carouselTypeEnum.partnership ? 'url("/images/industryLogoBg.png")' : ''};
  // @ts-ignore
  padding: ${(props) =>
    // @ts-ignore
    props.type === carouselTypeEnum.partnership ? '25px 30px;' : ''};
`;

const StyledImage = styled.div`
  flex-shrink: 0;

  width: ${(props) => {
    // @ts-ignore
    return props.type === carouselTypeEnum.solution ? '140px' : 'auto';
  }};
  max-width: 211px;
  position: relative;
  ${(props) => {
    // @ts-ignore
    return props.type === carouselTypeEnum.partnership
      ? 'display: flex;  height: 60px;  align-items: center;'
      : '';
  }};
  img {
    flex-shrink: 0;
    margin-left: 3px;
    position: relative;
    z-index: 5;
    ${(props) => {
      // @ts-ignore
      return props.type === carouselTypeEnum.solution ? 'filter: grayscale(50);' : '';
    }}
  }
  &:after {
    content: '';
    width: 190px;
    height: 190px;
    position: absolute;
    left: -18px;
    top: -50px;
    z-index: 0;
    border-radius: 50%;
    background: ${COLORS.LIGHT_DANGER};
    display: ${(props) => {
      // @ts-ignore
      return props.type === carouselTypeEnum.solution ? 'block' : 'none';
    }};
  }

  &:before {
    content: '';
    width: 130px;
    height: 130px;
    border-radius: 50%;
    position: absolute;
    top: -26px;
    left: 10px;
    z-index: 1;
    box-shadow: 0 3.6383px 20.2128px rgba(0, 0, 0, 0.35);
    display: ${(props) => {
      // @ts-ignore
      return props.type === carouselTypeEnum.solution ? 'block' : 'none';
    }};
  }
`;

const StyledContent = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  flex-grow: 1;
  padding: 0 30px 30px;
  // @ts-ignore
  background: ${(props) =>
    // @ts-ignore
    props.cardCustomBg};
  position: relative;

  &:before {
    content: '';
    width: 100%;
    height: 100%;
    position: absolute;
    top: -40px;
    left: 0;
    // @ts-ignore
    background: ${(props) =>
      // @ts-ignore
      props.cardCustomBg};
  }
  > * {
    position: relative;
    z-index: 2;
  }
`;

const StyledTitle = styled.h3`
  font-family: 'codec_proextrabold';
  font-weight: 600;
  font-size: 28px;
  line-height: 32px;
  text-transform: capitalize;
  white-space: pre-line;
  margin-bottom: ${(props) => {
    // @ts-ignore
    return props.type === carouselTypeEnum.solution ? '10px' : 0;
  }};
  color: ${(props) => {
    // @ts-ignore
    return props.type === carouselTypeEnum.solution ? COLORS.WHITE : COLORS.DARK;
  }}; // @ts-ignore
  min-height: ${(props) =>
    // @ts-ignore
    props.minHeight ? '64px;' : 'auto;'};
  display: -webkit-box;
  ${(props) =>
    // @ts-ignore
    props.minHeight
      ? 'min-height:64px;\n  -webkit-line-clamp: 2;\n  -webkit-box-orient: vertical;\n  overflow: hidden;'
      : ''}

  @media ${SIZES.m} {
    font-size: 20px;
    line-height: 22px;
    margin-bottom: 15px;
  }
`;

const StyledSubTitle = styled.h3`
  font-weight: 400;
  font-size: 15px;
  line-height: 32px;
  margin-bottom: ${(props) => {
    // @ts-ignore
    return props.type === carouselTypeEnum.solution ? '10px' : 0;
  }};
  color: ${COLORS.DANGER};
`;

const StyledText = styled.p`
  font-weight: 400;
  font-size: 16px;
  line-height: 20px;
  color: ${(props) => {
    // @ts-ignore
    return props.type === carouselTypeEnum.solution ? COLORS.WHITE : COLORS.DARK;
  }};
  margin-bottom: ${(props) => {
    // @ts-ignore
    return props.type === carouselTypeEnum.solution ? '50px' : '95px';
  }};

  @media ${SIZES.m} {
    font-size: 14px;
    line-height: 18px;
    margin-bottom: 9px;
  }
`;

const Inner = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  background: ${(props) => {
    // @ts-ignore
    return props.type === carouselTypeEnum.solution ? COLORS.DANGER : COLORS.WHITE;
  }};
  border-radius: 12px;
  overflow: hidden;
`;

const StyledBtn = styled.div`
  margin-top: auto;
`;
const CarouselCard = (props: {
  data: carouselDataType;
  type: carouselTypeEnum;
  titleMinHeight: boolean;
  cardCustomBg: string;
}) => {
  const { data, type, titleMinHeight = false, cardCustomBg = '' } = props;
  const { title, text, imageSrc, imageSrc_2x, subtitle = '', href } = data;
  const intl = useIntl();
  return (
    <StyledCard>
      <Inner
        // @ts-ignore
        type={type}>
        <StyledTop
          // @ts-ignore
          type={type}>
          <StyledImage
            // @ts-ignore
            type={type}>
            {/* <img src={imageSrc} alt={intl.formatMessage({ id: title })} /> */}

            <picture>
              <source srcSet={imageSrc_2x} type="image/png" />
              <img src={imageSrc} alt={intl.formatMessage({ id: title })} />
            </picture>
          </StyledImage>
        </StyledTop>
        <StyledContent
          // @ts-ignore
          cardCustomBg={cardCustomBg}>
          <StyledTitle
            // @ts-ignore
            minHeight={titleMinHeight}
            type={type}>
            {intl.formatMessage({ id: title })}
            <span className="text-light-danger">.</span>
          </StyledTitle>
          {subtitle && (
            <StyledSubTitle
              // @ts-ignore
              type={type}>
              {intl.formatMessage({ id: subtitle })}
            </StyledSubTitle>
          )}
          <StyledText
            // @ts-ignore
            type={type}>
            {intl.formatMessage({ id: text })}
          </StyledText>
          {type === carouselTypeEnum.solution && (
            <StyledBtn>
              <Link href={href}>
                <Button type={buttonsType.smallWhite}>
                  {intl.formatMessage({ id: 'button_label_learn_more' })}
                </Button>
              </Link>
            </StyledBtn>
          )}
        </StyledContent>
      </Inner>
    </StyledCard>
  );
};

export default CarouselCard;
