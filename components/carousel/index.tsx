import React from 'react';
import 'react-multi-carousel/lib/styles.css';
import Carousel from 'react-multi-carousel';
import CarouselCard from './carouselCard';
import styled from 'styled-components';
import ArrowLeft from '../../svgComponent/ArrowLeft';
import ArrowRight from '../../svgComponent/ArrowRight';
import { SIZES } from '../../const';

export type carouselDataType = {
  title: string;
  text: string;
  imageSrc: string;
  imageSrc_2x: string;
  subtitle?: string;
  href: string;
};

export enum carouselTypeEnum {
  partnership,
  solution
}

export type CarouselProps = {
  data: carouselDataType[];
  type: carouselTypeEnum;
  slidesToShow?: number;
  titleMinHeight?: boolean;
  cardCustomBg?: string;
};

const StyledWrapper = styled.div`
  ul {
    margin-bottom: 45px;
    margin-left: 15px;

    @media ${SIZES.m} {
      margin-bottom: 50px;
      margin-left: 0;
    }
    li {
      display: flex;
      margin-top: 10px;
    }
  }
`;

const StyledButton = styled.button`
  cursor: pointer;
  transition: 0.3s;
  &:first-child {
    margin-right: 20px;
  }
  &:hover {
    opacity: 0.8;
  }
`;

const StyledButtonGroup = styled.div`
  display: flex;
  position: absolute;
  left: 22px;
  bottom: 0;
  @media ${SIZES.m} {
    left: 41px;
  }
`;

const CustomLeftArrow = ({ onClick, ...rest }) => {
  return (
    <StyledButton
      onClick={() => {
        onClick();
      }}
      aria-label="Go to prev slide">
      <ArrowLeft />
    </StyledButton>
  );
};

const CustomRightArrow = ({ onClick, ...rest }) => {
  return (
    <StyledButton
      onClick={() => {
        onClick();
      }}
      aria-label="Go to next slide">
      <ArrowRight />
    </StyledButton>
  );
};

const ButtonGroup = ({ next, previous }) => {
  return (
    <StyledButtonGroup className="carousel-button-group">
      <CustomLeftArrow onClick={() => previous()} />
      <CustomRightArrow onClick={() => next()} />
    </StyledButtonGroup>
  );
};

const CarouselBlock = (props: CarouselProps) => {
  const { data, type, slidesToShow = 4, titleMinHeight = false, cardCustomBg } = props;

  const responsive = {
    desktopXXl: {
      breakpoint: { max: 3000, min: 1660 },
      items: slidesToShow
    },
    desktopXl: {
      breakpoint: { max: 1660, min: 1360 },
      items: 3
    },
    tabletLg: {
      breakpoint: { max: 1360, min: 991 },
      items: type === carouselTypeEnum.solution ? 2 : 3
    },
    mobileM: {
      breakpoint: { max: 991, min: 767 },
      items: 3
    },
    mobile: {
      breakpoint: { max: 767, min: 0 },
      items: 2
    }
  };
  return (
    <StyledWrapper>
      <Carousel
        infinite
        responsive={responsive}
        arrows={false}
        customButtonGroup={
          // @ts-ignore
          <ButtonGroup />
        }>
        {data?.map((item, index) => {
          return (
            <CarouselCard
              key={`item_${index}`}
              data={item}
              type={type}
              titleMinHeight={titleMinHeight}
              cardCustomBg={cardCustomBg}
            />
          );
        })}
      </Carousel>
    </StyledWrapper>
  );
};

export default CarouselBlock;
