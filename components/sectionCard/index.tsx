import React from 'react';
import styled from 'styled-components';
import Section from '../grid/section';
import Row from '../grid/row';
import { GRID_COLUMN_PADDING, COLORS, SIZES, SIZES_MIN } from '../../const';
import Container from '../grid/container';
import { useIntl } from 'react-intl';

type CardDataType = {
  title?: string;
  text?: string;
  bgColor?: string;
  bgItemColor?: string;
};

type SectionCardProps = {
  data?: CardDataType;
  children?: React.ReactNode;
  imageBackground?: boolean;
};

const Column = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
  padding: 0 42px;
  @media ${SIZES_MIN.m} {
    padding: 0 ${GRID_COLUMN_PADDING};
  }
`;

const SectionTitle = styled.h2`
  color: ${COLORS.DARK};
  font-family: 'codec_proextrabold';
  font-size: 24px;
  line-height: 1;
  width: 100%;
  margin: 0 0 20px;
  @media (min-width: 768px) {
    font-size: 36px;
    margin: 0 0 20px 58px;
  }
`;

const SectionText = styled.p`
  color: ${COLORS.DARK};
  font-size: 16px;
  max-width: 702px;
  margin: 0 0 30px 0;
  @media (min-width: 768px) {
    font-size: 18px;
    margin: 0 0 80px 58px;
  }
`;

const CardItemWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-left: -20px;
  margin-right: -20px;
`;

const CardItem = styled.div`
  background-color: ${(item) =>
    // @ts-ignore
    item.bgItemColor ? item.bgItemColor : COLORS.WHITE};
  box-shadow: 0px 2px 6px rgba(0, 0, 0, 0.15);
  border-radius: 12px;
  display: flex;
  flex-wrap: wrap;
  flex-direction: column;
  padding: 20px 20px 22px;
  &:not(:last-child) {
    margin: 0 0 30px;
  }
  @media (min-width: 992px) {
    width: calc(100% / 3 - 40px);
    padding: 36px 38px 50px;
    &:not(:last-child) {
      margin: 0 20px;
    }
  }
  .card-title {
    color: ${COLORS.DARK};
    font-family: 'codec_proextrabold';
    font-size: 20px;
    line-height: 1.1;
    margin: 0 0 20px;
    @media (min-width: 768px) {
      font-size: 28px;
      white-space: pre-line;
    }
  }
`;

const StyledContent = styled.p`
  color: ${COLORS.DARK};
  font-size: 16px;
  line-height: 1.3;
  @media (min-width: 768px) {
    font-size: 18px;
  }
`;

const CardSection = (props: SectionCardProps) => {
  const { data } = props;
  const { title, text, bgColor } = data;
  const intl = useIntl();
  return (
    <Section customPadding={'80px 0'} bgColor={bgColor}>
      <Container>
        <Row>
          <Column>
            {title && (
              <SectionTitle>
                {intl.formatMessage({ id: title })}
                <span className="text-light-danger">.</span>
              </SectionTitle>
            )}
            {text && <SectionText>{intl.formatMessage({ id: text })}</SectionText>}

            <CardItemWrapper>
              {
                // @ts-ignore
                data.items.map((item, index) => {
                  const { item_title, item_text, bgItemColor } = item;
                  return (
                    <CardItem
                      key={`item-${index}`}
                      // @ts-ignore
                      bgItemColor={bgItemColor}>
                      <h4 className="card-title">
                        {intl.formatMessage({ id: item_title })}
                        <span className="text-light-danger">.</span>
                      </h4>
                      <StyledContent>{intl.formatMessage({ id: item_text })}</StyledContent>
                    </CardItem>
                  );
                })
              }
            </CardItemWrapper>
          </Column>
        </Row>
      </Container>
    </Section>
  );
};

export default CardSection;
