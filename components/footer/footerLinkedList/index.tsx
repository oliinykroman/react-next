import React from 'react';
import styled from 'styled-components';
import { useIntl } from 'react-intl';
import { COLORS } from '../../../const';
import { useRouter } from 'next/router';
import Link from 'next/link';

type FooterLinkedListProps = {
  data: { title: string; href: string }[];
};

const StyledList = styled.ul`
  display: flex;
  flex-direction: column;
  li {
    margin-bottom: 12px;
    @media (min-width: 768px) {
      margin-bottom: 15px;
    }
    a {
      position: relative;
      transition: 0.3s;
      &:after {
        content: '';
        position: absolute;
        bottom: -2px;
        left: 0;
        width: 0;
        height: 2px;
        background: ${COLORS.LIGHT_DANGER};
        transition: 0.3s;
      }
      &:hover {
        &:after {
          width: 50%;
        }
      }
    }
  }
`;

const FooterLinkedList = (props: FooterLinkedListProps) => {
  const { data } = props;
  const intl = useIntl();
  const router = useRouter();
  return (
    <StyledList>
      {data.map((item, index) => {
        const { title, href } = item;
        return (
          <li key={`li-${index}`}>
            <Link href={href} locale={router.locale}>
              <a>{intl.formatMessage({ id: title })}</a>
            </Link>
          </li>
        );
      })}
    </StyledList>
  );
};

export default FooterLinkedList;
