import React from 'react';
import Section from '../grid/section';
import Container from '../grid/container';
import Row from '../grid/row';
import styled from 'styled-components';
import FooterLogoIcon from '../../svgComponent/FooterLogoIcon';
import { COLORS, SIZES, SIZES_MIN } from '../../const';
import FooterLinkedList from './footerLinkedList';
import { aboutList, developersList, featureList } from '../../configs/fooretMenuConfig';
import FooterIcons from './footerIcons';
import FooterBottom from './footerBottom';
import Link from 'next/link';

const RowBetween = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
  @media ${SIZES.xl} {
    flex-wrap: wrap;
  }
  @media ${SIZES.m} {
    padding: 0 27px;
  }
  .footer-nav-wrap {
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    width: 100%;
    @media ${SIZES_MIN.xl} {
      padding-left: 70px;
      padding-right: 90px;
    }
  }
`;
const FooterNav = styled.nav`
  display: flex;
  flex-direction: column;
  color: #fff;
  font-size: 14px;
  margin-bottom: 34px;
  @media (min-width: 768px) {
    margin-bottom: 0;
  }
  @media (min-width: 992px) {
    font-size: 18px;
  }
  &:first-child {
    @media (max-width: 767px) {
      order: 2;
    }
  }
  &:nth-child(2) {
    @media (max-width: 767px) {
      order: 1;
      width: 100%;
    }
  }
  &:last-child {
    @media (max-width: 767px) {
      order: 3;
    }
  }
`;
const FooterLogo = styled.a`
  display: flex;
  margin-bottom: 40px;
  width: 100%;
  max-width: 230px;
  @media ${SIZES.lg} {
    display: none;
  }
  @media (min-width: 1200px) {
    margin-bottom: 15px;
  }
  &:hover {
    opacity: 0.8;
  }
`;
const FooterTitle = styled.h3`
  color: ${COLORS.DANGER};
  text-transform: uppercase;
  margin-bottom: 13px;
  font-family: 'codec_proextrabold';
  font-size: 18px;
  line-height: 1.6;
  @media (min-width: 768px) {
    margin-bottom: 29px;
  }
`;

const StyledIconsWrapper = styled.div`
  display: flex;
  padding-top: 27px;
  padding-bottom: 25px;
  border-bottom: 1px solid #555555;
  border-top: 1px solid #555555;
  margin: 12px 0 30px;
  overflow: hidden;
  @media (max-width: 767px) {
    overflow-x: scroll;
    padding-left: 42px;
    width: calc(100% + 15px);
  }
  @media (min-width: 1200px) {
    margin: 68px 0 75px;
  }
`;
const IconsRow = styled.div`
  display: flex;
  flex-wrap: nowrap;
`;

const FooterLogoMobile = styled.a`
  display: none;
  margin-bottom: 40px;
  width: 100%;
  max-width: 230px;
  @media ${SIZES.lg} {
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;

const Footer = () => {
  return (
    <>
      <Section customPadding={'80px 0'} bgColor={COLORS.DARK}>
        <Container>
          <Row>
            <RowBetween>
              <FooterLogoMobile href="/">
                <img src={'/images/footerLogoMobile.png'} />
              </FooterLogoMobile>
              <FooterLogo href={'/'} className={'column'}>
                {' '}
                <FooterLogoIcon />
              </FooterLogo>
              <div className={'footer-nav-wrap'}>
                <FooterNav className={'column'}>
                  <FooterTitle>about</FooterTitle>
                  <FooterLinkedList data={aboutList} />
                </FooterNav>
                <FooterNav className={'column'}>
                  <FooterTitle>features</FooterTitle>
                  <FooterLinkedList data={featureList} />
                </FooterNav>
                <FooterNav className={'column'}>
                  <FooterTitle>developers</FooterTitle>
                  <FooterLinkedList data={developersList} />
                </FooterNav>
              </div>
            </RowBetween>
          </Row>
          <StyledIconsWrapper>
            <Row>
              <IconsRow>
                <FooterIcons />
              </IconsRow>
            </Row>
          </StyledIconsWrapper>
          <Row>
            <FooterBottom />
          </Row>
        </Container>
      </Section>
    </>
  );
};

export default Footer;
