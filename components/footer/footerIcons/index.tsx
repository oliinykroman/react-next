import React from 'react';
import Image from 'next/image';
import styled from 'styled-components';
import { certificateList } from '../../../configs/fooretMenuConfig';
import { COLORS } from '../../../const';
import { useIntl } from 'react-intl';

const StyledWrapper = styled.div`
  padding: 0 15px;
  text-align: center;
  &:not(:last-child) {
    margin-right: 20px;
  }
  min-width: 190px;
  @media (min-width: 992px) {
    width: calc(100% / 5 - 30px);
    min-width: unset;
  }
`;

const StyledInner = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
const StyledImage = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 10px;
  height: 75px;
  & img {
    max-width: none;
    max-height: 75px;
    @media (min-width: 768px) {
      max-width: 100%;
      max-height: 75px;
    }
  }
`;
const StyledText = styled.p`
  color: ${COLORS.WHITE};
  font-size: 10px;
`;
const FooterIcons = () => {
  const intl = useIntl();
  return (
    <>
      {certificateList.map((item, index) => {
        const { image, image_2x, alt, description } = item;
        return (
          <StyledWrapper key={`item-${index}`}>
            <StyledInner>
              <StyledImage>
                <picture>
                  <source srcSet={image_2x} type="image/png" />
                  <img src={image} alt={alt} />
                </picture>
              </StyledImage>
              <StyledText>{intl.formatMessage({ id: description })}</StyledText>
            </StyledInner>
          </StyledWrapper>
        );
      })}
    </>
  );
};

export default FooterIcons;
