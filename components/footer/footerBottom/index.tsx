import React from 'react';
import styled from 'styled-components';
import { socialLink } from '../../../configs/socilaLink';
import { COLORS, SIZES, SIZES_MIN } from '../../../const';
import { useIntl } from 'react-intl';

const StyledWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column-reverse;
  font-size: 13px;
  align-items: center;
  text-align: center;
  @media ${SIZES.m} {
    padding: 0 42px;
  }
  @media ${SIZES_MIN.m} {
    flex-direction: row;
    justify-content: space-between;
    text-align: left;
  }
  & * {
    color: ${COLORS.WHITE};
    display: flex;
    align-items: center;
    line-height: 1;
  }
  p {
    font-size: 10px;
    margin: 30px 0 0;
    @media ${SIZES_MIN.m} {
      font-size: 13px;
      margin: 0;
    }
  }
  a {
    margin: 0 0 5px 8px;
  }
`;

const { linkedIn } = socialLink;
const { href, Icon } = linkedIn;

const FooterBottom = () => {
  const intl = useIntl();
  return (
    <StyledWrapper className={'column'}>
      <p>{intl.formatMessage({ id: 'footer_bottom_copy' })}</p>
      <div>
        {intl.formatMessage({ id: 'footer_bottom_follow' })}
        <a href={href}>{<Icon />}</a>
      </div>
    </StyledWrapper>
  );
};

export default FooterBottom;
