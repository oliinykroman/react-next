import React from 'react';
import styled from 'styled-components';
import Section from '../grid/section';
import Row from '../grid/row';
import { GRID_COLUMN_PADDING, COLORS, ROUT_CONTENT_MODERATION, SIZES } from '../../const';
import Container from '../grid/container';
import { useIntl } from 'react-intl';
import Button, { buttonsType } from '../button';
import { useRouter } from 'next/router';
import InforIcon from '../../svgComponent/InforIcon';
import ParagraphNormal from '../typografy/paragraphNormal';
import Link from 'next/link';

type HowItWorksDataType = {
  subtitle: string;
  title: string;
  text: string;
  text_2?: string;
  bgColor?: string;
  image?: string;
  image_2x?: string;
  alt?: string;
  href: string;
  button_1?: {
    type: buttonsType;
    label: string;
    action: any;
  };
  button_2?: {
    type: buttonsType;
    label: string;
    action: any;
  };
  contentText?: string;
};

type SectionHowItWorksProps = {
  data?: HowItWorksDataType;
  children?: React.ReactNode;
  imageBackground?: boolean;
  contentSection?: boolean;
};

const Column = styled.div`
  padding: 0 ${GRID_COLUMN_PADDING};
  display: flex;
  flex-wrap: wrap;
  max-width: 100%;
  @media ${SIZES.m} {
    padding: 0 42px;
  }
`;

const TextWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: 0 0 145px;
  width: 100%;
  position: relative;
  @media (min-width: 768px) {
    justify-content: space-between;
  }
  &:after {
    content: '';
    border-radius: 50%;
    width: 95%;
    height: 25px;
    position: absolute;
    left: 0;
    right: 0;
    margin: auto;
    bottom: -55px;
    z-index: 0;
    box-shadow: 4px 5px 6px rgb(0 0 0 / 28%);
  }
  &:before {
    content: '';
    width: 100%;
    height: 40px;
    position: absolute;
    left: 0;
    bottom: -55px;
    z-index: 1;
    background: ${COLORS.BEIGE};
  }
  .text-holder {
    @media (min-width: 992px) {
      width: 50%;
    }
  }
  .button-holder {
    display: flex;
    align-items: flex-end;
    justify-content: flex-end;
    margin: 25px 0 0;
    flex-direction: column;
    @media (min-width: 992px) {
      margin: 0;
      width: 50%;
    }
    @media (min-width: 768px) {
      flex-direction: row;
    }
    button {
      & + button {
        display: none;
        margin: 10px 0 0 0;
        @media (min-width: 768px) {
          margin: 0 0 0 13px;
        }
      }
    }
  }
`;

const SectionSubTitle = styled.p`
  font-family: 'codec_proextrabold';
  font-size: 20px;
  line-height: 1.35;
  color: ${COLORS.DANGER};
  text-transform: uppercase;
  width: 100%;
  @media (min-width: 768px) {
    font-size: 24px;
  }
`;

const SectionTitle = styled.h2`
  color: ${COLORS.DARK};
  font-family: 'codec_proextrabold';
  font-size: 24px;
  line-height: 1;
  max-width: 550px;
  margin-bottom: 24px;
  @media (min-width: 768px) {
    font-size: 36px;
  }
`;

const SectionText = styled.p`
  color: ${COLORS.DARK};
  font-size: 16px;
  @media (min-width: 768px) {
    font-size: 18px;
  }
  & + p {
    margin: 30px 0 0;
  }
`;

const ImageWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  @media ${SIZES.m} {
    width: 100vw;
    max-width: 100%;
  }
`;

const StyledImage = styled.div`
  margin: 0 0 50px;
  width: 100%;
  ${(props) => {
    // @ts-ignore
    return props.pathName.match(ROUT_CONTENT_MODERATION)
      ? 'display: flex; justify-content: center; margin-bottom: 95px;'
      : '';
  }}

  @media ${SIZES.m} {
    overflow-x: scroll;
    min-width: 100vw;
    padding: 0 50px;
    margin: 0;
  }
  img {
    margin-left: auto;
    margin-right: auto;
    max-width: none;
    padding: 0 50px 0 0;
    @media (min-width: 768px) {
      max-width: 100%;
      padding: 0;
    }
  }
`;

const StyledContentWrapper = styled.div`
  display: flex;
  align-self: flex-start;
  width: 100%;
  max-width: 795px;
  background: #fcfcfc;
  margin: 45px auto 55px;
  box-shadow: 0px 2px 6px rgba(0, 0, 0, 0.15);
  border-radius: 12px;
  padding: 25px 20px;
`;

const StyledIcon = styled.div`
  width: 70px;
  height: 70px;
  margin-right: 25px;
  flex-shrink: 0;
`;

const StyledFlexCol = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const SectionHowItWorks = (props: SectionHowItWorksProps) => {
  const { data, contentSection = false } = props;
  const { href, subtitle, title, text, text_2, bgColor, image, image_2x, alt } = data;
  const intl = useIntl();
  const rout = useRouter();
  return (
    <Section customPadding={'90px 0 80px'} bgColor={bgColor}>
      <Container>
        <Row>
          <Column>
            <TextWrapper>
              <div className="text-holder">
                <SectionSubTitle>{intl.formatMessage({ id: subtitle })}</SectionSubTitle>
                <SectionTitle>
                  {intl.formatMessage({ id: title })}
                  <span className="text-light-danger">.</span>
                </SectionTitle>
                <SectionText>{intl.formatMessage({ id: text })}</SectionText>
                {data?.text_2 && <SectionText>{intl.formatMessage({ id: text_2 })}</SectionText>}
              </div>

              {(data?.button_1 || data?.button_2) && (
                <div className="button-holder">
                  {data?.button_1 && (
                    <Button type={data?.button_1.type} onClick={data?.button_1.action}>
                      {intl.formatMessage({ id: data?.button_1.label })}
                    </Button>
                  )}
                  {data?.button_2 && (
                    <Button type={data?.button_2.type} onClick={data?.button_2.action}>
                      {intl.formatMessage({ id: data?.button_2.label })}
                    </Button>
                  )}
                </div>
              )}
            </TextWrapper>

            <ImageWrapper>
              <StyledImage
                // @ts-ignore
                pathName={rout.pathname}>
                <picture>
                  <source srcSet={image_2x} type="image/png" />
                  <img src={image} alt={alt} />
                </picture>
              </StyledImage>
              <StyledFlexCol>
                {contentSection && (
                  <StyledContentWrapper>
                    <StyledIcon>
                      <InforIcon />
                    </StyledIcon>
                    <ParagraphNormal>
                      {intl.formatMessage({ id: data?.contentText })}
                    </ParagraphNormal>
                  </StyledContentWrapper>
                )}
                <Link href={href}>
                  <Button type={buttonsType.largeDanger}>
                    {intl.formatMessage({ id: 'button_label_book_demo' })}
                  </Button>
                </Link>
              </StyledFlexCol>
            </ImageWrapper>
          </Column>
        </Row>
      </Container>
    </Section>
  );
};

export default SectionHowItWorks;
