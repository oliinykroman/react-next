import React from 'react';
import styled from 'styled-components';
import { COLORS, GRID_COLUMN_PADDING, SIZES } from '../../../const';
import { useIntl } from 'react-intl';
import Row from '../../grid/row';
import Container from '../../grid/container';
import SectionTitleLarge from '../../typografy/sectionTitleLarge';
import CarouselBlock, { carouselTypeEnum } from '../../carousel';
import Section from '../../grid/section';
import SectionTitleMedium from '../../typografy/sectionTitleMedium';

const StyledFirstCol = styled.div`
  padding: 0 ${GRID_COLUMN_PADDING};
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  width: 343px;
  flex-shrink: 0;
  position: relative;
  background: ${COLORS.WHITE};
  &:after {
    content: '';
    border-radius: 50%;
    width: 25px;
    height: 100%;
    position: absolute;
    right: 0;
    top: -68px;
    z-index: 0;
    box-shadow: 4px 0 6px rgb(0 0 0 / 28%);
  }
  &:before {
    content: '';
    width: 40px;
    height: 100%;
    position: absolute;
    right: 0;
    top: -68px;
    z-index: 1;
   background: ${COLORS.WHITE};
  }
}
`;

const StyledSecondColumn = styled.div`
  width: 100vw;
  margin-left: -25px;
  margin-bottom: 80px;

  .react-multi-carousel-item {
    & > div {
      box-shadow: none;
    }
  }

  .react-multi-carousel-item--active {
    & > div {
      box-shadow: 0 14px 25px rgb(0 0 0 / 35%);
    }
  }

  .carousel-button-group {
    left: 40px;

    @media ${SIZES.m} {
      left: 15px;
    }
  }

  @media ${SIZES.m} {
    margin-left: 0;
    width: 150vw;
  }
`;

const StyledContainer = styled.div`
  overflow-x: hidden;
`;

const StyledRow = styled.div`
  display: flex;
  margin-left: -15px;
  margin-right: -15px;
  flex-wrap: nowrap;
`;
const StyledTitle = styled.div`
  padding: 0 ${GRID_COLUMN_PADDING};
  margin-bottom: 55px;
`;
type partnershipData = {
  title: string;
  carousel: { logo: any; title: string; subtitle: string; text: string }[];
  bgColor: string;
};

type PartnershipCarouselProps = {
  data: partnershipData;
  titleMinHeight?: boolean;
  cardCustomBg?: string;
};

const PartnershipCarousel = (props: PartnershipCarouselProps) => {
  const { data, titleMinHeight, cardCustomBg = '' } = props;
  const { title, carousel, bgColor } = data;
  const intl = useIntl();
  return (
    <StyledContainer>
      <Section bgColor={bgColor} customPadding={'70px 0 8px'}>
        <Container>
          <Row>
            <StyledTitle>
              <SectionTitleMedium>
                {intl.formatMessage({ id: title })}
                <span className="text-light-danger">.</span>
              </SectionTitleMedium>
            </StyledTitle>
          </Row>
          <StyledRow>
            <StyledSecondColumn>
              <CarouselBlock
                // @ts-ignore
                data={carousel}
                type={carouselTypeEnum.partnership}
                slidesToShow={5}
                titleMinHeight={titleMinHeight}
                cardCustomBg={cardCustomBg}
              />
            </StyledSecondColumn>
          </StyledRow>
        </Container>
      </Section>
    </StyledContainer>
  );
};

export default PartnershipCarousel;
