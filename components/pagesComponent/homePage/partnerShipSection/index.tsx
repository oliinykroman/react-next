import React from 'react';
import styled from 'styled-components';
import Section from '../../../grid/section';
import Container from '../../../grid/container';
import Link from 'next/link';
import Button, { buttonsType } from '../../../button';
import ParagraphNormal from '../../../typografy/paragraphNormal';
import PartnerShipIcons from '../partnerShipIcons';
import { useIntl } from 'react-intl';
import { SIZES } from '../../../../const';

const Partnership = styled.div`
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  width: 100%;
  @media ${SIZES.lg} {
    padding: 0 30px;

    .text-divider {
      width: calc(100% + 20px);
      margin-left: -10px;
      margin-right: -10px;
    }
  }
  @media ${SIZES.m} {
    flex-direction: column;
  }
  .text-holder {
    display: flex;
    justify-content: center;
    width: 100%;
    margin: 39px 0;
    text-align: center;
    p {
      max-width: 793px;
    }

    @media ${SIZES.lg} {
      margin-bottom: 23px;
    }
  }
  .icon-wrap {
    display: flex;
    justify-content: space-between;
    max-width: 1170px;
    margin: 0 auto;
    width: 100%;
    @media ${SIZES.m} {
      overflow-x: scroll;
      // margin-left: 30px;
      width: calc(100vw - 20vw);
    }
    @media ${SIZES.sm} {
      width: calc(100vw - 75px);
    }
  }
  button {
    padding: 12px 35px;
    font-size: 14px;
  }
`;
const StyledButtonWrapper = styled.div`
  @media ${SIZES.lg} {
    padding-left: 55px;
  }
`;
const smPadding = `
  @media ${SIZES.m} {
    padding: 0 0 55px;
    overflow: hidden;
  }
`;

const PartnerShipSection = () => {
  const intl = useIntl();
  return (
    <Section customPadding={'0 0 80px'} customStyle={smPadding}>
      <Container>
        <Partnership>
          <p className="text-divider">
            {intl.formatMessage({ id: 'home_partnership_section_title' })}
          </p>
          <div className="icon-wrap">
            <PartnerShipIcons />
          </div>
          <div className="text-holder">
            <ParagraphNormal>
              {intl.formatMessage({ id: 'home_partnership_section_p1' })}
            </ParagraphNormal>
          </div>
          <StyledButtonWrapper>
            <Link href="/partnership">
              <a>
                <Button type={buttonsType.smallDanger}>
                  {intl.formatMessage({ id: 'button_label_partners' })}
                </Button>
              </a>
            </Link>
          </StyledButtonWrapper>
        </Partnership>
      </Container>
    </Section>
  );
};

export default PartnerShipSection;
