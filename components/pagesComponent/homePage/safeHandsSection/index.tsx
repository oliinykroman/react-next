import React from 'react';
import styled from 'styled-components';
import LogoAge from '../../../../svgComponent/LogoAge';
import Section from '../../../grid/section';
import Row from '../../../grid/row';
import { COLORS, GRID_COLUMN_PADDING } from '../../../../const';
import { useIntl } from 'react-intl';

type SafeHandsDataType = {
  image: string;
  alt: string;
  title: string;
  text: string;
  image_2: string;
  alt_2: string;
  image_2x: string;
  image_2_2x: string;
  bgColor: string;
};

type SectionSafeHandsProps = {
  data?: SafeHandsDataType;
  children?: React.ReactNode;
  imageBackground?: boolean;
};

const ImageWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: flex-start;
  flex: 0 0 auto;
  margin: 0 auto;
  @media (min-width: 768px) {
    width: 42%;
  }
  .img {
    background-color: ${COLORS.YELLOW};
    border-radius: 8px;
    box-shadow: 0px 20px 60px rgba(0, 0, 0, 0.35);
    padding-bottom: 15px;
    margin: 0 auto;
    @media (min-width: 768px) {
      max-width: 247px;
    }
    @media (min-width: 992px) {
      margin-right: 17%;
    }
  }
`;

const TextWrapper = styled.div`
  margin: 60px auto 0;
  text-align: center;
  @media (min-width: 768px) {
    width: 58%;
    margin: 5px 0 0 25px;
    text-align: left;
  }
  @media (min-width: 992px) {
    margin: 5px 0 0 58px;
  }
  svg,
  img {
    @media (max-width: 767px) {
      margin: 0 auto;
    }
  }
`;

const Column = styled.div`
  padding: 0 ${GRID_COLUMN_PADDING};
  display: flex;
  width: 100%;
  @media (max-width: 767px) {
    flex-wrap: wrap;
    padding: 0 42px;
  }
`;

const SectionTitle = styled.h2`
  font-family: 'codec_proextrabold';
  font-size: 24px;
  line-height: 1;
  margin: 32px 0 25px;
  @media (min-width: 768px) {
    font-size: 36px;
  }
`;

const SectionText = styled.p`
  font-size: 16px;
  margin: 0 0 17px;
  @media (min-width: 768px) {
    font-size: 18px;
  }
`;

const ButtonWrapper = styled.div`
  margin: 35px 0 0;
`;

const StyledCustomContainer = styled.div`
  width: 100%;
  max-width: 1300px;
  margin: 0 auto;
  padding-right: ${GRID_COLUMN_PADDING};
  padding-left: ${GRID_COLUMN_PADDING};
`;

const StyledSpan = styled.span`
  color: ${COLORS.YELLOW};
`;

const SafeHandsSection = (props: SectionSafeHandsProps) => {
  const { data } = props;
  const { image, alt, title, text, image_2, alt_2, image_2x, image_2_2x, bgColor } = data;
  const intl = useIntl();
  return (
    <Section customPadding={'94px 0'} bgColor={bgColor}>
      <StyledCustomContainer>
        <Row>
          <Column>
            <ImageWrapper>
              <picture className="img">
                <source srcSet={image_2x} type="image/png" />
                <img src={image} alt={alt} />
              </picture>
            </ImageWrapper>
            <TextWrapper>
              <LogoAge />
              <SectionTitle>
                {intl.formatMessage({ id: title })}
                <StyledSpan>.</StyledSpan>
              </SectionTitle>
              <SectionText>{intl.formatMessage({ id: text })}</SectionText>
              <picture>
                <source srcSet={image_2_2x} type="image/png" />
                <img src={image_2} alt={alt_2} />
              </picture>
              <ButtonWrapper>
                <a
                  className="largeYellow"
                  target="_blank"
                  href="https://www.verifymyage.com/"
                  rel="noreferrer">
                  {intl.formatMessage({ id: 'button_label_learn_more' })}
                </a>
              </ButtonWrapper>
            </TextWrapper>
          </Column>
        </Row>
      </StyledCustomContainer>
    </Section>
  );
};

export default SafeHandsSection;
