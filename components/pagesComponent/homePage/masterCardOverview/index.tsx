import React from 'react';
import styled from 'styled-components';
import Section from '../../../grid/section';
import Container from '../../../grid/container';
import Row from '../../../grid/row';
import { COLORS, GRID_COLUMN_PADDING, SIZES } from '../../../../const';
import SectionTitleMedium from '../../../typografy/sectionTitleMedium';
import { useIntl } from 'react-intl';
import ParagraphNormal from '../../../typografy/paragraphNormal';
import MasterCardLogo from '../../../../svgComponent/MasterCardLogo';
import Button, { buttonsType } from '../../../button';
import Link from 'next/link';

const StyledColumnFirst = styled.div`
  padding: 0 42px;
  text-align: left;
  order: 1;
  & > * {
    white-space: normal;
  }
  @media (min-width: 992px) {
    & > * {
      white-space: pre-line;
    }
    width: 49%;
    padding: 0 ${GRID_COLUMN_PADDING};
  }
`;
const StyledColumnSecond = styled.div`
  padding: 0 20px;
  order: 2;
  @media (min-width: 992px) {
    width: 51%;
    padding: 0 ${GRID_COLUMN_PADDING};
  }
`;

type MasterCardOverviewProps = {
  title: string;
  text: string;
  quote: string;
  smallTitle: string;
  bgColor: string;
  link: { src: string; label: string };
  listData: { listTitle: string; listContent: string }[];
  href: string;
};

const Quote = styled.blockquote`
  font-style: italic;
  font-weight: 600;
  font-size: 18px;
  line-height: 1.2;
  margin: 0 auto;
  @media (min-width: 992px) {
    max-width: 525px;
    margin: 0;
  }
`;

const StyledTitle = styled.h3`
  margin-top: 24px;
  font-family: 'codec_proextrabold';
  font-weight: 600;
  font-size: 20px;
  line-height: 34px;
`;

const StyledItem = styled.li`
  display: flex;
  align-items: stretch;
  background: ${COLORS.WHITE};
  box-shadow: 0 2px 6px rgba(0, 0, 0, 0.15);
  border-radius: 12px;
  padding: 13px 23px;
  margin-bottom: 23px;
  @media ${SIZES.lg} {
    padding: 13px 28px 20px 23px;
  }
`;

const StyledIndex = styled.div`
  display: flex;
  align-items: flex-start;
  padding-top: 2px;
  font-family: 'codec_proextrabold';
  font-weight: 600;
  font-size: 36px;
  line-height: 1;
  border-right: 1px solid #eae9f0;
  padding-right: 15px;
  margin-right: 15px;
  @media (min-width: 768px) {
    font-size: 75px;
    padding-top: 11px;
  }
  span:nth-child(1) {
    display: flex;
    align-items: center;
    @media (min-width: 768px) {
      font-family: 'Source Sans Pro', sans-serif;
      width: 52px;
    }
  }
`;

const ListColumn = styled.div`
  display: flex;
  flex-direction: column;
`;

const ListTitle = styled.h3`
  font-family: 'codec_proextrabold';
  font-style: normal;
  font-weight: 600;
  font-size: 20px;
  line-height: 1;
  margin-top: 2px;
  margin-bottom: 6px;
  @media (min-width: 768px) {
    font-size: 28px;
    margin-top: 8px;
  }
`;

const StyledList = styled.ul`
  padding-top: 42px;
`;

const StyledLink = styled.a`
  display: inline-block;
  margin-bottom: 4px;
  font-style: normal;
  font-weight: 400;
  font-size: 16px;
  transition: 0.3s;
  text-decoration: underline;
  &:hover {
    opacity: 0.8;
  }
  @media (min-width: 768px) {
    margin-bottom: 30px;
    font-size: 18px;
  }
`;

const StyledIconWrapper = styled.div`
  @media (min-width: 992px) {
    margin-bottom: 47px;
  }
  svg {
    max-width: 105px;
    @media ${SIZES.m} {
      max-width: 71px;
      margin: 0 auto 0 0;
    }
  }
`;

const StyledButton = styled.div`
  order: 3;
  margin: 0 auto 0 42px;
  @media (min-width: 992px) {
    margin: 0;
  }
  @media (min-width: 1200px) {
    margin: -65px 0 0;
  }
`;

const customTitleStyle =
  'margin-bottom: 23px;\n  @media (min-width: 992px) {\n    margin-bottom: 50px;\n  }';
const customPStyle =
  'margin-bottom: 20px; max-width: 525px;   @media (max-width: 991px) {\n    margin: 0 0 20px;\n  }';

const MasterCardOverview = (props: { data: MasterCardOverviewProps }) => {
  const { data } = props;
  const { title, quote, text, smallTitle, link, listData, bgColor, href } = data;
  const intl = useIntl();

  return (
    <Section bgColor={bgColor}>
      <Container>
        <Row>
          <StyledColumnFirst>
            <SectionTitleMedium customStyles={customTitleStyle}>
              {intl.formatMessage({ id: title })}
              <span className="text-light-danger">.</span>
            </SectionTitleMedium>
            <ParagraphNormal customStyles={customPStyle}>
              {intl.formatMessage({ id: text })}
            </ParagraphNormal>
            <Quote>{intl.formatMessage({ id: quote })}</Quote>
            <StyledTitle>{intl.formatMessage({ id: smallTitle })}</StyledTitle>
            <StyledLink href={link.src}>{intl.formatMessage({ id: link.label })}</StyledLink>
            <StyledIconWrapper>
              <MasterCardLogo />
            </StyledIconWrapper>
          </StyledColumnFirst>

          <StyledButton>
            <Link href={href}>
              <Button type={buttonsType.largeDanger}>
                {intl.formatMessage({ id: 'button_label_get_in_touch' })}
              </Button>
            </Link>
          </StyledButton>

          <StyledColumnSecond>
            <StyledList>
              {listData?.map((item, index) => {
                const { listTitle, listContent } = item;
                return (
                  <StyledItem key={`item_${index}`}>
                    <StyledIndex>
                      <span>{index + 1}</span>
                      <span className="text-light-danger">.</span>
                    </StyledIndex>
                    <ListColumn>
                      <ListTitle>
                        {intl.formatMessage({ id: listTitle })}
                        <span className="text-light-danger">.</span>
                      </ListTitle>
                      <ParagraphNormal>{intl.formatMessage({ id: listContent })}</ParagraphNormal>
                    </ListColumn>
                  </StyledItem>
                );
              })}
            </StyledList>
          </StyledColumnSecond>
        </Row>
      </Container>
    </Section>
  );
};

export default MasterCardOverview;
