import React from 'react';
import styled from 'styled-components';
import { partnershipList } from '../../../../configs/partnershipMenuConfig';
import { SIZES } from '../../../../const';

const StyledWrapper = styled.div`
  padding: 0 15px;
  display: flex;
  align-items: center;
  justify-content: center;
  flex: 21%;

  @media ${SIZES.m} {
    margin-right: 20px;
  }
`;
const StyledImage = styled.div`
  display: flex;
  & img {
    max-width: none;
    @media (min-width: 768px) {
      max-width: 100%;
    }
  }
`;
const PartnershipIcons = () => {
  return (
    <>
      {partnershipList.map((item, index) => {
        const { image, image_2x, alt } = item;
        return (
          <StyledWrapper key={`item-${index}`}>
            <StyledImage>
              <picture>
                <source srcSet={image_2x} type="image/png" />
                <img src={image} alt={alt} />
              </picture>
            </StyledImage>
          </StyledWrapper>
        );
      })}
    </>
  );
};

export default PartnershipIcons;
