import React, { useState } from 'react';
import styled from 'styled-components';
import { COLORS } from '../../../../../const';

const StyledLabel = styled.label`
  width: 100%;
  display: flex;
  position: relative;
  margin-bottom: 14px;
  @media (min-width: 992px) {
    margin-bottom: 30px;
  }
  .label {
    position: absolute;
    left: 26px;
    bottom: ${(props) => {
      // @ts-ignore
      return props.validValue ? '25px' : '10px';
    }};
    font-size: ${(props) => {
      // @ts-ignore
      return props.validValue ? '12px' : '15px';
    }};
    @media (min-width: 768px) {
      bottom: ${(props) => {
        // @ts-ignore
        return props.validValue ? '30px' : '18px';
      }};
    }
  }
`;

const StyledInput = styled.input`
  width: 100%;
  background: ${COLORS.LIGHT_BEIGE_OPACITY_06};
  border-radius: 35px;
  padding: 12px 26px 8px;
  font-size: 14px;
  font-weight: 600;
  border: 1px solid
    ${(props) => {
      // @ts-ignore
      return props.validValue ? COLORS.DARK : 'transparent';
    }};
  @media (min-width: 768px) {
    padding: 25px 26px 9px;
  }
  &:focus {
    border: 1px solid ${COLORS.DARK};
  }
`;

const initState = {
  valid: false
};

const InputRow = (props: {
  label: string;
  name: string;
  defaultValue: string;
  onChange: (name: string, value: string) => void;
}) => {
  const { label, onChange, name, defaultValue } = props;
  const [inputState, setInputState] = useState(initState);
  const { valid } = inputState;
  const onInputChange = (event) => {
    const value = event.currentTarget.value ? event.currentTarget.value : '';
    setInputState((state) => ({
      ...state,
      valid: Boolean(value)
    }));
    onChange(name, value);
  };
  const onFocus = () => {
    setInputState((state) => ({
      ...state,
      valid: true
    }));
  };

  return (
    <StyledLabel
      // @ts-ignore
      validValue={valid}>
      <span className="label">{label}</span>
      <StyledInput
        name={name}
        // @ts-ignore
        validValue={valid}
        required={true}
        onBlur={onInputChange}
        onFocus={onFocus}
        onChange={onInputChange}
        value={defaultValue}
      />
    </StyledLabel>
  );
};

export default InputRow;
