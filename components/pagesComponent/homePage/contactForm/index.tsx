import React, { useState } from 'react';
import styled from 'styled-components';
import Button, { buttonsType } from '../../../button';
import InputRow from './inputRow';
import ParagraphNormal from '../../../typografy/paragraphNormal';
import {
  BUILD_ENV,
  COLORS,
  FORM_ID_DEV,
  FORM_ID_PROD,
  FORM_SUBMIT_URL,
  GRID_COLUMN_PADDING,
  SIZES
} from '../../../../const';
import Link from 'next/link';
import { useIntl } from 'react-intl';
import API, { envType } from '../../../../api/API';
// @ts-ignore
const formId = BUILD_ENV === envType.dev ? FORM_ID_DEV : FORM_ID_PROD;

const StyledForm = styled.form`
  display: flex;
  flex-direction: column;
  width: 100%;
  max-width: 520px;
  @media (min-width: 992px) {
    padding-left: 21px;
  }
  a {
    font-size: 12px;
    padding: 0 26px;
    margin-top: -10px;
    margin-bottom: 17px;
    @media (min-width: 992px) {
      margin-top: -20px;
      margin-bottom: 38px;
    }
  }
  button {
    padding: 21px 70px;
    font-size: 15px;

    @media ${SIZES.lg} {
      padding: 10px;
    }
  }
`;

export const StyledButtonWrapper = styled.div`
  margin-left: 0;
`;

const customTextStyle = `font-family: 'codec_proextrabold'; color: ${COLORS.DARK}; text-align: left; margin-bottom: 25px; @media ${SIZES.lg} {
    margin-bottom: 20px;
  }`;

const StyledError = styled.div`
  width: 100%;
  padding: ${GRID_COLUMN_PADDING};
  color: ${COLORS.WHITE};
  background: ${COLORS.DANGER};
  font-size: 14px;
  font-weight: 600;
  font-family: 'codec_proextrabold';
  margin-bottom: 20px;
  line-height: 1;
  box-shadow: 0px 2px 6px rgb(0 0 0 / 15%);
  border-radius: 5px;
`;

const StyledSuccess = styled.div`
  width: 100%;
  padding: ${GRID_COLUMN_PADDING};
  color: ${COLORS.WHITE};
  background: ${COLORS.GREEN};
  font-size: 14px;
  font-weight: 600;
  font-family: 'codec_proextrabold';
  margin-bottom: 20px;
`;

const formInitState = {
  firstname: '',
  lastname: '',
  email: '',
  phone: '',
  message: '',
  business_name: '',
  website: ''
};

const ContactForm = () => {
  const intl = useIntl();
  const [formState, setFromState] = useState(formInitState);
  const [disabled, setDisabled] = useState(false);
  const [formSubmitMessage, setFormSubmitMessage] = useState({ status: '', message: '' });
  const onChange = (name: string, value: string) => {
    setFromState((state) => ({
      ...state,
      [name]: value
    }));
  };
  const onSubmit = async (event) => {
    event.preventDefault();
    setDisabled(true);
    setFormSubmitMessage({ status: '', message: '' });
    const data = Object.keys(formState).map((item) => {
      return {
        name: item,
        value: formState[item]
      };
    });
    const path = `${FORM_SUBMIT_URL}/${formId}`;
    const response = await API.sendFormData(path, { fields: data });
    if (response?.response?.data?.errors?.length) {
      setFormSubmitMessage((state) => {
        return {
          ...state,
          status: 'error',
          message: response?.response?.data?.errors[0].message
        };
      });
    } else {
      setFormSubmitMessage((state) => {
        return {
          ...state,
          status: 'success',
          message: response?.data?.inlineMessage.replace(/(<([^>]+)>)/gi, '')
        };
      });
    }
    setDisabled(false);
  };
  return (
    <StyledForm onSubmit={onSubmit}>
      <ParagraphNormal customStyles={customTextStyle}>
        Ready to see a demo? Complete the form below and a member of our team will be in touch
        shortly.
      </ParagraphNormal>
      <InputRow
        name="firstname"
        onChange={onChange}
        label={intl.formatMessage({ id: 'home_contact_input_1' })}
        defaultValue={formState.firstname}
      />
      <InputRow
        name="lastname"
        onChange={onChange}
        label={intl.formatMessage({ id: 'home_contact_input_2' })}
        defaultValue={formState.lastname}
      />
      <InputRow
        name="email"
        onChange={onChange}
        label={intl.formatMessage({ id: 'home_contact_input_3' })}
        defaultValue={formState.email}
      />
      <InputRow
        name="phone"
        onChange={onChange}
        label={intl.formatMessage({ id: 'home_contact_input_4' })}
        defaultValue={formState.phone}
      />
      <InputRow
        name="business_name"
        onChange={onChange}
        label={intl.formatMessage({ id: 'home_contact_input_5' })}
        defaultValue={formState.business_name}
      />
      <InputRow
        name="website"
        onChange={onChange}
        label={intl.formatMessage({ id: 'home_contact_input_6' })}
        defaultValue={formState.website}
      />
      <InputRow
        name="message"
        onChange={onChange}
        label={intl.formatMessage({ id: 'home_contact_input_7' })}
        defaultValue={formState.message}
      />

      {formSubmitMessage.status === 'error' && (
        <StyledError>{formSubmitMessage.message}</StyledError>
      )}
      {formSubmitMessage.status === 'success' && (
        <StyledSuccess>{formSubmitMessage.message}</StyledSuccess>
      )}

      <Link href="#">{intl.formatMessage({ id: 'home_contact_label' })}</Link>

      <StyledButtonWrapper>
        <Button type={buttonsType.largeDanger} disabled={disabled}>
          {intl.formatMessage({ id: 'button_label_submit' })}
        </Button>
      </StyledButtonWrapper>
    </StyledForm>
  );
};

export default ContactForm;
