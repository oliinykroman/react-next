import React from 'react';
import styled from 'styled-components';
import Section from '../../../grid/section';
import Row from '../../../grid/row';
import { GRID_COLUMN_PADDING, COLORS } from '../../../../const';
import Container from '../../../grid/container';
import { useIntl } from 'react-intl';

type NumberDataType = {
  number: string;
  title: string;
  text: string;
  bgColor: string;
};

type SectionNumberProps = {
  data?: NumberDataType;
  children?: React.ReactNode;
  imageBackground?: boolean;
};

const NumberWrapper = styled.div`
  background-color: ${COLORS.LIGHT_DANGER};
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  flex: 0 0 auto;
  height: 90px;
  width: 90px;
  margin-bottom: 9px;
  @media (min-width: 768px) {
    height: 200px;
    width: 200px;
    margin-bottom: 0;
  }
`;

const Number = styled.div`
  color: ${COLORS.WHITE};
  font-family: sans-serif;
  font-size: 65px;
  font-weight: 900;
  line-height: 1;
  @media (min-width: 768px) {
    font-size: 144px;
  }
`;

const TextWrapper = styled.div`
  margin: 10px auto 0;
  max-width: 790px;
  @media (min-width: 768px) {
    margin: 10px 0 0 60px;
  }
`;

const Column = styled.div`
  padding: 0 42px;
  display: flex;
  width: 100%;
  flex-wrap: wrap;
  @media (min-width: 768px) {
    justify-content: center;
    flex-wrap: nowrap;
    padding: 0 ${GRID_COLUMN_PADDING};
  }
`;

const SectionTitle = styled.h2`
  font-family: 'codec_proextrabold';
  font-size: 38px;
  line-height: 1;
  color: ${COLORS.WHITE};
  margin-bottom: 17px;
  @media (min-width: 768px) {
    font-size: 60px;
  }
`;

const SectionText = styled.p`
  font-size: 18px;
  color: ${COLORS.WHITE};
  @media (min-width: 768px) {
    font-size: 24px;
  }
`;

const NumberSection = (props: SectionNumberProps) => {
  const { data } = props;
  const { title, text, bgColor, number } = data;
  const intl = useIntl();
  return (
    <Section customPadding={'80px 0'} bgColor={bgColor}>
      <Container>
        <Row>
          <Column>
            <NumberWrapper>
              <Number>{intl.formatMessage({ id: number })}</Number>
            </NumberWrapper>
            <TextWrapper>
              <SectionTitle>
                {intl.formatMessage({ id: title })}
                <span className="text-light-danger">.</span>
              </SectionTitle>
              <SectionText>{intl.formatMessage({ id: text })}</SectionText>
            </TextWrapper>
          </Column>
        </Row>
      </Container>
    </Section>
  );
};

export default NumberSection;
