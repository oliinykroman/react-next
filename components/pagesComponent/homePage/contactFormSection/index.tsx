import React from 'react';
import styled from 'styled-components';
import Section from '../../../grid/section';
import Container from '../../../grid/container';
import Row from '../../../grid/row';
import ContactForm from '../contactForm';
import SectionTitleMedium from '../../../typografy/sectionTitleMedium';
import { COLORS, GRID_COLUMN_PADDING, SIZES } from '../../../../const';
import CustomerIcon from '../../../../svgComponent/CustomerIcon';
import ParagraphNormal from '../../../typografy/paragraphNormal';
import Vimeo from '@u-wave/react-vimeo';
import { FormattedMessage, useIntl } from 'react-intl';

const StyledColumnFirst = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 auto 47px;
  padding: 0 42px;
  @media (min-width: 992px) {
    flex: 0 0 55%;
    margin: 0;
    padding: 0 ${GRID_COLUMN_PADDING};
  }
  .video {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    max-width: 620px;
    height: 100%;
    iframe {
      width: 100%;
      height: 100%;
      max-width: 620px;
      box-shadow: 0 20px 60px rgb(0 0 0 / 35%);
    }
  }
  svg {
    width: 100%;
    max-width: 90px;
    @media (min-width: 768px) {
      max-width: 100px;
    }
  }
`;

const StyledColumnSecond = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 auto;
  padding: 0 42px;
  @media (min-width: 992px) {
    padding: 0 20px;
    flex: 0 0 45%;
    padding: 0 ${GRID_COLUMN_PADDING};
  }
`;

const titleStyle = `
  color: ${COLORS.DARK};
   padding: 19px 0 13px;
   @media (min-width: 992px) {
    padding: 40px 0 30px; 
  }
`;

const customTextStyle = `
  padding: 0 0 35px;
  max-width: 600px;
  @media (min-width: 992px) {
    padding: 0 0 50px;
  }
`;

const smPadding = `
  @media ${SIZES.m} {
    padding: 61px 0 55px;
  }
`;

const VideoWrapper = styled.div`
  width: 100%;
  padding-top: 52%;
  position: relative;

  @media (max-width: 1280px) {
    padding-top: 56%;
  }

  @media ${SIZES.lg} {
    width: calc(100% + 40px);
    margin-left: -20px;
    margin-right: -20px;
    padding-top: 62%;
  }
`;

const ContactFormSection = () => {
  const intl = useIntl();
  return (
    <Section customPadding={'80px 0 88px'} customStyle={smPadding} id="contactForm">
      <Container>
        <Row>
          <StyledColumnFirst>
            <CustomerIcon />
            <SectionTitleMedium customStyles={titleStyle}>
              <FormattedMessage
                id="home_contact_section_title"
                values={{
                  span: (msg: string) => <span className={'text-light-danger'}>{msg}</span>
                }}
              />
            </SectionTitleMedium>
            <ParagraphNormal customStyles={customTextStyle}>
              {intl.formatMessage({ id: 'home_contact_section_p1' })}
            </ParagraphNormal>
            <VideoWrapper>
              <Vimeo
                video="https://player.vimeo.com/610652354"
                autoplay={false}
                width="100%"
                height="100%"
                className="video"
              />
            </VideoWrapper>
          </StyledColumnFirst>
          <StyledColumnSecond>
            <ContactForm />
          </StyledColumnSecond>
        </Row>
      </Container>
    </Section>
  );
};

export default ContactFormSection;
