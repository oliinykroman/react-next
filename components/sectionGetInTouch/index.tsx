import React from 'react';
import styled from 'styled-components';
import Section from '../grid/section';
import Row from '../grid/row';
import { GRID_COLUMN_PADDING, COLORS } from '../../const';
import Container from '../grid/container';
import Button, { buttonsType } from '../button';
import { useIntl } from 'react-intl';
import Link from 'next/link';

type GetInTouchDataType = {
  title: string;
  text: string;
  bgColor: string;
  href: string;
  button: {
    type: buttonsType;
    label: string;
    action: any;
  };
};

type SectionGetInTouchProps = {
  data: GetInTouchDataType;
  children: React.ReactNode;
  imageBackground: boolean;
};

const SectionTitle = styled.h2`
  font-family: 'codec_proextrabold';
  font-size: 36px;
  line-height: 1;
  color: ${COLORS.WHITE};
  @media (max-width: 1199px) {
    width: 100%;
    margin: 0 0 30px;
  }
`;

const SectionText = styled.p`
  font-size: 18px;
  color: ${COLORS.WHITE};
  width: 100%;
  max-width: 628px;
  padding: 0 0 30px;
  @media (min-width: 1200px) {
    padding: 0 60px 0 0;
  }
`;

const Column = styled.div`
  padding: 0 ${GRID_COLUMN_PADDING};
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  @media (max-width: 1199px) {
    flex-wrap: wrap;
    padding: 0 42px;
  }
`;

const ButtonWrapper = styled.div`
  display: flex;
  button {
    font-size: 14px;
    padding: 22px 53px;

    @media (max-width: 1199px) {
      padding: 10px;
    }
  }
`;

const SectionGetInTouch = (props: SectionGetInTouchProps) => {
  const { data } = props;
  const { title, text, bgColor, button, href } = data;
  const intl = useIntl();
  return (
    <Section customPadding={'205px 0 203px'} bgColor={bgColor}>
      <Container>
        <Row>
          <Column>
            <SectionTitle>
              {intl.formatMessage({ id: title })}
              <span className="text-light-danger">.</span>
            </SectionTitle>
            <SectionText>{intl.formatMessage({ id: text })}</SectionText>
            <ButtonWrapper>
              <Link href={href}>
                <Button type={button.type} onClick={button.action}>
                  {intl.formatMessage({ id: button.label })}
                </Button>
              </Link>
            </ButtonWrapper>
          </Column>
        </Row>
      </Container>
    </Section>
  );
};

export default SectionGetInTouch;
