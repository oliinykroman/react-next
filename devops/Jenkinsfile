pipeline {

    agent { node { label 'jenkins-slave-node' } }

    environment {

        BITBUCKET_USER_TOKEN = credentials ('bitbucket_user_token')
        GCP_REGION = credentials ('gcp_region')
        GCR_HOSTNAME = credentials ('gcp_hostname')
        DEFAULT_GCP_PROJECT_ID = credentials ('default_gcp_project_id')
        KUBECONFIG_DEV = credentials ('gcp_kubernetes_config_dev')
        KUBECONFIG_PRD = credentials ('gcp_kubernetes_config_prd')
        SONAR_USER = credentials ('sonar_user')
        SONAR_URL = credentials ('sonar_url')
    }

    options {
        timeout(time: 1, unit: 'HOURS')
    }

    stages {

        stage('nucleus-devops') {
            steps {
                checkout([
                    $class: 'GitSCM',
                    branches: [[name: '*/master']],
                    doGenerateSubmoduleConfigurations: false,
                    extensions: [[$class: 'RelativeTargetDirectory',
                    relativeTargetDir: 'nucleus-devops']],
                    submoduleCfg: [],
                    userRemoteConfigs: [[credentialsId: 'svc-devops', url: 'https://bitbucket.org/verifymyage/nucleus-devops.git']]
                ])
            }
        }
        
        stage('Tagging') {
            steps {
                parallel(
                    'Tagging app': {
                        sh 'bash -x nucleus-devops/pipeline-shared-librares/tagging.sh'
                    },
                    'Branch Check': {
                        sh 'echo ' + env.GIT_BRANCH
                    }
                )
            }   
        }

        stage('Unit Test') {
            steps {
                sh 'bash -x nucleus-devops/pipeline-shared-librares/unit_test.sh'
            }
        }

        stage('SonarQube Scan') {
            steps {
                sh 'bash -x nucleus-devops/pipeline-shared-librares/sonar_scan.sh'
            }
        }

        stage('Build Application') {
            steps {
                sh 'bash -x nucleus-devops/pipeline-shared-librares/build_package.sh'
            }
        }

        stage('Build & Push Images to Registry DEV') {
            when {
                expression { env.GIT_BRANCH == 'origin/dev' }
            }
            steps {
                sh 'bash -x nucleus-devops/pipeline-shared-librares/docker.sh -r $GCP_REGION -d $GCR_HOSTNAME -e dev'
            }
        }

        stage('Deploy DEV') {
            when {
                expression { env.GIT_BRANCH == 'origin/dev' }
            }
            steps {
                sh 'bash -x nucleus-devops/pipeline-shared-librares/deploy.sh -r $GCP_REGION -e dev -i deploy -d k8s'
                sh 'bash -x nucleus-devops/pipeline-shared-librares/check_upstart_app.sh -u $URL_APP/status -s 200 -t 120 -c 5 -e dev'
            }
        }

        stage('Build & Push Images to Registry PRD') {
            when {
                expression { env.GIT_BRANCH == 'origin/master' }
            }
            steps {
                sh 'bash -x nucleus-devops/pipeline-shared-librares/docker.sh -r $GCP_REGION -d $GCR_HOSTNAME -e prd'
            }
        }

        stage('Deploy PRD') {
            when {
                expression { env.GIT_BRANCH == 'origin/master' }
            }
            steps {
                sh 'bash -x nucleus-devops/pipeline-shared-librares/deploy.sh -r $GCP_REGION -e prd -i deploy -d k8s'
                sh 'bash -x nucleus-devops/pipeline-shared-librares/check_upstart_app.sh -u $URL_APP/status -s 200 -t 120 -c 5 -e prd'
            }
        }
        
    }
}