import { createGlobalStyle } from 'styled-components';
import { COLORS, GRID_COLUMN_PADDING, SIZES, SIZES_MIN } from '../../const';

const GlobalStyle = createGlobalStyle`
  @font-face {
    font-family: 'Codec Pro';
    src: url('/fontsNew/Codec-Pro-Regular.woff2') format('woff2'),
    url('/fontsNew/Codec-Pro-Regular.woff') format('woff');
    font-weight: normal;
    font-style: normal;
  }
  
  @font-face {
    font-family: 'Codec Pro Bold';
    src: url('/fontsNew/Codec-Pro-Bold.woff2') format('woff2'),
    url('/fontsNew/Codec-Pro-Bold.woff') format('woff');
    font-weight: normal;
    font-style: normal;
  }

  @font-face {
    font-family: 'codec_proextrabold';
    src: url('/fontsNew/Codec-Pro-Extrabold.woff2') format('woff2'),
    url('/fontsNew/Codec-Pro-Extrabold.woff') format('woff');
    font-weight: normal;
    font-style: normal;

  }
  body {
    margin: 0;
    padding: 0;
    font-family: 'Codec Pro';
  }
  .h3-title {
    font-style: normal;
    font-weight: 700;
    font-size: 18px;
    line-height: 22px;
  }
  .column {
    padding: 0 15px;
  }
  .arrow-down {
    width: 0;
    height: 0;
    border-style: solid;
    border-width: 4px 3.5px 0 3.5px;
    border-color: ${COLORS.GRAY} transparent transparent transparent;
    //margin: 2px 0 0 3px;
    margin: -2px 0 0 -5px;
  }
  .text-danger {
    color: ${COLORS.DANGER};
  }
  .text-light-danger {
    color: ${COLORS.LIGHT_DANGER};
  }
  .text-divider {
    position: relative;
    font-weight: 600;
    font-size: 16px;
    text-align: center;
    width: 100%;
    margin: 0 0 35px;
    &:before,
    &:after {
      content: '';
      background-color: ${COLORS.GRAY_81}; 
      width: 24vw;
      height: 1px;
      position: absolute;
      top: 50%;
      display: none;
      max-width: 430px;
      @media (min-width: 992px) {
        display: block;
      }
    }
    &:before {
      left: 15px;
    }
    &:after {
      right: 15px;
    }
  }

  .largeYellow {
    border-radius: 44px;
    border: 2px solid;
    border-color: ${COLORS.YELLOW};
    background-color: ${COLORS.YELLOW};
    cursor: pointer;
    font-weight: 600;
    font-family: 'codec_proextrabold';
    font-size: 15px;
    transition: 0.3s;
    padding: 18px 70px;
    display: inline-block;
    &:hover {
      border-color: #ffcc6a;
      background-color: #ffcc6a;
    }
  }
  
  .stickyBox {
    padding: 0 ${GRID_COLUMN_PADDING};
    @media ${SIZES_MIN.lg} {
      flex: 33.3333% 0 0;
    }
    @media ${SIZES.lg} {
      position: static!important;
    }
  }
  body{
    @media ${SIZES.xl} {
      padding-top: 60px;
    }
  } 
  
  html {
    scroll-behavior: smooth;
  }
`;

export default GlobalStyle;
