import axios from 'axios';
import { API_URL } from '../const';

export enum envType {
  dev = 'dev',
  prod = 'prod'
}

type RequestHeaders = {
  [key: string]: string;
};

type RequestOptions = {
  headers: RequestHeaders;
};

class Api {
  private static instance: Api;
  private apiUrl = API_URL;
  private headers: RequestHeaders = {};
  constructor() {
    if (typeof Api.instance === 'object') {
      return Api.instance;
    }
    Api.instance = this;
    return Api.instance;
  }

  public callPostUrl = async (path: string = '', body: object = {}) => {
    const options: RequestOptions = {
      headers: { ...this.headers }
    };
    options.headers['Content-Type'] = 'Content-Type: application/json';
    try {
      const res = await axios
        .post(`${this.apiUrl}${path}`, body, {
          // ...options
        })
        .then((response) => {
          return response;
        });
      return res;
    } catch (error) {
      return error;
    }
  };

  public sendFormData = async (path: string, data: any) => {
    return await this.callPostUrl(path, data);
  };
}

export default new Api();
