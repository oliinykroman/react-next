import * as React from 'react';

const Image = (props) => (
  <svg
    width={43}
    height={40}
    viewBox="0 0 43 40"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}>
    <g clipPath="url(#clip0_0_2214)">
      <path
        d="M20.6 40.0046C31.647 40.0046 40.6023 31.0493 40.6023 20.0023C40.6023 8.95534 31.647 0 20.6 0C9.553 0 0.597656 8.95534 0.597656 20.0023C0.597656 31.0493 9.553 40.0046 20.6 40.0046Z"
        fill="#E93C8D"
      />
      <path
        d="M20.5997 15.987C21.3807 15.987 22.0139 15.3539 22.0139 14.5729C22.0139 13.7918 21.3807 13.1587 20.5997 13.1587C19.8187 13.1587 19.1855 13.7918 19.1855 14.5729C19.1855 15.3539 19.8187 15.987 20.5997 15.987Z"
        stroke="white"
        strokeWidth={0.927321}
        strokeMiterlimit={10}
        strokeLinecap="square"
      />
      <path
        d="M11.2148 24.9033H29.9792"
        stroke="#121212"
        strokeWidth={1.39098}
        strokeMiterlimit={10}
        strokeLinecap="square"
      />
      <path
        d="M11.2148 22.8863L14.989 17.8927L18.7632 22.8863L24.3689 15.4585L29.9792 22.8863"
        stroke="white"
        strokeWidth={0.927321}
        strokeMiterlimit={10}
        strokeLinecap="square"
      />
      <path
        d="M29.9792 10.6179H11.2148V29.3823H29.9792V10.6179Z"
        stroke="#121212"
        strokeWidth={1.39098}
        strokeMiterlimit={10}
        strokeLinecap="square"
      />
    </g>
    <defs>
      <clipPath id="clip0_0_2214">
        <rect width="42.303" height="40" fill="white" />
      </clipPath>
    </defs>
  </svg>
);

export default Image;
