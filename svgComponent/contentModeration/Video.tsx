import * as React from 'react';

const Video = (props) => (
  <svg
    width={41}
    height={40}
    viewBox="0 0 41 40"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}>
    <path
      d="M20.6566 40.0046C31.7036 40.0046 40.6589 31.0493 40.6589 20.0023C40.6589 8.95534 31.7036 0 20.6566 0C9.60964 0 0.654297 8.95534 0.654297 20.0023C0.654297 31.0493 9.60964 40.0046 20.6566 40.0046Z"
      fill="#E93C8D"
    />
    <path
      d="M31.3206 11.6565H9.99219V24.1753H31.3206V11.6565Z"
      stroke="#121212"
      strokeWidth={1.39098}
      strokeMiterlimit={10}
    />
    <path
      d="M18.8018 15.3657V20.9296L23.4384 18.1477L18.8018 15.3657Z"
      stroke="white"
      strokeWidth={0.927321}
      strokeLinejoin="round"
    />
    <path
      d="M18.3242 27.8843H31.7843"
      stroke="#121212"
      strokeWidth={1.39098}
      strokeMiterlimit={10}
    />
    <path
      d="M15.556 29.2756C16.3242 29.2756 16.947 28.6529 16.947 27.8846C16.947 27.1164 16.3242 26.4937 15.556 26.4937C14.7878 26.4937 14.165 27.1164 14.165 27.8846C14.165 28.6529 14.7878 29.2756 15.556 29.2756Z"
      stroke="white"
      strokeWidth={0.927321}
      strokeLinejoin="round"
    />
    <path
      d="M12.8249 27.8843H9.52832"
      stroke="#121212"
      strokeWidth={1.39098}
      strokeMiterlimit={10}
    />
  </svg>
);

export default Video;
