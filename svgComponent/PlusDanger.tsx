import * as React from 'react';
import { SVGProps } from 'react';

const PlusDanger = (props: SVGProps<SVGSVGElement>) => (
  <svg width={48} height={48} fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
    <circle cx={24} cy={24} transform="rotate(-90 24 24)" fill="#E93C8D" r={24} />
    <path d="M13 24h21.5M23.75 34.75v-21.5" stroke="#fff" strokeWidth={3} />
  </svg>
);

export default PlusDanger;
