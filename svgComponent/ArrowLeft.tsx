import * as React from 'react';
import { SVGProps } from 'react';

const ArrowLeft = (props: SVGProps<SVGSVGElement>) => (
  <svg width={50} height={50} fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
    <circle cx={25} cy={25} r={24} fill="#fff" stroke="#A60054" strokeWidth={2} />
    <path
      d="M13.293 24.293a1 1 0 0 0 0 1.414l6.364 6.364a1 1 0 0 0 1.414-1.414L15.414 25l5.657-5.657a1 1 0 0 0-1.414-1.414l-6.364 6.364ZM36 24H14v2h22v-2Z"
      fill="#A60054"
    />
  </svg>
);

export default ArrowLeft;
