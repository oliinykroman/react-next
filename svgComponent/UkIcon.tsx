import * as React from 'react';
import { SVGProps } from 'react';

const UkIcon = (props: SVGProps<SVGSVGElement>) => (
  <svg width={21} height={21} fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
    <circle cx={10.5} cy={10.5} r={10.5} fill="#C4C4C4" />
    <mask
      id="a"
      style={{
        maskType: 'alpha'
      }}
      maskUnits="userSpaceOnUse"
      x={0}
      y={0}
      width={21}
      height={21}>
      <circle cx={10.5} cy={10.5} r={10.5} fill="#C4C4C4" />
    </mask>
    <g mask="url(#a)">
      <path d="M-10.979-.115V21.26h43.037V-.115H-10.98Z" fill="#012169" />
      <path
        d="M-10.979-.115 32.058 21.26-10.98-.115Zm43.037 0L-10.98 21.259 32.058-.115Z"
        fill="#000"
      />
      <path
        d="M31.096 23.173 10.54 12.96l-20.557 10.212-1.923-3.828 17.667-8.774-17.667-8.774 1.923-3.828L10.54 8.181 31.096-2.031l1.924 3.828-17.668 8.774 17.668 8.774-1.924 3.828Z"
        fill="#fff"
      />
      <path
        d="M-10.979-.115 32.058 21.26-10.98-.115Zm43.037 0L-10.98 21.259 32.058-.115Z"
        fill="#000"
      />
      <path
        d="M31.415 22.534 10.539 12.163l-20.876 10.371-1.285-2.55L7.333 10.57l-18.955-9.412 1.285-2.551L10.54 8.979 31.415-1.392l1.285 2.55-18.955 9.413L32.7 19.983l-1.285 2.551Z"
        fill="#C8102E"
      />
      <path d="M10.54-.115V21.26-.115ZM-10.98 10.572h43.037-43.037Z" fill="#000" />
      <path
        d="M14.126 21.259H6.953v-7.124H-10.98V7.009H6.953V-.115h7.173v7.124h17.932v7.126H14.126v7.124Z"
        fill="#fff"
      />
      <path d="M10.54-.115V21.26-.115ZM-10.98 10.572h43.037-43.037Z" fill="#000" />
      <path
        d="M12.691 21.259H8.387v-8.55h-19.366V8.434H8.387v-8.55h4.304v8.55h19.367v4.276H12.69v8.549Z"
        fill="#C8102E"
      />
    </g>
  </svg>
);

export default UkIcon;
