import * as React from 'react';
import { SVGProps } from 'react';

const LinkedIn = (props: SVGProps<SVGSVGElement>) => (
  <svg width={23} height={23} fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
    <path
      d="M.47 7.559h4.728v15.205H.47V7.559ZM2.843 0a2.746 2.746 0 0 1 2.742 2.742 2.746 2.746 0 0 1-2.742 2.742A2.74 2.74 0 0 1 .1 2.742 2.74 2.74 0 0 1 2.843 0ZM8.169 7.559h4.526v2.083h.062c.633-1.196 2.17-2.453 4.473-2.453 4.782 0 5.67 3.147 5.67 7.243v8.332h-4.72v-7.392c0-1.767-.027-4.034-2.453-4.034-2.46 0-2.83 1.925-2.83 3.902v7.524h-4.72V7.559H8.17Z"
      fill="#fff"
    />
  </svg>
);

export default LinkedIn;
