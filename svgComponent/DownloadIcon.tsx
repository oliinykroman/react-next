import * as React from 'react';
import { SVGProps } from 'react';

const DownloadIcon = (props: SVGProps<SVGSVGElement>) => (
  <svg width={88} height={88} fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
    <path
      d="M44.138 87.264c23.82 0 43.14-19.31 43.14-43.14 0-23.83-19.32-43.13-43.14-43.13S.998 20.304.998 44.134c0 23.83 19.31 43.14 43.14 43.14v-.01Z"
      fill="#121212"
    />
    <path d="M34.508 58.154h19.24" stroke="#fff" strokeWidth={2.4} />
    <path
      d="M44.129 49.134v-32.66c-15.27 0-27.65 12.38-27.65 27.65 0 15.27 12.38 27.65 27.65 27.65 15.27 0 27.65-12.38 27.65-27.65 0-10.5-5.85-19.63-14.47-24.32"
      stroke="#E93C8D"
      strokeWidth={2.4}
      strokeLinejoin="round"
    />
    <path
      d="m53.338 41.364-9.2 9.2-9.21-9.2"
      stroke="#E93C8D"
      strokeWidth={2.4}
      strokeLinejoin="round"
    />
  </svg>
);

export default DownloadIcon;
