import * as React from 'react';
import { SVGProps } from 'react';

const ArrowRight = (props: SVGProps<SVGSVGElement>) => (
  <svg width={50} height={50} fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
    <circle
      cx={25}
      cy={25}
      r={24}
      transform="rotate(180 25 25)"
      fill="#fff"
      stroke="#A60054"
      strokeWidth={2}
    />
    <path
      d="M36.707 25.707a1 1 0 0 0 0-1.414l-6.364-6.364a1 1 0 0 0-1.414 1.414L34.586 25l-5.657 5.657a1 1 0 0 0 1.414 1.414l6.364-6.364ZM14 26h22v-2H14v2Z"
      fill="#A60054"
    />
  </svg>
);

export default ArrowRight;
