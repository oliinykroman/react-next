import * as React from 'react';
import { SVGProps } from 'react';

const WhiteChevron = (props: SVGProps<SVGSVGElement>) => (
  <svg
    className="WhiteChevron"
    width={15}
    height={24}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}>
    <path d="m2 2 10 10L2 22" stroke="#fff" strokeWidth={3} />
  </svg>
);

export default WhiteChevron;
