import * as React from 'react';
import { SVGProps } from 'react';

const MobileLogo = (props: SVGProps<SVGSVGElement>) => (
  <svg width={41} height={41} fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
    <path
      d="M19.996 0C8.949 0 0 8.957 0 20.004S8.957 40 20.004 40 40 31.043 40 19.996C40 8.957 31.05 0 19.996 0Zm-4.672 33.625c-4.944 0-8.958-4.014-8.958-8.957 0-4.944 4.014-8.957 8.958-8.957 4.943 0 8.957 4.013 8.957 8.957.009 4.943-4.005 8.957-8.957 8.957.008 0 0 0 0 0Z"
      fill="url(#a)"
    />
    <path
      d="M18.466 8.136c-7.395 0-13.4 5.996-13.4 13.4 0 3.536 1.4 6.925 3.891 9.442-3.462-3.496-3.446-9.139.05-12.601 3.496-3.463 9.138-3.447 12.601.049 3.463 3.496 3.447 9.138-.05 12.601-3.47 3.447-9.072 3.447-12.551 0 5.24 5.223 13.72 5.207 18.943-.033 5.223-5.24 5.207-13.72-.033-18.943a13.36 13.36 0 0 0-9.451-3.915Z"
      fill="url(#b)"
    />
    <defs>
      <linearGradient
        id="a"
        x1={8.062}
        y1={8.083}
        x2={33.627}
        y2={33.642}
        gradientUnits="userSpaceOnUse">
        <stop stopColor="#FF45B8" />
        <stop offset={1} stopColor="#A60054" />
      </linearGradient>
      <linearGradient
        id="b"
        x1={26.519}
        y1={29.534}
        x2={9.453}
        y2={12.468}
        gradientUnits="userSpaceOnUse">
        <stop stopColor="#FF45B8" />
        <stop offset={1} stopColor="#A60054" />
      </linearGradient>
    </defs>
  </svg>
);

export default MobileLogo;
